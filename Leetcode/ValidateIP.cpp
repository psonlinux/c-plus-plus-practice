//
//  ValidateIP.cpp
//  Leetcode
//
//  Created by Prashant Singh on 10/08/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

class Solution {
    bool isIPV4(int s, string & str, int k){
        if(s == str.size() || str.size() > 15 || str.size() < 7){
            return false;
        }
        
        string num = "";
        int i;
        for(i = s; i < str.size(); i++){
            if(str[i] == '.'){
                break;
            }
            
            if(isdigit(str[i]) == false){
                return false;
            }
            num += str[i];
        }
        
        if(num.size() == 0 || num.size() > 3){    //"12..33.4"
            return false;
        }
        
        int x = stoi(num);  //convert to integer
        if((x != 0 && str[s] == '0') || (x == 0 && num.size() > 1)){    // invalid with leading zero
            return false;
        }
        
        //base case
        if(x >= 0 && x < 256 && k == 1 && i == str.size()){
            return true;
        }
        
        // recursive case
        if(x >= 0 && x < 256 && isIPV4(i + 1, str, k - 1)){
            return true;
        }
        
        return false;   // ip not valid
    }
    
    bool isIPV6(int s, string & str, int k){
        if(s == str.size() || str.size() > 39 || str.size() < 15){
            return false;
        }
        
        string num = "";
        int i;
        for(i = s; i < str.size(); i++){
            if(str[i] == ':'){
                break;
            }
            
            if(isxdigit(str[i]) == false){
                return false;
            }
            num += str[i];
        }
        
        if(num.size() == 0 || num.size() > 4){    // invalid case
            return false;
        }
        
        int x = stoi(num, nullptr, 16);
        
        //base case
        if(x >= 0 && x < 65536 && k == 1 && i == str.size()){
            return true;
        }
        
        //recursive case
        if(x >= 0 && x < 65536 && isIPV6(i + 1, str, k - 1)){
            return true;
        }
        
        return false;
    }
public:
    string validIPAddress(string IP) {
        int ipv4 = 4, ipv6 = 8;
        if(isIPV4(0, IP, ipv4) == true){
            return "IPv4";
        }
        else if(isIPV6(0, IP, ipv6) == true){
            return "IPv6";
        }
        else{
            return "Neither";
        }
    }
};
