//
//  ExpressionAddOp.cpp
//  Leetcode
//
//  Created by Prashant Singh on 21/06/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//
#include "vector"
#include "stack"
#include "string"
#include "iostream"
using namespace std;

class Solution {
    void eval(string & num, int start, int prevVal, long valueSoFar, int t, string res, vector<string> & v){
        if(start == num.size()){
            if(valueSoFar == t){
                v.push_back(res);
            }
            return;
        }
        
        long cur = 0;
        string curStr = "";
        for(int i = start; i < num.size(); i++){
            if(num[start] == '0' && i > start){
                break;
            }
            curStr = curStr + num[i];
            cur = 10 * cur + num[i] - '0';
            if(start == 0){
                eval(num, i + 1, cur, cur, t, res + curStr, v);
            }
            else{
                eval(num, i + 1, cur * prevVal, valueSoFar - prevVal + prevVal * cur, t, res + '*' + curStr, v);
                eval(num, i + 1, -cur, valueSoFar - cur, t, res + '-' + curStr, v);
                eval(num, i + 1, cur, valueSoFar + cur, t, res + '+' + curStr, v);
            }
        }
    }
public:
    vector<string> addOperators(string num, int target) {
        vector<string> v;
        if(num == ""){
            return v;
        }
        
        eval(num, 0, 0, 0, target, "", v);
        return v;
    }
};

int main(){
    string expr = "22";
    int target = 4;
    Solution sol;
    sol.addOperators(expr, target);
    return 0;
}
