class Solution {
   public:
    string fractionToDecimal(int n, int d) {
        long long nr = n, dr = d;
        if (nr % dr == 0) {
            return to_string(nr / dr);
        }

        // some decimal part exist
        string res = "";

        // handle sign
        int sign = 1;
        if (nr < 0) {
            sign *= -1;
            nr *= -1;
        }
        if (dr < 0) {
            sign *= -1;
            dr *= -1;
        }

        if (sign == -1) {
            res = "-";
        }
        res += to_string(nr / dr) + ".";
        long long r = nr % dr;

        unordered_map<long long, int> indexOfRem;

        long long div = 0, q = 0;
        while (r != 0 && indexOfRem.find(r) == indexOfRem.end()) {
            indexOfRem[r] = res.size();
            div = r * 10;
            q = div / dr;
            res += to_string(q);
            r = div % dr;
        }

        // terminated
        if (r == 0) {
            return res;
        } else {
            res += ')';
            res.insert(indexOfRem[r], "(");
            return res;
        }
    }
};