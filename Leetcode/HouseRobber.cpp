
/*
https://leetcode.com/problems/house-robber/

Time: O(n)
Space: O(n)
*/
class Solution {
    vector<int> dp;
    vector<int> money;
    
    /*
    Top down DP
    This function returns maximum money robbed from house i to n - 1.
    Idea : Either house i is included in robbery or not.
    */

    int robTopDown(int i){
        if(i >= money.size())
            return 0;
        
        if(dp[i] != -1)
            return dp[i];
        
        /*
        m1 contains the maximum money robbed from house i plus 
        from i + 2 to n - 1
        */

        int m1 = money[i] + robTopDown(i + 2);
        
        /*
        robber exclude i'th house .
        m2 contains the max money robbed from house i + 1 to n - 1
        */

        int m2 = robTopDown(i + 1);
        dp[i] = max(m1, m2);
        return dp[i];
    }
public:
    //Bottom Up DP
    //returns max money robbed from house 0 to n - 1
    int rob(vector<int>& money) {
        int houses = money.size();
        if(houses == 0)
            return 0;
        
        /*
        robbed[i] : Max money robbed from house 0 to i 
        */
        vector<int> robbed(houses, -1);    //initial no value is saved
        
        //base case
        
        robbed[0] = money[0];                   //only one house
        robbed[1] = max(money[0], money[1]);    //only two house
        
        for(int house = 2; house < houses; house++){
            //case 1 : including
            //money robbed from houses [0, house] 
            int m1 = money[house] + robbed[house - 2];
            
            //excluding
            //max money robbed from houses [0, house - 1]
            int m2 = robbed[house - 1];
            
            robbed[house] = max(m1, m2);
        }
        return robbed[houses - 1];
    }
};