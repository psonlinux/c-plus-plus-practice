/*
T : O(log n)
S : O(1)
*/
class Solution {
   public:
    double myPow(double x, int n) {
        double res = 1;
        if (n == 0 || x == 1) {
            return res;
        }

        int sign = 1;
        long e = n;
        if (e < 0) {
            sign = -1;
            e = -1 * e;
        }

        while (e > 1) {
            if (e % 2 == 1) {
                res *= x;
                e -= 1;
            } else {
                x *= x;
                e /= 2;
            }
        }

        res = res * x;
        if (sign == -1) {
            res = 1 / res;
        }
        return res;
    }
};