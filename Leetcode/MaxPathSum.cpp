/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/*
T : O(n)
S : O(n) , on stack space is used
*/
class Solution {
    long long getMaxPath(TreeNode* node, long long& mp) {
        if (node == NULL) {
            return INT_MIN;
        }

        if (node->left == NULL && node->right == NULL) {
            return node->val;
        }

        long long lt = getMaxPath(node->left, mp);
        long long rt = getMaxPath(node->right, mp);
        long long maxSubPath = lt;
        if (rt > lt) {
            maxSubPath = rt;
        }

        // path through this node
        long long pathWithNode = lt + node->val + rt;
        if (pathWithNode > mp) {
            mp = pathWithNode;
        }
        if (maxSubPath > mp) {
            mp = maxSubPath;
        }

        if (node->val > maxSubPath + node->val) {
            return node->val;
        } else {
            return maxSubPath + node->val;
        }
    }

   public:
    int maxPathSum(TreeNode* root) {
        long long mp = root->val;
        long long path = getMaxPath(root, mp);
        if (path > mp) {
            mp = path;
        }
        return mp;
    }
};