/*
https://leetcode.com/problems/positions-of-large-groups/
Time : O(n)
Space : O(n)
*/
class Solution {
public:
    vector<vector<int>> largeGroupPositions(string S) {
        vector<vector<int>> a;
        int c = 1, s = 0, e = 0;
        for(int i = 1; i < S.size(); i++){
            if(S[i] == S[i - 1]){
                c += 1;
                e = i;
            }
            
            if(i + 1 == S.size() || S[i] != S[i - 1]){
                if(c >= 3){
                    vector<int> v = {s, e};
                    a.push_back(v);
                }   
                c = 1;
                s = i;
                e = i;
            }
        }
        return a;
    }
};