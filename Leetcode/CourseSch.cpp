/* https://leetcode.com/problems/course-schedule/
T : O(numCourses)
S : O(V + E)
*/
class Solution {
    /*
    If cycle exist then at some point c must exceed numCourses .
    Else not .
    */
    bool detectCycle(int course, int c, vector< vector<int> > & pqs, int numCourses, vector<bool> & visited){
        visited[course] = true;
        c += 1;
        if(c > numCourses)
            return true;
        for(int i = 0; i < pqs[course].size(); i++){
            if(detectCycle(pqs[course][i], c, pqs, numCourses, visited))
                return true;
        }
        return false;
    }
public:
    bool canFinish(int numCourses, vector<pair<int, int>>& pq) {
        int n = pq.size();
        if(n == 0)return true;
        
        // pqs[i] = all prereqs for i'th course
        vector< vector<int> > pqs(numCourses);
        for(int i = 0; i < pq.size(); i++){
            pqs[pq[i].first].push_back(pq[i].second);
        }
        
        vector<bool> visited(numCourses, false);
        /*
        Check for each course whether there is a cycle starting from that
        */
        for(int i = 0; i < numCourses; i++){
            
            if(!visited[i] && detectCycle(i, 0, pqs, numCourses, visited))
                return false;
        }
        return true;
    }
};