//
//  DeleteNodesAndReturnForest.cpp
//  Leetcode
//
//  Created by Prashant Singh on 08/07/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//


/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
    /* Save sub tree of this node */
    void saveSubtree(TreeNode * node, vector<TreeNode *> & forest){
        if(node->left){
            forest.push_back(node->left);
        }
        
        if(node->right){
            forest.push_back(node->right);
        }
    }
    
    /*
     Key : process small tree first .
     Every node is child of some node except root node .
     Check whether left or right node can be deleted .
     Before deleteon save its subtree and then delete that node .
     */
    void consider(TreeNode * node, unordered_set<int> & toDeleteSet, vector<TreeNode *> & forest){
        if(node != NULL){
            // is left node can be deleted
            consider(node->left, toDeleteSet, forest);
            consider(node->right, toDeleteSet, forest);
            
            // now process this node
            if(node->left && toDeleteSet.find(node->left->val) != toDeleteSet.end()){
                saveSubtree(node->left, forest);
                
                // delete
                node->left = NULL;
            }
            
            if(node->right && toDeleteSet.find(node->right->val) != toDeleteSet.end()){
                saveSubtree(node->right, forest);
                
                // delete
                node->right = NULL;
            }
        }
    }
public:
    vector<TreeNode*> delNodes(TreeNode* root, vector<int>& toDel) {
        if(toDel.size() == 0){
            return {root};
        }
        
        //get address of each node
        unordered_set<int> st;
        int td = toDel.size();
        for(int i = 0; i < td; i++){
            st.insert(toDel[i]);
        }
        
        // add forest and mark the node with INT_MAX
        vector<TreeNode *> forest;
        consider(root, st, forest);
        
        // process root
        if(st.find(root->val) == st.end()){
            forest.push_back(root);
        }
        else{
            saveSubtree(root, forest);
        }
        return forest;
    }
};
