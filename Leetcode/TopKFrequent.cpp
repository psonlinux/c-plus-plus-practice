/*
T : O(A.size())
S : O(A.size())
*/
class Item {
   public:
    int fr, data;
};

class Solution {
    /*
    Get K 'th largest item .
    Non-increasing order .
    */
    int KLargest(int K, vector<Item>& v, int lo, int ub) {
        if (lo > ub) {
            return -1;
        }
        if (ub - lo == 1) {
            return lo;
        }

        int i = lo + 1, j = ub - 1;
        while (true) {
            while (i <= j && v[i].fr >= v[lo].fr)
                i += 1;
            while (j >= i && v[j].fr < v[lo].fr)
                j -= 1;

            if (i > j) {
                break;
            }

            swap(v[i], v[j]);
            i += 1;
            j -= 1;
        }
        swap(v[lo], v[j]);

        if (j == K - 1) {
            return j;
        } else if (j > K - 1) {
            return KLargest(K, v, lo, j);  // consider left of partition
        } else {
            return KLargest(K, v, j + 1, ub);  // consider right of partition
        }
    }

   public:
    vector<int> topKFrequent(vector<int>& A, int k) {
        // frequency of each A[i]
        unordered_map<int, int> fr;

        // count frequency      T : O(A.size()) S: O(A.size())
        int n = A.size();
        for (int i = 0; i < n; i++) {
            fr[A[i]] += 1;
        }

        // create a array where each item hase two values
        // v[i].fr, v[i].data
        vector<Item> v(fr.size());
        unordered_map<int, int>::iterator it;  // T : O(A.size())
        int i = 0;
        for (it = fr.begin(); it != fr.end(); it++) {
            v[i].data = it->first;
            v[i].fr = it->second;
            i += 1;
        }

        // use K'th largest item logic to get K most frequent item
        KLargest(k, v, 0, v.size());  // T : O(A.size())

        vector<int> res;
        // take first K items from vector
        for (int i = 0; i < k; i++) {
            res.push_back(v[i].data);
        }

        return res;
    }
};