/*
T : O(nlogn)
S : O(n)
n = # buildings
*/
// compare building height
struct compareHeight {
    bool operator()(const vector<int>& a, const vector<int>& b) {
        return a[2] < b[2];
    }
};

class Solution {
   public:
    vector<pair<int, int>> getSkyline(vector<vector<int>>& B) {
        int n = B.size();
        if (n == 0) {
            return {};
        }

        // Keep x co-ordinate of left and right edges of building T : O(n)
        map<int, int> heightMap;
        for (int i = 0; i < n; i++) {
            heightMap[B[i][0]] = 0;  // left x of building
            heightMap[B[i][0] - 1] = 0;

            heightMap[B[i][1]] = 0;  // right x of building
            heightMap[B[i][1] - 1] = 0;
        }

        // keep each building in map with left x value being the KEY .
        // T : O(n)
        unordered_map<int, vector<vector<int>>> bMap;
        for (int i = 0; i < n; i++) {
            bMap[B[i][0]].push_back(B[i]);
        }

        // create heap for currently active buildings . // T : O(n log n)
        priority_queue<vector<int>, vector<vector<int>>, compareHeight> pq;
        int x = 0;
        vector<vector<int>> bs;
        for (map<int, int>::iterator it = heightMap.begin(); it != heightMap.end(); it++) {
            // put building in heap
            x = it->first;
            bs = bMap[x];
            for (int i = 0; i < bs.size(); i++) {
                pq.push(bs[i]);
            }

            // get the tallest building
            // check if x lies in its range, if not remove it from  heap
            // update height if the x coordinate lies in buiiding range
            vector<int> building;
            while (pq.empty() == false) {
                building = pq.top();
                if (x >= building[0] && x < building[1]) {
                    heightMap[x] = building[2];
                    break;
                } else {
                    pq.pop();  // this building will not be considered further
                }
            }
        }

        // collect all key points
        // iterate over each critical point
        vector<pair<int, int>> res;
        for (map<int, int>::iterator it = heightMap.begin(); it != heightMap.end(); it++) {
            x = it->first;
            if (heightMap.find(x - 1) != heightMap.end() && heightMap[x] != heightMap[x - 1]) {
                res.push_back(make_pair(x, heightMap[x]));
            }
        }
        return res;
    }
};