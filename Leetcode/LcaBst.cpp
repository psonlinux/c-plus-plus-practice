/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/*
https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-search-tree/submissions/
TIme : O(n)
Space : O(n)
*/
class Solution {
public:
    TreeNode * lca(TreeNode *r, TreeNode *p, TreeNode *q){
        if(r->val < p->val)
            return lowestCommonAncestor(r->right, p, q);
        if(r->val > q->val)
            return lowestCommonAncestor(r->left, p, q); 
        return r;   
    }
    
    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        if(q->val < p->val)swap(p, q);
        return lca(root, p, q);
    }
};