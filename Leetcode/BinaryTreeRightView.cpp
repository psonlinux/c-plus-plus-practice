//
//  BinaryTreeRightView.cpp
//  Leetcode
//
//  Created by Prashant Singh on 10/07/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/*
 https://leetcode.com/problems/binary-tree-right-side-view/
 T : O(nodes in tree)
 S : O(node in tree)
 */
class Solution {
public:
    vector<int> rightSideView(TreeNode* root) {
        vector<int> rv;
        if(root == NULL){
            return rv;
        }
        
        vector<TreeNode *> q;
        q.push_back(root);      // intialize first level
        
        // pick rightmost node from each level
        while(q.empty() == false){
            TreeNode * rightMostNode = q.back();
            rv.push_back(rightMostNode->val);
            
            // add next level
            int n = q.size();
            vector<TreeNode *> nextLevel;
            for(int i = 0; i < n; i++){
                if(q[i]->left != NULL){
                    nextLevel.push_back(q[i]->left);
                }
                
                if(q[i]->right != NULL){
                    nextLevel.push_back(q[i]->right);
                }
            }
            
            //make this level current level
            q = nextLevel;
        }
        return rv;
    }
};
