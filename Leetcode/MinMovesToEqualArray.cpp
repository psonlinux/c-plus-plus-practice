//
//  MinMovesToEqualArray.cpp
//  Leetcode
//
//  Created by Prashant Singh on 15/07/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

/*
 T : O(nlogn)
 S : O(n)
 */
class Solution {
public:
    int minMoves2(vector<int>& A) {
        long n = A.size();
        long minMoves = 0;
        if(n <= 1){
            return minMoves;
        }
        
        minMoves = INT_MAX;
        
        //sort
        sort(A.begin(), A.end());
        
        //keep sum of A[0...i] in advance
        long S[n];
        S[0] = A[0];
        for(int i = 1; i < n; i++){
            S[i] = S[i - 1] + A[i];
        }
        
        for(int i = 0; i < n; i++){
            long moves = 0;
            // before A[i]
            if(i > 0){
                moves += ((long)A[i] * i - S[i - 1]);
            }
            
            //after A[i]
            moves += (S[n - 1] - S[i] - (n - (i + 1)) * A[i]);
            
            if(moves < minMoves){
                minMoves = moves;
            }
        }
        return minMoves;
    }
};
