//
//  MinTouch.cpp
//  Leetcode
//
//  Created by Prashant Singh on 19/10/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//


#include<iostream>
#include<fstream>
using namespace std;

/*
 28 = 4 * 35 / 5
 */
int num[10];
int op[4];
int pNum[1000];

void genAllPermutation(){
    for(int i = 0; i < 10; i++){
        for(int j = 0; j < 10; j++){
            for(int k = 0; k < 10; k++){
                int x = 0;
                if(num[i]){
                    x = x * 10 + i;
                }
                
                if(num[j]){
                    x = x * 10 + j;
                }
                
                if(num[k]){
                    x = x * 10 + k;
                }
                
                if(num[i] || num[j] || num[k]){
                    pNum[x] = true;
                }
            }
        }
    }
}

int touchRequired(int n){
    if(n > 999){
        return 4;
    }
    else if(n > 99){
        return 3;
    }
    else if(n > 9){
        return 2;
    }
    else if(n >= 0){
        return 1;
    }
    else{
        return 4;
    }
}

void minTouchHelper(int W, int M, int exp, int touch, int & res){
    //cout << exp << " "  << touch << endl;
    if(touch > M){
        return;
    }
    
    if(exp < 0 || exp > 999){
        return;
    }
    
    if(exp == W){
        if(touch + 1 < res){
            res = touch + 1;
        }
        return;
    }
    
    for(int i = 0; i < 4; i++){
        if(op[i] == false){
            continue;
        }
        for(int j = 1; j < 1000; j++){
            if(pNum[j] == false){
                continue;
            }
            
            if(i == 0){
                minTouchHelper(W, M, exp + j, touch + 1 + touchRequired(j), res);
            }
            else if(i == 1){
                minTouchHelper(W, M, exp - j, touch + 1 + touchRequired(j), res);
            }
            else if(i == 2){
                minTouchHelper(W, M, exp * j, touch + 1 + touchRequired(j), res);
            }
            else{
                if(j != 0){
                    minTouchHelper(W, M, exp / j, touch + 1 + touchRequired(j), res);
                }
            }
        }
    }
}

int minTouch(int W, int M){
    if(pNum[W] == true){
        return touchRequired(W);
    }
    
    int res = M + 1;
    for(int i = 0; i < 1000; i++){
        if(pNum[i] == true){
            minTouchHelper(W, M, i, touchRequired(i), res);
        }
    }
    return res;
}

/* Answer
 #1 4         2 + 3 = 5
 #2 9         16 * 62 - 11 = 981
 */
int main(){
    ifstream in("in.txt", ios::in);
    if(!in){
        cout << "Error" << endl;
        exit(1);
    }

    int T = 0;
    in >> T;

    int t = 0;
    while(t < T){
        int N, P, M;
        in >> N >> P >> M;
        
        int x = -1;
        memset(num, 0, 10 * sizeof(int));
        memset(op, 0, 4 * sizeof(int));
        for(int i = 0; i < N; i++){
            in >> x;
            num[x] = true;
        }
        
        for(int i = 0; i < P; i++){
            in >> x;
            op[x - 1] = true;
        }
        
        int W = -1;
        in >> W;
        genAllPermutation();
        int res = minTouch(W, M);
        cout << res << endl;
        
        t += 1;
    }
    return 0;
}
