/* https://leetcode.com/problems/unique-binary-search-trees/
T : O(n^2)
S : O(n)
*/
class Solution {
    /*
     Top down approach, takes stack space
     */
    int numTrees(int n, vector<int> & memo){
        if(n <= 1){
            return 1;
        }
        
        if(memo[n] != -1){
            return memo[n];
        }
        
        int x = 0;
        for(int i = 1; i <= n; i++){
            x += numTrees(i - 1) * numTrees(n - i);
        }
        memo[n] = x;
        return x;
    }
public:
    // Bottom up approach
    int numTrees(int n) {
        vector<int> nt(n + 1, 1); //nt[i] == num of trees
        
        for(int i = 2; i <= n; i++){
            int x = 0;
            for(int j = 1; j <= i; j++){
                x += nt[j - 1] * nt[i - j];
            }
            nt[i] = x;
        }
        return nt[n];
    }
};
