/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/*
https://leetcode.com/problems/diameter-of-binary-tree/
Time : O(n)
Space : O(n)

There is one node on longest path whose left subtree height + right subtree height + 2 will 
give longest path .
*/
class Solution {
public:
    int height(TreeNode *r, int & dm){
        if(r == NULL || (!r->left && !r->right))return 0;
        int lh = height(r->left, dm);
        int rh = height(r->right, dm);
        
        //calculate path
        int lp = (r->left)?(1 + lh) : lh;
        int rp = (r->right)?(1 + rh) : rh;
        dm = max(dm, lp + rp);
        return 1 + max(lh, rh);
    }
    
    int diameterOfBinaryTree(TreeNode* root) {
        if(root == NULL || (!root->left && !root->right))return 0;
        int dm = INT_MIN;
        height(root, dm);
        return dm;
    }
};
