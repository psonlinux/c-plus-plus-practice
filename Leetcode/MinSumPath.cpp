/* https://leetcode.com/problems/minimum-path-sum/
T : O(m * n)
S : O(m * n)
*/
class Solution {
public:
    int minPathSum(vector<vector<int>>& grid) {
        int m = grid.size();
        int n = grid[0].size();
        
        int lc = 0, uc = 0;
        for(int i = 0; i < m; i++){
            for(int j = 0; j < n; j++){
                //base case
                if(i == 0 && j == 0)
                    continue;
                
                // left cell
                if(j - 1 >= 0)  lc = grid[i][j - 1];
                else            lc = INT_MAX;
                
                //above cell
                if(i - 1 >= 0)  uc = grid[i - 1][j];
                else            uc = INT_MAX;
                
                grid[i][j] = grid[i][j] + min(lc, uc);
            }
        }
        return grid[m - 1][n - 1];
    }
};