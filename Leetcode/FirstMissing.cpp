/*
T : O(n)
S : O(1)
*/
class Solution {
public:
    int firstMissingPositive(vector<int>& A) {
        int n = A.size(), i = 0;
        /* case 1: vector has all integer from [1...n]
         then return n + 1
         
         case 2:some +ve integer is missing in [1...n]
         then return that i + 1
         
         */
        while(i < n){
            if(A[i] > 0 && A[i] <= n && (A[i] != i + 1) && (A[i] != A[A[i] - 1])){
                swap(A[i], A[A[i] - 1]);
            }
            else{
                i += 1;
            }
        }
        
        // return i + 1 if A[i] != i + 1
        for(int i = 0; i < n; i++){
            if(A[i] != i + 1){
                return i + 1;
            }
        }
        return n + 1;
    }
};
