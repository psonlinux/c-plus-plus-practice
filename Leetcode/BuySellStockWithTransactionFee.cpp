//
//  BuySellStockWithTransactionFee.cpp
//  Leetcode
//
//  Created by Prashant Singh on 09/06/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

/*
 T : O(n)
 The idea is same as that we can sell stock at i'th day .
 
 maxProfit(0, i, fee) =
 max{
 maxProfit(0, i-1, fee),
 max{ p[i] - p[k] - fee + maxProfit(0, k - 1, fee)   , k >= 0 && k < i
 }
 }
 */
class Solution {
    int maxProfit(int i, int fee, vector<int> & prices, vector<int> & memo){
        if(i <= 0){
            return 0;
        }
        
        int x = maxProfit(i - 1, fee, prices, memo);
        int y = 0;
        for(int k = 0; k < i; k++){
            y = max(y, prices[i] - prices[k] - fee + maxProfit(k - 1, fee, prices, memo));
        }
        memo[i] = max(x, y);
        return memo[i];
    }
public:
    int maxProfit(vector<int>& prices, int fee) {
        int n = prices.size();
        if(n <= 1){
            return 0;
        }
        
        int maxProfit[n + 1];
        maxProfit[0]    = 0;
        maxProfit[1]    = 0;
        int x = -prices[0] + maxProfit[0];
        for(int i = 2; i < n + 1; i++){
            x               = max(x, -prices[i - 1] + maxProfit[i - 1]);
            maxProfit[i]    = max(maxProfit[i - 1], x + prices[i - 1] - fee);
        }
        return maxProfit[n];
    }
};
