//
//  WildCardMatch.cpp
//  Leetcode
//
//  Created by Prashant Singh on 01/05/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

#include <iostream>
#include <vector>
#include <string>
using namespace std;

/*
 "aaabbbaabaaaaababaabaaabbabbbbbbbbaabababbabbbaaaaba"
 "a*******b"
 */
/*
 T : O(s * p)
 S : O(s * p)
 */
class Solution {
    bool isMatch(string & s, int i, string & p, int j, vector<vector<short>> & dp){
        if(dp[i][j] != -1){
            return dp[i][j];
        }
        
        if(i < s.size() && j == p.size()){  // only p empty
            dp[i][j] = false;
            return false;
        }
        
        // case 1 : p[j] == '*'
        if(j < p.size() && p[j] == '*'){
            if(isMatch(s, i, p, j + 1, dp)){
                dp[i][j] = true;
                return true;
            }
            
            if(i < s.size() && isMatch(s, i + 1, p, j, dp)){
                dp[i][j] = true;
                return true;
            }
        }
        else if(j < p.size() && p[j] == '?'){
            if(i == s.size()){
                dp[i][j] = false;
                return false;
            }
            
            if(isMatch(s, i + 1, p, j + 1, dp)){
                dp[i][j] = true;
                return true;
            }
        }
        else if(i < s.size() && j < p.size()){
            if(s[i] == p[j] && isMatch(s, i + 1, p, j + 1, dp)){
                dp[i][j] = true;
                return true;
            }
            dp[i][j] = false;
            return false;
        }
        dp[i][j] = false;
        return false;
    }
public:
    bool isMatch(string s, string p) {
        vector<vector<short>> dp(s.size() + 1, vector<short>(p.size() + 1, -1));
        dp[s.size()][p.size()] = true;
        return isMatch(s, 0, p, 0, dp);
    }
};

int main(){
    string s = "aaabbbaabaaaaababaabaaabbabbbbbbbbaabababbabbbaaaaba", p = "a*******b";
    Solution sol;
    if(sol.isMatch(s, p)){
        cout << "Matched\n";
    }
    else{
        cout << "Not matched\n";
    }
    return 0;
}
