/* https://leetcode.com/problems/sort-colors/
T : O(n)
S : O(n)
*/
class Solution {
public:
    void sortColors(vector<int>& v) {
        int n = v.size();
        int i = 0, lo = 0, hi = v.size() - 1;
        while(i <= hi){
            if(v[i] < 1){
                swap(v[i], v[lo]);
                i += 1;
                lo += 1;
            }
            else if(v[i] > 1){
                swap(v[i], v[hi]);
                hi -= 1;
            }
            else
                i++;
        }
    }
};