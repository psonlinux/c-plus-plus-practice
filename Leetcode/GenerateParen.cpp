class Solution {
    void helper(string s, int open, int close, int n, vector<string> & ps){
        //all parens are placed
        if(close == n)
            ps.push_back(s);
        else{
            // put "("
            if(open < n)
                helper(s + '(', open + 1, close, n, ps );
            
            //put ")"
            if(open > close)
                helper(s + ')', open, close + 1, n, ps );
        }     
    }
public:
    vector<string> generateParenthesis(int n) {
        vector<string> ps;
        helper("", 0, 0, n, ps);
        return ps;
    }
};