//
//  LRUCache.cpp
//  Leetcode
//
//  Created by Prashant Singh on 02/05/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

/*
 T : O(1)
 S : O(cache size)
 */
struct Node{
    int key, value;
    struct Node * next;
};

typedef struct Node Node;

/* - We need two data structure :
 1 : unordered_map for keeping (key, value) : O(1)
 2 : set for keeping (lruStatus, key)
 */
class LRUCache {
    int N;  // size of link list
    int size = 0;
    // Head : always points to Least recently used item
    // Tail : always poins to Most recently used item
    Node *head = NULL, *tail = NULL;
    
    // Create a hash table to store (key, pointer)
    unordered_map<int, Node *> kp;
public:
    LRUCache(int capacity) {
        N = capacity;
    }
    
    int get(int key) {
        if(kp[key] == NULL){    // key not present in cache
            return -1;
        }
        // cout << key << "is in" << endl;
        // if already recent then simply return value
        int value = kp[key]->value;
        if(kp[key]->next == NULL){
            return value;
        }
        
        // create new node
        Node *newNode = (Node *)malloc(sizeof(Node));
        newNode->key    = key;
        newNode->value  = (kp[key])->value;
        newNode->next   = NULL;
        
        // remove it
        Node *curNode = kp[key];
        curNode->key    = curNode->next->key;
        curNode->value  = curNode->next->value;
        curNode->next   = curNode->next->next;
        
        kp[curNode->key] = curNode;
        
        if(curNode->next == NULL){
            tail = curNode;
        }
        
        // make it most recent
        tail->next  = newNode;
        tail        = newNode;
        kp[key]     = newNode;
        return value;
    }
    
    void put(int key, int value) {
        // cout << key << " " << value << endl;
        // create new node
        Node *newNode = (Node *)malloc(sizeof(Node));
        newNode->key    = key;
        newNode->value  = value;
        newNode->next   = NULL;
        
        //update value if already present
        if(kp[key] != NULL){
            // update val
            Node *nd     = kp[key];
            nd->value           = value;
            if(nd->next == NULL){   // already most recent
                return;
            }
            
            //remove it from list
            nd->value   = nd->next->value;
            nd->key     = nd->next->key;
            nd->next    = nd->next->next;
            kp[nd->key] = nd;
            
            // update tail
            if(nd->next == NULL){
                tail = nd;
            }
            
            // make it more recent by adding to tail
            tail->next  = newNode;
            tail        = newNode;
            
            //update new node's pointer in kp
            kp[key] = newNode;
            return ;
        }
        
        
        // key not already present so add it
        if(size < N){
            if(tail == NULL){
                tail = newNode;
                head = newNode;
            }
            else{
                tail->next  = newNode;
                tail        = newNode;
            }
            
            kp[key] = newNode;
            size += 1;
        }
        else{   // insert and remove least recently used item
            kp.erase(head->key);
            head = head->next;  // evict least recently used item
            if(head == NULL){
                tail = NULL;
            }
            
            // insert
            if(tail == NULL){
                tail = newNode;
                head = newNode;
            }
            else{
                tail->next  = newNode;
                tail        = newNode;
            }
            
            // add it's pointer
            kp[key] = newNode;
        }
    }
};

/**
 * Your LRUCache object will be instantiated and called as such:
 * LRUCache* obj = new LRUCache(capacity);
 * int param_1 = obj->get(key);
 * obj->put(key,value);
 */
