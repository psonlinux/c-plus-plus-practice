/* https://leetcode.com/problems/valid-mountain-array/
T : O(n)
S : O(n)
*/
class Solution {
public:
    bool validMountainArray(vector<int>& A) {
        if(A.size() < 3)return false;
        bool hasHitPeak = false;
        for(int i = 1; i < A.size() - 1; i++){
            if(A[i - 1] < A[i] && A[i] > A[i + 1])
                hasHitPeak = true;
            
            if(A[i - 1] < A[i] == false && hasHitPeak == false)
                    return false;
            if(A[i] > A[i + 1] == false && hasHitPeak == true)
                    return false;
        }
        if(hasHitPeak)  return true;
        else            return false;
    }
};