//
//  PermutationInString.cpp
//  Leetcode
//
//  Created by Prashant Singh on 08/08/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

/*
 https://leetcode.com/problems/permutation-in-string/
 */
class Solution {
    //checks whether two string have same number of chars ex "ababc" "cabab"
    // order may be different
    bool isValid(vector<int> & a, vector<int> & b){
        for(int i = 0; i < a.size(); i++){
            if(a[i] != b[i]){
                return false;
            }
        }
        return true;
    }
public:
    bool checkInclusion(string s, string t) {
        int m = s.size(), n = t.size();
        if(m > n){
            return false;
        }
        //count frequency of each char of s
        vector<int> fr(26);
        for(int i = 0; i < m; i++){
            fr[s[i] - 'a'] += 1;
        }
        
        vector<int> bag(26);
        int i = 0;
        for(i = 0; i < m; i++){
            bag[t[i] - 'a'] += 1;
        }
        
        if(isValid(fr, bag) == true){
            return true;
        }
        
        //check each window
        for(i = m; i < n; i++){
            bag[t[i] - 'a']     += 1;   //add
            bag[t[i - m] - 'a'] -= 1;   //sub
            if(isValid(fr, bag) == true){
                return true;
            }
        }
        return false;
    }
};
