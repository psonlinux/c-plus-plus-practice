/*
// Definition for a Node.
class Node {
public:
    int val;
    Node* left;
    Node* right;
    Node* next;

    Node() {}

    Node(int _val, Node* _left, Node* _right, Node* _next) {
        val = _val;
        left = _left;
        right = _right;
        next = _next;
    }
};
*/
/*
T : O(n)
S : implicit stack O(n). extra O(1)
*/
class Solution {
    void connect(Node * a, Node * b){
        if(a == NULL || b == NULL){
            return;
        }
        
        //connect a b
        a->next = b;
        
        //connect a's child
        connect(a->left, a->right);
        
        //connect b's child
        connect(b->left, b->right);
        
        //connect right , left
        connect(a->right, b->left);
    }
public:
    Node* connect(Node* root) {
        if(root == NULL){
            return root;
        }
        connect(root->left, root->right);
        return root;
    }
};
