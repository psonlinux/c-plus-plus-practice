/* https://leetcode.com/problems/flatten-binary-tree-to-linked-list/
T : O(n)
S : O(n)
*/
/** 
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
    TreeNode * p = NULL;        //holds already flatten tree
    void flat(TreeNode * r){
        if(!r)return;
        flat(r->right);
        flat(r->left);
        r->right = p;
        r->left = NULL;
        p = r;
    }
public:
    void flatten(TreeNode* root) {
        if(root == NULL)
            return ;
        flat(root);
    }
};