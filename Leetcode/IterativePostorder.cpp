//
//  IterativePostorder.cpp
//  Leetcode
//
//  Created by Prashant Singh on 11/06/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/* Postorder : Left tree, Right tree, Root*/
class Solution {
public:
    vector<int> postorderTraversal(TreeNode* root) {
        vector<int> post;
        if(root == NULL){
            return post;
        }
        stack<TreeNode *> stack;
        stack.push(root);       // intialize stack
        
        // iterate till some node remains to visit
        TreeNode * temp = NULL;
        while(stack.empty() == false){
            temp = stack.top();
            stack.pop();
            if(temp->left == NULL && temp->right == NULL){   // leaf node
                post.push_back(temp->val);
            }
            else{
                
                stack.push(new TreeNode(temp->val));    // save root
                if(temp->right != NULL){                // save right subtree
                    stack.push(temp->right);
                }
                
                if(temp->left != NULL){                 // save left subtree
                    stack.push(temp->left);
                }
            }
        }
        return post;
    }
};
