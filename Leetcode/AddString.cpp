/* https://leetcode.com/problems/add-binary/
T : O(n)
S : O(n)
*/
class Solution {
public:
    string addBinary(string a, string b) {
        if(b.size() > a.size())swap(a, b);    //a will always holds larger string
        int m = a.size(), n = b.size();
        string r    = "";
        int j = n - 1, sum = 0;
        
        //iterate over a
        for(int i = m - 1; i >= 0; i--){
            sum = sum / 2 + a[i] - '0';
            if(j >= 0){
                sum += b[j] - '0';
                j -= 1;
            }
            r += '0' + sum % 2;
        }
        
        //if sum of most significant digits produce a 1
        if(sum / 2 == 1)r += '1';
        reverse(r.begin(), r.end());
        return r;
    }
};