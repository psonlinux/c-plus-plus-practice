/*
https://www.hackerrank.com/challenges/coin-change/problem
*/

#include <iostream>
#include <vector>

using namespace std;

long getWays(long n, vector<long> &c){
	int coinTypes = c.size();

	//base case
	if(n == 0)
		return 1;		//1 way to give cash = 0

	if(coinTypes == 0)	//0 ways to give change when cash != 0 and you have no coins
		return 0;

	vector< vector<long> > cc(n + 1, vector<long>(coinTypes));
    
    for(int coin = 0; coin < coinTypes; coin++)
        cc[0][coin] = 1;
    
    long in, ex;
    for(int cash = 1; cash < n + 1; cash++){
        for(int coin = 0; coin < coinTypes; coin++){
            //ways by including  at least one c[j] coins
            in = (cash - c[coin] >= 0) ? cc[cash - c[coin]][coin] : 0;
            
            //ways by excluding c[j]
            ex = (coin >= 1) ? cc[cash][coin - 1] : 0;
            
            cc[cash][coin] = in + ex;
        }
    }
    
    //no. of ways for giving cash = n and using all coins type
    return cc[n][coinTypes - 1];
}
int main(){
	int n = 4;
	vector<long> coins{1, 2, 3};
	cout << getWays(n, coins) << endl;
	return 0;
}