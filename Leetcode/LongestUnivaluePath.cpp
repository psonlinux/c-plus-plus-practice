/** https://leetcode.com/problems/longest-univalue-path/
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/* Tough question hai !!
T : O(n)
S : O(n)
*/
class Solution {
    // go to each node and check for longest univalue path
    // passing through this node
    int visit(TreeNode *n, int & lup){
        if(n){
            if(n->left == NULL && n->right == NULL)
                return 0;
            else{
                int lp = visit(n->left, lup);
                int rp = visit(n->right, lup);
                
                //path length with this node in between
                if(n->left && n->left->val == n->val){
                    lp += 1;
                }else{
                    lp = 0;
                }
                
                if(n->right && n->right->val == n->val){
                    rp += 1;
                }else{
                    rp = 0;
                }
                
                if(lp + rp > lup)
                    lup = lp + rp;
                
                //return max of left path or right to its parent
                return max(rp, lp);
            }  
        }
        else{
            return 0;
        }
    }
public:
    int longestUnivaluePath(TreeNode* root) {
        int lup = 0;
        visit(root, lup);
        return lup;
    }
};