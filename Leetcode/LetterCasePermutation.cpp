/*
https://leetcode.com/problems/letter-case-permutation
Time : O(2^(size of S))
space : O(2^(size of S))
*/
class Solution {
public:
    vector<string> letterCasePermutation(string S) {
        vector<string> v;
        v.push_back(S);
        
        int k = 0;      //current index of char to toggl
        while(k < S.size()){
            if(isalpha(S[k])){
                int size = v.size();
                for(int i = 0; i < size; i++){
                    string str = v[i];
                    str[k] ^= 32;   //Toggle case of letter
                    v.push_back(str);
                }
            }
            k += 1;
        }
        return v;
    }
};