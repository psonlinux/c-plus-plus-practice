/* https://leetcode.com/problems/maximal-square/
T : O(r * c)
S : O(r * c)
*/
class Solution {
public:
    int maximalSquare(vector<vector<char>>& mt) {
        if(mt.size() == 0)  return 0;
        int r = mt.size();
        int c = mt[0].size();
        
        vector<vector<int>> bm(r, vector<int>(c));
        // bm[i][j] = n, means size of largest square of dimension n x n 
        int area = 0;
        for(int i = 0; i < r; i++){
            for(int j = 0; j < c; j++){
                if(mt[i][j] == '1'){
                    if(i == 0 || j == 0)
                        bm[i][j] = 1;
                    else{
                        bm[i][j] = min(bm[i-1][j], min(bm[i][j-1], bm[i-1][j-1])) + 1;
                    }    
                }
                area = max(area, bm[i][j] * bm[i][j]);
            }
        }
        return area;
    }
};