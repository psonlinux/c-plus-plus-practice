/*
https://leetcode.com/contest/weekly-contest-105/problems/maximum-sum-circular-subarray/
*/
#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
using namespace std;

class Solution {
public:
    int kadane(vector<int> &A){
            int n = A.size();
            int max = A[0];     //holds max sum subarray
            int prevSum = 0;    //holds max subarray ending at i'th index
            for(int i = 0; i < n; i++){
                if (prevSum + A[i] > A[i]){
                    prevSum = prevSum + A[i];
                }
                else{
                    prevSum = A[i];
                }
                
                if(prevSum > max)
                    max = prevSum;
            }
        return max;
    }
    
    int maxSubarraySumCircular(vector<int>& A) {
        int n = A.size();
        int circularMax = INT_MIN;
        
        //case 1 : max sum subarray does not wrap
        int maxByKadane = kadane(A);
        cout << maxByKadane;
        
        //case 2: max sum subarray wraps
        //wrapMaxSum = sum(A) - (-maxSumSubrray(-A))
        int sumA = 0; //sum of array
        bool allNeg = true;
        for(int i = 0; i < n; i++){
            sumA += A[i];
            allNeg = allNeg && (A[i] < 0) ? true : false;
            A[i] = -A[i];   //invert sign of each item
        }
        
        //case when all item are negative
        if (allNeg)
            return maxByKadane;
        
        int wrapMaxSum = sumA - (-kadane(A));
        //take max of two cases
        return (maxByKadane > wrapMaxSum) ? maxByKadane : wrapMaxSum;
    }
};