//
//  SolveSudoku.cpp
//  Leetcode
//
//  Created by Prashant Singh on 28/05/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

class Solution {
    /* Solve Sudoku Recursively
     */
    bool solveSudoku(int i, int j, vector<vector<char>>& B){
        if(i == B.size()){   // base case
            return true;
        }
        
        if(i < B.size() && j == B[0].size()){
            return solveSudoku(i + 1, 0, B);
        }
        
        // cell contains a number
        if(B[i][j] != '.'){
            return solveSudoku(i, j + 1, B);
        }
        
        // Otherwise This is a empty cell
        // Try placing a valid num in empty cell B[i][j]
        for(char v = '1'; v <= '9'; v++){
            if(isSafe(v, i, j, B) == true){
                B[i][j] = v;
                if(solveSudoku(i, j + 1, B) == true){
                    return true;
                }
            }
        }
        
        B[i][j] = '.';  // undo the assignment
        return false;
    }
    
    /* Check if it is safe to place v at B[i][j]
     */
    bool isSafe(int v, int i, int j, vector<vector<char>>& B){
        // check row constraint
        for(int k = 0; k < B[0].size(); k++){
            if(B[i][k] == v){
                return false;
            }
        }
        
        // check column constraint
        for(int k = 0; k < B.size(); k++){
            if(B[k][j] == v){
                return false;
            }
        }
        
        // check 3 x 3 region
        int R = sqrt(B.size());
        int r0 = i / R, c0 = j / R;
        for(int a = 0; a < R; a++){
            for(int b = 0; b < R; b++){
                if(B[R * r0 + a][R * c0 + b] == v){
                    return false;
                }
            }
        }
        
        return true;
    }
public:
    void solveSudoku(vector<vector<char>>& B) {
        solveSudoku(0, 0, B);
    }
};
