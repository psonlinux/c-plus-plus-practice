/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
 /*
 T : O(n)
 S : O(n)
 */
class Solution {
public:
    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
        // first node is dummy node
        ListNode *head = new ListNode(-1), *tail = head;
        
        while(l1 && l2){
            if(l1->val <= l2->val){
                tail->next = l1;
                l1 = l1->next;
            }
            else{
                tail->next = l2;
                l2 = l2->next;
            }
            tail = tail->next;      // increment tail
        }
        
        if(l1) tail->next = l1;
        if(l2) tail->next = l2;
        return head->next;
    }
};