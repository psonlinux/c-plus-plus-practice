//
//  PrisonCells.cpp
//  Leetcode
//
//  Created by Prashant Singh on 26/09/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//
#include<iostream>
#include<vector>
#include<string>
#include<unordered_map>
#include<queue>
#include<unordered_set>
using namespace std;


class Solution {
    int stateToInt(vector<int> &cells){
        int n = cells.size();
        int state = 0;
        for(int i = 0; i < n; i++){
            state = state << 1;
            state = state | cells[i];
        }
        return state;
    }
    
    vector<int> intToState(int state){
        vector<int> cells(8);
        int bit = 0;
        for(int i = 0; i < 8; i++){
            bit = (state >> i) & 1;
            cells[7 - i] = bit;
        }
        return cells;
    }
public:
    vector<int> prisonAfterNDays(vector<int>& cells, int N) {
        int n = cells.size();
        unordered_map<int, int> map;
        vector<int> curState(n), prevState(n);
        prevState       = cells;
        int cycle = 0, cycleLen = 0;
        
        int statePrev, stateCur;
        while(true){
            if(cycle == N || N == 0){
                return intToState(statePrev);
            }
            
            //conv to integer
            statePrev   = stateToInt(prevState);
            auto it     = map.find(statePrev);
            if(it == map.end()){
                for(int j = 1; j < n - 1; j++){
                    curState[j] = !(prevState[j - 1] ^ prevState[j + 1]);
                }
                stateCur    = stateToInt(curState);
                map[statePrev] = cycle;
                statePrev = stateCur;
                cycle   += 1;
                N       -= 1;
            }
            else{
                cycleLen = cycle - it->second + 1;
                break;
            }
            prevState = curState;
        }
        
        cout << cycleLen;
        N = N % cycleLen;
        for(int i = 1; i <= N; i++){
            stateCur = map[statePrev];
            statePrev = stateCur;
        }
        return intToState(statePrev);
    }
};
int main(){
    vector<int> cells = {1,0,0,1,0,0,1,0};
    Solution sol;
    int N = 17;
    vector<int> res = sol.prisonAfterNDays(cells, N);
    return 0;
}
