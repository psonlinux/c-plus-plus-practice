/* T : O(n^4) board dimension n x n
S : O(n ^ 2)
*/
class Solution
{
    /* converts a cordinate to integer */

    bool dfs(int i, int j, int wi, string &word, vector<vector<char>> &board)
    {
        //printf("%i %i %i\n", i, j, wi);
        if (i < 0 || i >= board.size() || j < 0 || j >= board[0].size())
            return false;
        if ((board[i][j] & 128) == 0 && board[i][j] == word[wi])
        {
            if (wi == word.size() - 1) // last char of string, no need for further exploration
                return true;

            board[i][j] ^= 128; // i, j is now visited
            // explore
            // left
            if (dfs(i, j - 1, wi + 1, word, board))
            {
                return true;
            }

            // right
            else if (dfs(i, j + 1, wi + 1, word, board))
            {
                return true;
            }

            //bottom
            else if (dfs(i - 1, j, wi + 1, word, board))
            {
                return true;
            }

            //up
            else if (dfs(i + 1, j, wi + 1, word, board))
            {
                return true;
            }

            // unchoose this location
            else
            {
                board[i][j] ^= 128;
                return false;
            }
        }
        return false;
    }

  public:
    bool exist(vector<vector<char>> &board, string word)
    {
        int r = board.size();
        int c = board[0].size();

        for (int i = 0; i < r; i++)
        {
            for (int j = 0; j < c; j++)
            {
                if (dfs(i, j, 0, word, board))
                    return true;
            }
        }
        return false;
    }
};