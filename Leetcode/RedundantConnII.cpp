//
//  RedundantConnII.cpp
//  Leetcode
//
//  Created by Prashant Singh on 14/05/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//
#include <iostream>
#include <vector>
#include <unordered_map>
#include <cfloat>
using namespace std;

struct State{
    int parent, size;   // every vertex has parent and no. of decendants
};
typedef struct State state;

class Solution {
    unordered_map<int, state> vtx;
    int component;      //total connected component in graph
    
    /* find set in which vertex x is present
     */
    int find(int x){
        int p = vtx[x].parent;
        while(p != vtx[p].parent){
            p = vtx[p].parent;
        }
        
        vtx[x].parent = p;
        return p;
    }
    
    /*
     put both vertices u and v in same set .
     */
    void unite(int u, int v){
        component -= 1;
        // attach smaller tree to bigger tree
        if(vtx[u].size > vtx[v].size){
            vtx[v].parent   = u;
            vtx[u].size     += vtx[v].size;
        }
        else{
            vtx[u].parent   =  v;
            vtx[v].size     += vtx[u].size;
        }
    }
    
    vector<int> findRedundantConnection(vector<vector<int>>& edges) {
        vector<int> res;
        int E = edges.size();
    
        //iterate over all edges
        int p1, p2, u, v;
        unordered_map<int, state>::iterator p, m;
        for(int e = 0; e < E; e++){
            u = edges[e][0];
            v = edges[e][1];
            p = vtx.find(u);
            m = vtx.find(v);
            if(p == vtx.end()){
                vtx[u] = {u, 1};
            }
            
            if(m == vtx.end()){
                vtx[v] = {v, 1};
            }
            
            p1 = find(u);
            p2 = find(v);
            if(p1 == p2){     // this edge create cycle
                res.push_back(u);
                res.push_back(v);
                break;
            }
            else{
                unite(p1, p2);
            }
        }
        return res;
    }
public:
    vector<int> findRedundantDirectedConnection(vector<vector<int>>& edges) {
        vector<int> res(2);
        
        // case 1 :a node w , has two parent say u, v
        // so we can delete one edge (u, w) or (v, w) to get a tree
        int E = edges.size();
        vector<pair<int, int>> parent(E + 1);  // vertex = E
        int u, v;     // vwtp = vertex with two parent
        int e1 = -1, e2 = -1;
        for(int e = 0; e < E; e++){
            u = edges[e][0];
            v = edges[e][1];
            if(parent[v].first == 0){
                parent[v].first     = u;       // found vertex with two parent
                parent[v].second    = e;
            }
            else{
                e1 = parent[v].second;
                e2 = e;
            }
        }
        
        if(e1 == -1 && e2 == -1){
            // case 2: cycle
            component = E;
            vtx.clear();
            res = findRedundantConnection(edges);
            if(res.size() == 2){
                return res;
            }
        }
        
        // try removing these two edge one by one and see if we get
        // the tree
        vector<int> fe = edges[e1], se = edges[e2];
        
        // create new edges without these two
        vector<vector<int>> newEdges;
        for(int e = 0; e < E; e++){
            if(e != e1 && e != e2){
                newEdges.push_back(edges[e]);
            }
        }
        
        newEdges.push_back(fe);
        component = E;
        vtx.clear();
        res = findRedundantConnection(newEdges);
        if(res.size() == 0 && component == 1){
            return se;
        }
        
        // remove other edge
        newEdges.pop_back();
        newEdges.push_back(se);
        component = E;
        vtx.clear();
        res = findRedundantConnection(newEdges);
        if(res.size() == 0 && component == 1){
            return fe;
        }
        return res;
    }
};


int main(){
    vector<vector<int>> edges = {{1,2}, {2,3}, {3,4}, {4,1}, {1,5}};
    Solution sol;
    vector<int> v = sol.findRedundantDirectedConnection(edges);
    printf("%i %i\n", v[0], v[1]);
    return 0;
}
