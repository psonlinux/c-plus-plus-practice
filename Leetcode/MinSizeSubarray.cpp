/*
T : O(A.size())
S : O(1)
*/
class Solution {
   public:
    int minSubArrayLen(int s, vector<int>& A) {
        int n = A.size();
        if (s == 0 || n == 0) {
            return 0;
        }

        int i = 0, j = 0;
        int curSum = A[j], gSize = n + 1;
        j += 1;

        // Find minisum window at each i
        while (i < n) {
            //increment j to increase curSum
            while (j < n && curSum < s) {
                curSum += A[j];
                j += 1;
            }

            // is this window minimum
            if (curSum >= s) {
                if (j - i < gSize) {
                    gSize = j - i;
                }
            }

            curSum -= A[i];
            i += 1;
        }

        if (gSize == n + 1) {
            return 0;
        }
        return gSize;
    }
};