/*
 https://leetcode.com/problems/first-unique-character-in-a-string/
 T : O(n)
 S : O(n)
 Constant time query for first unique char for streaming string
 */
/*O(n)*/
struct Node{
    char c;
    int ind;
    Node * next, * prev;
    Node(char c, int ind){
        this->c     = c;
        this->ind   = ind;
        next        = NULL;
        prev        = NULL;
    }
};
class Solution {
public:
    int firstUniqChar(string s) {
        unordered_map<char, Node *> seen;
        Node * head = new Node(-1, -1), * tail = head;
        int n = s.size();
        
        if(n == 0){
            return -1;
        }
        
        if(n == 1){
            return 0;
        }
        for(int i = 0; i < n; i++){
            // not seen , then add to both map
            auto it = seen.find(s[i]);
            if(it == seen.end()){
                Node * node = new Node(s[i], i);
                node->prev = tail;
                tail->next = node;
                tail = tail->next;
                
                //store in um
                seen[s[i]] = node;
            }
            else if(it->second != NULL){
                Node * addr = it->second;
                it->second = NULL;
                
                if(tail == addr){
                    tail = tail->prev;
                }
                
                Node * prevNode = addr->prev;
                prevNode->next = addr->next;
                if(addr->next){
                    addr->next->prev = prevNode;
                }
            }
        }
        
        
        if(head->next == NULL){
            return -1;
        }
        return head->next->ind;
    }
};
