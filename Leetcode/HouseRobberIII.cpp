/** https://leetcode.com/problems/house-robber-iii/
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/*
T : O(n)
S : O(n)
*/
class Solution {
    /* Keep sum of alternate leveles in m1 and m2
    */
    int maxMoney(TreeNode *n, unordered_map<TreeNode *, int> & cache){
       if(n == NULL){
           return 0;
       }
       
       if(n->left == NULL && n->right == NULL)  //leaf node
            return n->val;
            
        if(cache.find(n) != cache.end())
            return cache[n];
        
        // case 1 : excluding this house
        int m1 = maxMoney(n->left, cache) + maxMoney(n->right, cache);    //left and right nods are not adjacent

        // case 2 : including this house
        //this house money + next non adjacent house money
        int m2 = n->val;
        if(n->left){
            m2 += maxMoney(n->left->left, cache) + maxMoney(n->left->right, cache);    
        }

        if(n->right){
            m2 += maxMoney(n->right->left, cache) + maxMoney(n->right->right, cache);
        }

        cache[n] = max(m1, m2);
        return cache[n];
    }
public:
    int rob(TreeNode* root) {
        if(root == NULL)
            return 0;
        unordered_map<TreeNode *, int> cache;
        return maxMoney(root, cache);
    }
};