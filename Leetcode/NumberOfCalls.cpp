/*
Time : O(1)
Space : O(1)
*/
class RecentCounter {
    queue<int> pings;
public:
    RecentCounter() {
        
    }
    
    int ping(int t) {
        int lb = 3000;
        pings.push(t);
        int x = t - lb;
        while(!pings.empty() && pings.front() < x)
            pings.pop();
        return pings.size();
    }
};

/**
 * Your RecentCounter object will be instantiated and called as such:
 * RecentCounter* obj = new RecentCounter();
 * int param_1 = obj->ping(t);
 */