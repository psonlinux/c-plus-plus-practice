//
//  RegExp.cpp
//  Leetcode
//
//  Created by Prashant Singh on 01/05/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//
#include <iostream>
#include <string>
#include <vector>
using namespace std;

/*
 T : O(s.size() * p.size())
 S : O(s.size() * p.size())
 */
class Solution {
    int k = 0;
    bool isMatch(string & s, int i, string & p, int j, vector<vector<short>> & dp) {
        cout << k << endl;
        k += 1;
        if(dp[i][j] != -1){
            return dp[i][j];
        }
        
        if(i < s.size() && j >= p.size()){    // s = "asd" p = ""
            dp[i][j] = false;
            return false;
        }
        
        if(j < p.size() - 1 && p[j + 1] == '*'){   // second char is '*'
            if(isMatch(s, i, p, j + 2, dp)){
                dp[i][j] = true;
                return true;
            }
            if(i < s.size() && s[i] == p[j] && isMatch(s, i + 1, p, j, dp)){
                dp[i][j] = true;
                return true;
            }
            if(i < s.size() && p[j] == '.' && isMatch(s, i + 1, p, j, dp)){
                dp[i][j] = true;
                return true;
            }
        }
        else{
            if(i >= s.size()){  // s is empty and p can't generate empty string
                dp[i][j] = false;
                return false;   // s = "" , p = "aw..m"
            }
            if(p[j] == '.' && i < s.size()){
                if(isMatch(s, i + 1, p, j + 1, dp)){
                    dp[i][j] = true;
                    return true;
                }
            }
            if(i < s.size() && s[i] == p[j]){
                if(isMatch(s, i + 1,  p, j + 1, dp)){
                    dp[i][j] = true;
                    return true;
                }
            }
        }
        dp[i][j] = false;
        return false;
    }
    
public:
    bool isMatch(string s, string p){
        vector<vector<short>> dp(s.size() + 1, vector<short>(p.size() + 1, -1));
        // dp[i][j] = true, means s[i...s.size() - 1] matches with p[j...p.size() - 1]
        dp[s.size()][p.size()] = true;
        return isMatch(s, 0, p, 0, dp);
    }
};

int main(){
    string s = "aaaaaaab", p = "a*a*a*a*a*a*c";
    Solution sol;
    if(sol.isMatch(s, p)){
        cout << "Matched\n";
    }
    else{
        cout << "Not Matched\n";
    }
    return 0;
}
