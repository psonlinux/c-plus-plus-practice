/*
https://leetcode.com/problems/self-dividing-numbers/
*/
class Solution {
public:
    vector<int> selfDividingNumbers(int left, int right) {
        vector<int> sol;
        for(int i = left; i <= right; i++){
            int x = i, d;
            while(x){
                d = x%10;
                if(d == 0 || i%d != 0)break;
                x = x/10;
            }
            if(x == 0)sol.push_back(i);
        }
        return sol;
    }
};