/*T : O(n^2)
S : O(n)
*/
class Solution {
    int minJump(int i, vector<int> & nums, vector<int> & cache){
        if(cache[i] != -1)
            return cache[i];
        if(i == nums.size() - 1){    // start index is the last index
            cache[i] = 0;
            return cache[i];
        }
        if(i + nums[i] >= nums.size() - 1){    // reached last index in single 
            cache[i] = 1;
            return cache[i];
        }
        
        int mn = nums.size();      // max jump possible
        for(int j = i + 1; j <= i + nums[i]; j++){
            int x = minJump(j, nums, cache);
            if(x < mn) mn = x;
        }
        
        cache[i] = mn + 1;
        return cache[i];
    }
    
public:
    int jump(vector<int>& nums) {
        vector<int> cache(nums.size(), -1);
        return minJump(0, nums, cache);
    }
};