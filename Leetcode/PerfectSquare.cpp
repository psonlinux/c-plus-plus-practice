/* https://leetcode.com/problems/perfect-squares/
T : O(n * sqrt(n))
S : O(n)
*/
class Solution {
public:
    int numSquares(int n) {
        vector<int> t(n + 1);
        t[0] = 0;
        t[1] = 1;
        for(int i = 2; i < n + 1; i++){
            int c = i;      // min count of used perfect squares 
            
            for(int j = 1; j * j <= i; j++){
                c = min(c, 1 + t[i - j * j]);
            }
            
            t[i] = c;       //save min count
        }
        return t[n];
    }
};