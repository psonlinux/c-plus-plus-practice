/*
T : O(n)
S : O(k)
*/
class Solution {
   public:
    vector<int> maxSlidingWindow(vector<int>& v, int k) {
        int n = v.size();
        vector<int> res;  // output
        if (n == 0) {
            return res;
        }

        // initialize deque
        deque<int> dq;
        for (int i = 0; i < n; i++) {
            // make place for new item
            if (dq.empty() == false && dq.front() <= i - k) {
                dq.pop_front();
            }

            // remove all item <= v[i] from deque
            while (dq.empty() == false && v[dq.back()] <= v[i]) {
                dq.pop_back();
            }
            dq.push_back(i);

            if (i >= k - 1) {
                res.push_back(v[dq.front()]);
            }
        }
        return res;
    }
};