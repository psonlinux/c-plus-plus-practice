// https://leetcode.com/problems/linked-list-cycle-ii/
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
 /*
 T : O(n)
 S : O(1)
 */
class Solution {
public:
    ListNode *detectCycle(ListNode *head) {
        if(head == NULL || head->next == NULL)  // no cycle
            return NULL;
        
        ListNode *slow = head, *fast = head;   // e is entry node
        while(fast && fast->next){
            slow = slow->next;
            fast = fast->next->next;
            if(slow == fast)    break;
        }
        
        if(slow != fast)    return NULL;    // no cycle
        
        
        // now slow and fast points to same node, if cycle exist
        //  move entry and slow node until both points to same node
        ListNode * entry = head;
        while(slow){
            if(slow == entry)   break;
            slow    = slow->next;
            entry   = entry->next;
        }
        return entry;
    }
};