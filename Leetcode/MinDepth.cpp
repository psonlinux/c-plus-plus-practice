/** https://leetcode.com/problems/minimum-depth-of-binary-tree/
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/*
T : O(nodes)
S : O(nodes)
*/
class Solution {
    void minDepth(TreeNode *n, int d, int  & md){
        if(n){
            if(n->left == NULL && n->right == NULL){
                if(d < md)md = d;
            }
            minDepth(n->left, d + 1, md);
            minDepth(n->right, d + 1, md);
        }  
    }
public:
    int minDepth(TreeNode* root) {
        if(root == NULL)return 0;
        int md = INT_MAX;   //intial minimum depth is infinity
        minDepth(root, 1, md);
        return md;
    }
};