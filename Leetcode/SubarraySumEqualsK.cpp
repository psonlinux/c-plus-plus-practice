//
//  SubarraySumEqualsK.cpp
//  Leetcode
//
//  Created by Prashant Singh on 22/07/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

class Solution {
public:
    int subarraySum(vector<int>& A, int k) {
        int n = A.size();
        unordered_map<int, int> map;
        
        int c = 0, sum = 0;
        for(int i = 0; i < n; i++){
            sum += A[i];
            
            //find all prefixes. Counts all proper suffix of A[0...i] which adds to k
            auto it = map.find(sum - k);
            if(it != map.end()){
                c += it->second;
            }
            
            // count 1 more if whole A[0...i] == k
            if(sum == k){
                c += 1;
            }
            
            map[sum] += 1;  // now add this prefix sum
        }
        return c;
    }
};
