/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/* .    1
      /   \
    null   1
Idea : inorder traversal produce the BST in sorted order .
If any node value is <= than it's previous node then return false;
T : O(n)
S : O(n)
*/
class Solution {
    bool inorder(TreeNode *r, TreeNode * &prev){
        if(r){
            bool left = inorder(r->left, prev);
            bool mid = !prev || (prev && r->val > prev->val);
            prev = r;
            bool right = inorder(r->right, prev);
            return left && mid && right;
        }
        return true;
    }
public:
    bool isValidBST(TreeNode* r) {
        TreeNode * prev = NULL;
        return inorder(r, prev);
    }
};