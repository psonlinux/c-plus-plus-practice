/*
KMP
T O(n)
S O(string size)
*/
class Solution {
    // Create longest prefix suffix table table
    // lps[i] = length of longest proper suffix of pat[0...i] which is also a proper prefix
    /* A B C D A B C Y A
       0 0 0 0 1 2 3 0 1
    */
    vector<int> computeLps(string & pat){
        int n = pat.size();
        vector<int> lps(n);
        for(int i = 1; i < n; i++){
            if(pat[i] == pat[0]){   // pat[i] is sffix and prefix
                lps[i] = 1;
            }
            if(pat[i] == pat[lps[i - 1]]){  // pat[i] is part of longer prefix
                lps[i] = lps[i - 1] + 1;
            }
        }
        return lps;
    }
    
    /* Search a substring in given text . Return its index if found.
    Return -1 if not found .
    */
    int KMP(string & text, string & pat, vector<int> & lps){
        int j = 0, n = text.size();
        for(int i = 0; i < n; i++){
            if(text[i] == pat[j]){  
                j += 1;
            }
            else if(j != 0){
                j = lps[j - 1];
                i -= 1;
            }
        }
        return j;
    }
public:
    string shortestPalindrome(string pat) {
        // string of size 0 and 1 are already shortest palindrome
        if(pat.size() < 2){ 
            return pat;
        }
        
        string text = pat;
        reverse(text.begin(), text.end());
        
        // lps table
        vector<int> lps = computeLps(pat);
        
        // Use KMP to find pat in text
        int k = KMP(text, pat, lps);
        for(int i = k; i < pat.size(); i++){
            text.push_back(pat[i]);
        }
        return text;
    }
};