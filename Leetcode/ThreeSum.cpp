/*
T : O(n^2)
S : O(n)
*/
class Solution {
public:
    vector<vector<int>> threeSum(vector<int>& nums) {
        int n = nums.size();
        vector<vector<int>> triplets;
        if(n < 3)   return triplets;
        
        sort(nums.begin(), nums.end());     // nlogn
        for(int i = 0; i < n - 2; i++){     // n^2
            int tgt = -nums[i];
            int lo = i + 1, hi = n - 1;
            while(lo < hi){
                if(nums[lo] + nums[hi] == tgt){
                    vector<int> v(3, 0);
                    v[0] = nums[i];
                    v[1] = nums[lo];
                    v[2] = nums[hi];
                    triplets.push_back(v);
                    lo += 1;
                    hi -= 1;
                    
                    //skip same no.
                    while(lo < hi && nums[lo] == v[1])   lo += 1;
                    while(lo < hi && nums[hi] == v[2])   hi -= 1;
                }
                else if(nums[lo] + nums[hi] < tgt)
                    lo += 1;
                else
                    hi -= 1;
            }
            
            // skip same i
            while(i < n - 2 && nums[i + 1] == nums[i])
                i += 1;
        }
        return triplets;
    }
};