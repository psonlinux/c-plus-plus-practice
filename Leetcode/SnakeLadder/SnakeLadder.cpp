/* https://practice.geeksforgeeks.org/problems/snake-and-ladder-problem/0 */
#include<iostream>
#include<vector>
#include<string>
#include<unordered_map>
#include<queue>
#include<unordered_set>
#include<algorithm>
using namespace std;

struct Info{
    int pos, d;
    Info(int pos, int d){
        this->pos = pos;
        this->d = d;
    }

    Info(){}
};

bool isValid(int x, int N, vector<bool> & vis){
    if(x > N || vis[x]){
        return false;
    }
    return true;
}

int minSteps(unordered_map<int, int> &go){
    int N = 30;
    vector<bool> vis(N + 1);
    queue<Info> q;
    Info start(1, 0);
    vis[1] = true;

    q.push(start);

    while(q.empty() == false){
        Info x = q.front();
        q.pop();
        if(x.pos == N){
            return x.d; //found
        }

        for(int i = 1; i <= 6; i++){
            if(isValid(x.pos + i, N, vis)){
                q.push(Info(x.pos + i, x.d + 1));
                vis[x.pos + i] = true;

                if(go.find(x.pos + i) != go.end()){     //ladder or snake
                    q.push(Info(go[x.pos + i], x.d + 1));
                    vis[go[x.pos + i]] = true;
                }
            }
        }
    }
    return -1;
}

int main() {
	int T = 0, t = 0;
	cin >> T;
	while(t < T){
	    int n = 0;
	    cin >> n;
	    unordered_map<int, int> go;
	    for(int i = 0; i < n; i++){
	        int u, v;
	        cin >> u >> v;
	        go[u] = v;
	    }
	    cout << minSteps(go) << endl;
	    t += 1;
	}
	return 0;
}
