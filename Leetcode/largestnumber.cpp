/*
4 , 43  -> 443
4, 46   -> 464
[128,12]
T : O(n log n)  , n = # of ints
S : O(# of strings)
*/
class Solution {
    string removeLeadingZero(string& s) {
        int i = 0;
        // skip zero
        for (i = 0; i < s.size(); i++) {
            if (s[i] != '0') {
                return s.substr(i);
            }
        }
        return "0";
    }

    int partition(vector<string>& v, int lo, int ub) {
        int i = lo + 1, j = ub - 1;
        while (true) {
            while (i <= j && compare(v[i], v[lo]) > 0) {
                i += 1;
            }

            while (j >= i && compare(v[j], v[lo]) < 0) {
                j -= 1;
            }

            if (i > j) {
                break;
            }

            swap(v[i], v[j]);
            i += 1;
            j -= 1;
        }

        swap(v[lo], v[j]);
        return j;
    }

    void quicksort(vector<string>& v, int p, int q) {
        if (q - p <= 1) {
            return;
        }
        int par = partition(v, p, q);
        quicksort(v, p, par);
        quicksort(v, par + 1, q);
    }

    static int compare(string& s, string& t) {
        return (s + t > t + s);
    }

   public:
    string largestNumber(vector<int>& nums) {
        // convert all ints to strings
        vector<string> vs;
        for (int i = 0; i < nums.size(); i++) {
            vs.push_back(to_string(nums[i]));
        }

        string res;
        //quicksort(vs, 0, vs.size());
        sort(vs.begin(), vs.end(), compare);
        for (int i = 0; i < vs.size(); i++) {
            res += vs[i];
        }

        // remove leading zeros
        res = removeLeadingZero(res);
        return res;
    }
};