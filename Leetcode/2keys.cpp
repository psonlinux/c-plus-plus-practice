class Solution {
    int minOp(int text, int n, vector<int> & memo){
        if(text == n){  // no op is required if text already contain n A's
            return 0;
        }
        
        if(text + text == n){   // copy, paste
            return 2;
        }
        
        if(text + text > n){    // not possible
            return n;
        }
        
        if(memo[text] != -1){
            return memo[text];
        }
        
        int minOperation = n;    // max operation possible is n
        int op = 1, x = 0, buffer = text;
        while(buffer <= n){
            buffer  += text;
            op      += 1;       // count paste
            x       = op + minOp(buffer, n, memo);
            minOperation = min(minOperation, x);
        }
        memo[text] = minOperation;
        return minOperation;
    }
public:
    int minSteps(int n) {
        int text = 1;   // text has 1 A
        vector<int> memo(n + 1, -1);
        memo[n] = 0;
        return minOp(text, n, memo);
    }
};
