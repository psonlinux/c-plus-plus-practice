/*
T : O(n)
S : O(1)
*/
class Solution {
   public:
    int numDecodings(string s) {
        int n = s.size();
        if (n == 0) {
            return 0;
        }

        if (s[0] == '0') {  // message starting with zero can not be decoded
            return 0;
        }

        if (n == 1) {
            return 1;
        }

        int p1 = 1;  // num of decodings of s[0...i - 2]
        int p2 = 1;  // num of decodings of s[0...i - 1]

        for (int i = 1; i < n; i++) {
            int x = 0;
            if (s[i] != '0') {
                x = p2;
            }

            int ld = stoi(s.substr(i - 1, 2));
            if (ld <= 26 && ld > 9) {
                x += p1;
            }

            p1 = p2;
            p2 = x;
        }
        return p2;
    }
};