/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

class Solution {
public:
    TreeNode * trimBST(TreeNode *r, int L, int R){
        if(r){
            if(r->val < L)
                return trimBST(r->right, L, R);
            else if(r->val > R)
                return trimBST(r->left, L, R);
            else{
                r->left = trimBST(r->left, L, R);
                r->right = trimBST(r->right, L, R);
            }
        }
        return NULL;
    }
};

/*
class Solution {
public:
    bool isInRange(TreeNode *node, int L, int R){
        return node->val >= L && node->val <= R ;
    }
    
    TreeNode* trimBST(TreeNode* root, int L, int R) {
        //find a node which lie in [L, R]
        TreeNode *node = root;
        while(node && !isInRange(node, L, R)){
            if(node->val > R)
                node = node->left;
            else
                node = node->right;
        }
        
        //expand from that node to find lb and rb .
        //move lb and rb in opposite direction .
        TreeNode *lb = node, *rb = node;
        while(true){
            
            if(lb->left){
                if(lb->left->val >= L)
                    lb = lb->left;
                else{
                    lb->left = lb->left->right;
                }
            }
            
            if(rb->right){
                if(rb->right->val <= R)
                    rb = rb->right;
                else
                    rb->right = rb->right->left;
            }            
            
            //reached left and right boundary
            if( (!lb->left) && (!rb->right) )
                break;
        }
        return node;
    }
};
*/