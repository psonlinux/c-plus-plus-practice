/** https://leetcode.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal/
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/* 
T : O(nlogn)
S : O(n)
*/
class Solution {
    TreeNode * makeTree(vector<int>& p, int pl, int pr, vector<int>& in, int il, int ir){
        if(pl > pr)
            return NULL;
        
        if(pl == pr){
            TreeNode * node = new TreeNode(p[pl]);
            return node;
        }
        
        TreeNode * r = new TreeNode(p[pl]);
        
        //find root in inorder
        int i;
        for(i = il; i <= ir; i++){
            if(in[i] == p[pl])
                break;
        }
        
        //make left subtree
        int countLeft = i - il;         // nodes in left subtree
        r->left     = makeTree(p, pl + 1, pl + countLeft, in, il, i - 1);
        
        // make right subtree
        int cr = ir - i;                // nodes in right subtree
        r->right    = makeTree(p, pl + 1 + countLeft, pr, in, i + 1, ir);
        return r;
    }
public:
    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
        return makeTree(preorder, 0, preorder.size() - 1, inorder, 0, inorder.size() - 1);
    }
};