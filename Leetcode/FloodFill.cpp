/*
https://leetcode.com/problems/flood-fill/
Time : O(items in 2D array)
Space : O(items in 2D array)
*/
class Solution {
public:
    void dfs(vector<vector<int>>& image, int sr, int sc, int newColor){
        int curColor = image[sr][sc];
        image[sr][sc] = newColor;
        int m = image.size();
        int n = image[0].size();
        if(sr - 1 >= 0 && image[sr - 1][sc] == curColor)dfs(image, sr - 1, sc, newColor);
        if(sr + 1 <  m && image[sr + 1][sc] == curColor)dfs(image, sr + 1, sc, newColor);
        if(sc - 1 >= 0 && image[sr][sc - 1] == curColor)dfs(image, sr, sc - 1, newColor);
        if(sc + 1 <  n && image[sr][sc + 1] == curColor)dfs(image, sr, sc + 1, newColor);
        
    }
    
    vector<vector<int>> floodFill(vector<vector<int>>& image, int sr, int sc, int newColor) {
        if(image.size() == 0)return image;
        if(image[sr][sc] == newColor)return image;
        dfs(image, sr, sc, newColor);
        return image;
    }
};