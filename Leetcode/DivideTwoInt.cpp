class Solution {
    long long divs(long long& dd, long long dr) {
        if (dd < dr) {
            return 0;
        }

        long long q = 1;
        // stop when dd becomes less than divisor
        while (dr + dr <= dd) {
            dr = dr + dr;  // double every time
            q = q + q;     // count of dr
        }
        dd = dd - dr;
        return q;
    }

   public:
    int divide(int d, int r) {
        long long dd = d, dr = r;
        // handle sign of result
        int sign = 1;
        if (dd < 0) {
            sign *= -1;
            dd *= -1;
        }
        if (dr < 0) {
            sign *= -1;
            dr *= -1;
        }

        // simple case when dr = 1
        if (dr == 1) {
            if (dd > INT_MAX) {
                if (sign == 1)
                    return INT_MAX;
                else
                    return INT_MIN;
            }
            return sign * dd;
        }

        long long q = 0, count = 0;
        while (dd >= dr) {
            count = divs(dd, dr);
            q += count;
        }

        return (sign < 0) ? -q : q;
    }
};