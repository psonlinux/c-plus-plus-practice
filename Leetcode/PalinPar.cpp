/*
T : O(n^2) considered every substring
S : O(n) on stack
*/
class Solution {
    bool isPal(string &s, int start, int end) {
        while (start < end) {
            if (s[start] != s[end]) return false;
            start += 1;
            end -= 1;
        }
        return true;
    }

    void getAllPartitions(string &s, int start, vector<string> &par,
                          vector<vector<string>> &partitions) {
        if (start == s.size()) {
            partitions.push_back(par);
        }
        // check if s[0...i] is palindrome, then save it and get the
        // partitions of s[i + 1 ... s.size() - 1]
        for (int i = start; i < s.size(); i++) {
            if (isPal(s, start, i)) {
                par.push_back(s.substr(start, i - start + 1));  // save it

                // get partion of s[i + 1 ... end]
                getAllPartitions(s, i + 1, par, partitions);

                // unchoose s[start ... i]
                par.pop_back();
            }
        }
    }

   public:
    vector<vector<string>> partition(string s) {
        vector<vector<string>> partitions;
        vector<string> par;
        getAllPartitions(s, 0, par, partitions);
        return partitions;
    }
};