/*
https://leetcode.com/problems/shortest-distance-to-a-character/
*/
/*
Time : O(n)
*/
class Solution {
public:
    vector<int> shortestToChar(string S, char C) {
        int n = S.size();
        vector<int> d(n, n);    //holds distance of character
        int pos = -n;   //initial pos of C
        
        //pass from left to right
        for(int i = 0; i < n; i++){
            if(S[i] == C)pos = i;
            d[i] = min(d[i], abs(i - pos));
        }
        
        //pass from right to left
        for(int i = n - 1; i >= 0; i--){
            if(S[i] == C)pos = i;
            d[i] = min(d[i], abs(i - pos));
        }
        
        return d;
    }
};