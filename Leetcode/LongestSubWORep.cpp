/*
T : O(s.size())
S : O(s.size())
*/
class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        if(s.size() == 0)   return 0;
        int lsl = 1, start = 0, i;    // longest substring len
        unordered_map<char, int> sub;
        for(i = 0; i < s.size(); i++){
            if(sub.find(s[i]) != sub.end() && sub[s[i]] >= start){    // char already visited
                start = sub[s[i]] + 1; //remove that char and all before
            }
            sub[s[i]] = i;    // save char and its index
            if(i - start + 1 > lsl)    // is current substring greater
                lsl = i - start + 1;  
        }
        return lsl;
    }
};