/*
https://leetcode.com/problems/middle-of-the-linked-list/
Time : O(n)
Space : O(1)
*/
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* middleNode(ListNode* head) {
        if(!head->next)
            return head;
        
        ListNode *p = head, *q = head->next;
        
        //keep moving p and q if q is not NULL
        while( q != NULL){
            //q moves two steps
            q = q->next;
            if (q)
                q = q->next;
            
            //p moves one step
            p = p->next;
        }
        return p;
    }
};