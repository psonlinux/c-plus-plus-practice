/* https://leetcode.com/problems/task-scheduler/
["G","C","A","H","A","G","G","F","G","J","H","C","A","G","E","A","H","E","F","D","B","D","H","H","E","G","F","B","C","G","F","H","J","F","A","C","G","D","I","J","A","G","D","F","B","F","H","I","G","J","G","H","F","E","H","J","C","E","H","F","C","E","F","H","H","I","G","A","G","D","C","B","I","D","B","C","J","I","B","G","C","H","D","I","A","B","A","J","C","E","B","F","B","J","J","D","D","H","I","I","B","A","E","H","J","J","A","J","E","H","G","B","F","C","H","C","B","J","B","A","H","B","D","I","F","A","E","J","H","C","E","G","F","G","B","G","C","G","A","H","E","F","H","F","C","G","B","I","E","B","J","D","B","B","G","C","A","J","B","J","J","F","J","C","A","G","J","E","G","J","C","D","D","A","I","A","J","F","H","J","D","D","D","C","E","D","D","F","B","A","J","D","I","H","B","A","F","E","B","J","A","H","D","E","I","B","H","C","C","C","G","C","B","E","A","G","H","H","A","I","A","B","A","D","A","I","E","C","C","D","A","B","H","D","E","C","A","H","B","I","A","B","E","H","C","B","A","D","H","E"...
1

Output = 1001
Expected = 1000
*/

class Solution {
    static bool sortinrev(const pair<int,char> &a,  const pair<int,char> &b) 
    { 
       return (a.first > b.first); 
    } 
public:
    int leastInterval(vector<char>& tasks, int n) {
        //count frequency of each char
        int p = tasks.size();
        unordered_map<char, int> m;
        for(int i = 0; i < p; i++)
            m[tasks[i]] += 1;
        
        //put in priority queue , to get max occuring task first .
        vector< pair<int, char> > v;
        for(auto it = m.begin(); it != m.end(); it++){
            v.push_back(make_pair(it->second,  it->first));
        }
        
        //sort
        sort(v.begin(), v.end(), sortinrev);
        
        int li = 0;     //least interval
        while(v.size() > 0){
            //retrieve max task and interleave other task in beween
            pair<int, char> task = v.front();
            v.erase(v.begin());
            
            while(task.first > 0){
                li += 1;
                task.first -= 1;
                int gap = 0;
                auto it = v.begin();
                while(it != v.end() && gap < n){
                    li += 1;
                    gap += 1;
                    it->first -= 1;    //task completed, remove from queue

                    //delete this task if its count is zero
                    if(it->first == 0) it = v.erase(it);
                    else                it++;     
                }
                
                if(v.size() > 0 || task.first > 0)
                    li += (n - gap);
            }
        }
        return li;
    }
};