/* T : O(logn)
S : O(n)
*/
class Solution {
public:
    int search(vector<int>& v, int t) {
        int n = v.size();
        if(n == 0)  return -1;
        int lo = 0, hi = n - 1;
            
        // find min item
        int p = 0;      //assuming min item is the pivot
        while(lo <= hi){
            p = (lo + hi) / 2;
            if(v[p] > v[hi])                     lo = p + 1;
            else if (v[p] < v[hi])               hi = p;
            else if(p == 0 || v[p] < v[p - 1])   break;
        }
        
        // find range in which target lies
        lo = 0, hi = p - 1;
        if(t >= v[p] && t <= v[n - 1]){  //target lies in right side of min item
            lo = p;
            hi = n - 1;
        }
        
        int mid;
        while(lo <= hi){
            mid = (lo + hi) / 2;
            if(t == v[mid])     return mid;
            else if(t < v[mid]) hi = mid - 1;
            else                lo = mid + 1;
        }
        
        return -1;
    }
};