//
//  DailyTemp.cpp
//  Leetcode
//
//  Created by Prashant Singh on 09/07/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

/*
 https://leetcode.com/problems/daily-temperatures/
 T : O(n), each item is pushed and popped from deque atmost 2 times . O(2 * n)
 S : O(n)
 */
class Solution {
public:
    vector<int> dailyTemperatures(vector<int>& T) {
        int n = T.size();
        if(n == 0){
            return {};
        }
        vector<int> days(n);
        
        deque<int> dq;
        
        // keep item in bag if it is smaller than prev item
        for(int i = 0; i < n; i++){
            // set result for item smaller than T[i] and then keep i in bag
            while(dq.empty() == false && T[i] > T[dq.back()]){
                days[dq.back()] = i - dq.back();
                dq.pop_back();
            }
            dq.push_back(i);
        }
        return days;
    }
};
