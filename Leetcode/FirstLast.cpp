/* https://leetcode.com/problems/find-first-and-last-position-of-element-in-sorted-array/ 
T : O(logn)
S : O(n)
*/
class Solution {
public:
    vector<int> searchRange(vector<int>& nums, int target) {
        vector<int> pos(2, -1);
        if(nums.size() == 0)    return pos;
        int lo = 0, hi = nums.size() - 1, mid;
        //find lo end
        while(lo <= hi){
            mid = (lo + hi) / 2;
            if(target <= nums[mid])      hi = mid - 1;
            else if(target > nums[mid]) lo = mid + 1;                    
        }
        
        if(lo < nums.size() && nums[lo] == target)
            pos[0]  = lo;
        
        //find high end
        lo = 0, hi = nums.size() - 1, mid;
        while(lo <= hi){
            mid = (lo + hi) / 2;
            //cout << lo << " " << mid << " " << hi<< endl;
            if(target < nums[mid])      hi = mid - 1;
            else if(target >= nums[mid]) lo = mid + 1;                     
        }
        
        if(hi >= 0 && nums[hi] == target)
            pos[1] = hi;
        return pos;
    }
};