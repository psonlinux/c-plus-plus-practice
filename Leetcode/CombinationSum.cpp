class Solution {
    void getCombinations(int start, int target, vector<int> & v, vector<int>& C, vector<vector<int>> & res){
        if(target == 0){
            res.push_back(v);
        }
        
        if(target < 0){
            return;
        }
        
        if(start == C.size() && target != 0){
            return ;
        }
        
        for(int i = start; i < C.size(); i++){
            v.push_back(C[i]);
            getCombinations(i, target - C[i], v, C, res);
            v.pop_back();
        }
    }
public:
    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
        vector<vector<int>> res;
        vector<int> v;
        getCombinations(0, target, v, candidates, res);
        return res;
    }
};
