//https://leetcode.com/problems/all-oone-data-structure/
struct KeyNode{
    string key;
    struct KeyNode *prev, *next;
    struct ValueNode * bucket;

    KeyNode(string _key){
        key = _key;
        prev = NULL;
        next = NULL;
        bucket = NULL;
    }

    KeyNode(){
        key = "";
        prev = NULL;
        next = NULL;
        bucket = NULL;
    }
};

struct ValueNode{
    int val;
    struct ValueNode *prev, *next;
    struct KeyNode * item;
    ValueNode(int _val){
        val = _val;
        prev = NULL;
        next = NULL;
        item = NULL;
    }

    ValueNode(){
        val = -1;
        prev = NULL;
        next = NULL;
        item = NULL;
    }
};


class AllOne {
    unordered_map<string, KeyNode *> keyToPtr;
    ValueNode * buckets = NULL, * bucketsTail = NULL;

    void checkForDeletion(ValueNode * bucket){
        if(bucket == NULL){
            return;
        }

        if(bucket->item == NULL){
            //if it's the first node
            ValueNode * pb = bucket->prev;
            ValueNode * nb = bucket->next;

            //link nodes
            if(pb){
                pb->next = nb;
            }

            if(nb){
                nb->prev = pb;
            }

            // update bucketTail
            if(bucketsTail == bucket){
                bucketsTail = pb;
            }

            //update bucket head
            if(buckets == bucket){
                buckets = nb;
            }
        }
    }

    /*bucket should exist
    */
    void checkForInsertion(ValueNode * bucket){
        if(bucket->next && bucket->next->val == bucket->val + 1){
            return;
        }

        ValueNode * nextNode = bucket->next;
        ValueNode * newNode = new ValueNode(bucket->val + 1);
        newNode->next = nextNode;
        newNode->prev = bucket;
        bucket->next = newNode;
        if(nextNode){
            nextNode->prev = newNode;
        }

        //update tail
        if(bucketsTail == bucket){
            bucketsTail = bucketsTail->next;
        }
    }
public:
    /** Initialize your data structure here. */
    AllOne() {
    }

    /** Inserts a new key <Key> with value 1. Or increments an existing key by 1. */
    void inc(string key) {
        auto it = keyToPtr.find(key);
        if(it == keyToPtr.end()){
            if(buckets == NULL || buckets->val != 1){
                ValueNode *newNode = new ValueNode(1);
                newNode->next = buckets;
                buckets = newNode;
                if(newNode->next){
                    newNode->next->prev = newNode;
                }

                if(bucketsTail == NULL){
                    bucketsTail = buckets;
                }
            }

            //create new node
            KeyNode *kn = new KeyNode(key);

            //check if value 1 bucket exist
            kn->bucket = buckets;

            keyToPtr[key] = kn;

            KeyNode *rem = buckets->item;
            buckets->item = kn;
            kn->next = rem;
            if(rem){
                rem->prev = kn;
            }
        }
        else{   // key already present
            KeyNode *kn = it->second;
            ValueNode *vn = kn->bucket;
            checkForInsertion(vn);

            //insert key in next bucket
            ValueNode * nextBucket = vn->next;
            KeyNode * nextNodes = kn->next;
            KeyNode * prevNodes = kn->prev;

            if(prevNodes){
                prevNodes->next = nextNodes;
            }

            if(nextNodes){
                nextNodes->prev = prevNodes;
            }

            ValueNode * curBucket = kn->bucket;
            if(kn == curBucket->item){
                curBucket->item = kn->next;
            }

            kn->prev = NULL;
            kn->next = NULL;

            //insert into new bucket
            KeyNode * rem = nextBucket->item;
            nextBucket->item = kn;
            kn->next = rem;

            if(rem){
                rem->prev = kn;
            }

            //update node's new bucket
            kn->bucket = nextBucket;

            //check for deletion of previous bucket
            checkForDeletion(curBucket);
        }
    }

    /** Decrements an existing key by 1. If Key's value is 1, remove it from the data structure. */
    void dec(string key) {
        auto it = keyToPtr.find(key);
        if(it == keyToPtr.end()){
            return;
        }

        KeyNode *kn = it->second;

        //remove
        KeyNode *pn = kn->prev;
        KeyNode *nn = kn->next;

        if(pn){
            pn->next = nn;
        }

        if(nn){
            nn->prev = pn;
        }

        ValueNode *curBucket = kn->bucket;
        if(curBucket->item == kn){
            curBucket->item = kn->next;
        }

        if(curBucket->val == 1){
            keyToPtr.erase(key);
            checkForDeletion(curBucket);
            return;
        }

        //create previous bucket if not exist

        ValueNode * prevBucket = curBucket->prev;
        if(prevBucket == NULL || prevBucket->val != curBucket->val - 1){
            ValueNode * newNode = new ValueNode(curBucket->val - 1);
            newNode->next = curBucket;
            newNode->prev = prevBucket;

            if(prevBucket){
                prevBucket->next = newNode;
            }

            if(curBucket){
                curBucket->prev = newNode;
            }

            //update head
            if(buckets == curBucket){
                buckets = newNode;
            }
        }

        prevBucket = curBucket->prev;
        kn->next = prevBucket->item;
        if(prevBucket->item){
            prevBucket->item->prev = kn;
        }
        prevBucket->item = kn;
        kn->prev = NULL;
        kn->bucket = prevBucket;

        checkForDeletion(curBucket);
    }

    /** Returns one of the keys with maximal value. */
    string getMaxKey() {
        if(bucketsTail && bucketsTail->item){
            return bucketsTail->item->key;
        }
        return "";
    }

    /** Returns one of the keys with Minimal value. */
    string getMinKey() {
        if(buckets && buckets->item){
            return buckets->item->key;
        }
        return "";
    }
};

/**
 * Your AllOne object will be instantiated and called as such:
 * AllOne* obj = new AllOne();
 * obj->inc(key);
 * obj->dec(key);
 * string param_3 = obj->getMaxKey();
 * string param_4 = obj->getMinKey();
 */
