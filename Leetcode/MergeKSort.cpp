/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
/*
T : O((total nodes) * logk)
S : O(k)
*/
class Cmp{
    public:
    int operator() (const ListNode * p, const ListNode * q){
        return p->val >= q->val;
    }
};

class Solution {
public:
    ListNode* mergeKLists(vector<ListNode*>& lists) {
        if(lists.size() == 0)
            return NULL;
        ListNode * head = new ListNode(-1);
        ListNode * tail = head;
        
        //create a min heap for first item of every list
        priority_queue<ListNode *, vector<ListNode *>, Cmp> minHp;
        for(int i = 0; i < lists.size(); i++){
            if(lists[i] != NULL)
                minHp.push(lists[i]);
        }
        
        // pick min item out and push its next item again in queue
        while(!minHp.empty()){
            tail->next = minHp.top();   // pick min
            tail = tail->next;
            minHp.pop();                // remove min
            if(tail->next != NULL)
                minHp.push(tail->next);   
        }      
        return head->next;
    }
};