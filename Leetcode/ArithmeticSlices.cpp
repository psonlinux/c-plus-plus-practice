/*
https://leetcode.com/problems/arithmetic-slices/
Time : O(n)
Space : O(1)
*/
class Solution {
public:
    int numberOfArithmeticSlices(vector<int>& A) {
        int n = A.size();
        int total = 0;
        int count = 0;
        bool asEnd = false, asStart = false;
        for(int i = 2; i < n; i++){
            //find arithmetic series
            if( (A[i] - A[i - 1]) == (A[i - 1] - A[i - 2])){
                count++;
                asStart = true;
            }
            else{ 
                asEnd = true;
            }
            
            //count no. of sub arithmetic series
            if(asStart && asEnd){
                total += (count*(count + 1))/2;
                asStart = false;
                asEnd = false;
                count = 0;   //reset count
            }
        }
        
        //last item is a part of arithmetic series
        if(asStart)
            total += (count*(count + 1))/2;
        return total;
    }
};