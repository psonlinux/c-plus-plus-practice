/*
T : O(n)
S : O(1)
*/
/*
// Definition for a Node.
class Node {
public:
    int val;
    Node* next;
    Node* random;

    Node() {}

    Node(int _val, Node* _next, Node* _random) {
        val = _val;
        next = _next;
        random = _random;
    }
};
*/
class Solution {
   public:
    Node* copyRandomList(Node* head) {
        //Round 1: create new list where each new node will follow original node
        Node *cur = head, *temp;
        while (cur) {
            temp = cur->next;
            Node* n = new Node(cur->val, NULL, NULL);
            n->next = cur->next;
            cur->next = n;
            cur = temp;
        }

        // Round 2: set random pointer of new list nodes
        cur = head;
        while (cur) {
            temp = cur->next->next;
            if (cur->random) {
                cur->next->random = cur->random->next;
            }
            cur = temp;
        }

        // Round 3 : restore original list and separate new list nodes
        Node *dcTail = new Node(-1, NULL, NULL), *dcHead = NULL;
        cur = head;
        while (cur) {
            if (dcHead == NULL) {  // set head only first time
                dcHead = cur->next;
            }
            dcTail->next = cur->next;
            dcTail = dcTail->next;

            cur->next = cur->next->next;
            cur = cur->next;
        }

        return dcHead;  // deep copy head
    }
};