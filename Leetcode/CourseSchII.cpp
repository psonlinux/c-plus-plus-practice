class Solution {
    /* Detect cycle in graph
    */
    bool isCyclic(int c, vector<vector<int>> & prevCourse, vector<bool> & isVis, vector<bool> & stack){
        //printf("%i \n", c);
        isVis[c] = true;
        stack[c] = true;
        for(int i = 0; i < prevCourse[c].size(); i++){
            if(stack[prevCourse[c][i]]){    // detected back edge
                return true;
            }
            if(!isVis[prevCourse[c][i]] && isCyclic(prevCourse[c][i], prevCourse, isVis, stack)){
                return true;
            }
        }
        stack[c] = false;
        return false;
    }
    
    /* Get all courses in order .
    */
    void getPrereq(int c, vector<vector<int>> & prevCourse, vector<int> & order, vector<bool> & isVisited){
        isVisited[c] = true;
        //push all prerequisite of course 'c' , then push course c 
        for(int i = 0; i < prevCourse[c].size(); i++){
            if(isVisited[prevCourse[c][i]] == false){
                getPrereq(prevCourse[c][i], prevCourse, order, isVisited);
            }
        }
        order.push_back(c);
    }
    
public:
    vector<int> findOrder(int numCourses, vector<pair<int, int>>& prerequisites) {
        // make graph where u->v edge means u is the pre-requisite for v
        vector<vector<int>> prevCourse(numCourses);
        for(int i = 0; i < prerequisites.size(); i++){
            prevCourse[prerequisites[i].first].push_back(prerequisites[i].second);
        }
        
        // detect cycle
        // Return empty vector if cycle detected
        vector<bool> isVis(numCourses);
        for(int i = 0; i < numCourses; i++){
            vector<bool> stack(numCourses);
            if(isVis[i] == false){
                if(isCyclic(i, prevCourse, isVis, stack)){
                    return {};
                }
            }  
        }
        
        // create order
        vector<int> order;
        vector<bool> isVisited(numCourses);
        for(int i = 0; i < numCourses; i++){
            if(isVisited[i] == false){
                getPrereq(i, prevCourse, order, isVisited);    
            }
        }
        return order;
    }
};