class Solution {
    void permute(vector<int> & p, int lo, int hi, vector<vector<int>> & sol){
        if(lo == hi){
            sol.push_back(p);
            return ;
        }
            
            
        for(int k = lo; k <= hi; k++){
            swap(p[k], p[lo]);
            permute(p, lo + 1, hi, sol);
            swap(p[k], p[lo]);
        }
    }
public:
    vector<vector<int>> permute(vector<int>& p) {
        vector<vector<int>> sol;
        int n = p.size();
        permute(p, 0, n - 1, sol);
        return sol;
    }
};