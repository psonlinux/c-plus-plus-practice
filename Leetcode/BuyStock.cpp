/* https://leetcode.com/problems/best-time-to-buy-and-sell-stock/
T : O(n)
S : O(n)
*/
class Solution {
public:
    /*
    Either i'th day share is sold or not .
    If not sold then 
    maxPro[0...i] = maxPro[0...i-1]
    
    if sold then
    maxPro[0...i] = prices[i] - min price[0...i]
    */
    int maxProfit(vector<int>& prices) {
        if(prices.size() <= 1)
            return 0;
        int maxPro = 0;
        int minPrice = prices[0];
        for(int i = 1; i < prices.size(); i++){
            if(prices[i] < minPrice)
                minPrice = prices[i];
            maxPro = max(maxPro, prices[i] - minPrice);
        }
        return maxPro;
    }
};