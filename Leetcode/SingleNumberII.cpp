//
//  SingleNumberII.cpp
//  Leetcode
//
//  Created by Prashant Singh on 14/06/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

class Solution {
public:
    int singleNumber(vector<int>& A) {
        int n = A.size();
        vector<int> count(32, 0);
        for(int i = 0; i < n; i++){
            for(int j = 0; j < 32; j++){
                if(A[i] & (1 << j)){
                    count[31 - j] += 1;
                }
            }
        }
        
        int result = 0;
        for(int i = 0; i < 32; i++){
            result |= (count[i] % 3) * (1 << (31 - i));
        }
        return result;
    }
};
