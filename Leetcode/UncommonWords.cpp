/*
https://leetcode.com/problems/uncommon-words-from-two-sentences/
Time : O(A.size() + B.size())
Space : O(A.size() + B.size())
*/
class Solution {
public:
    vector<string> uncommonFromSentences(string A, string B) {
        vector<string> uw;  //uncommon words
        unordered_map<string, int> freq;
        string word;
        
        //process A
        stringstream sa(A);
        while(sa >> word)
            freq[word]++;
        
        //process B
        stringstream sb(B);
        while(sb >> word)
            freq[word]++;
        
        unordered_map<string, int>::iterator m;
        for(m = freq.begin(); m != freq.end(); m++){
            if(m->second == 1)
                uw.push_back(m->first);
        }
        return uw;
    }
};