/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
/* https://leetcode.com/problems/add-two-numbers/
 T : O(n)
 S : O(n)
 */
class Solution
{
  public:
    ListNode *addTwoNumbers(ListNode *l1, ListNode *l2)
    {
        ListNode *res = NULL, *tail = NULL;
        int carry = 0, base = 10;
        while (l1 || l2)
        {
            // create a new node
            ListNode *sum = new ListNode(0);
            sum->val += carry;
            if (l1)
            {
                sum->val += l1->val;
                l1 = l1->next;
            }
            if (l2)
            {
                sum->val += l2->val;
                l2 = l2->next;
            }

            carry = sum->val / base;
            sum->val = sum->val % base;

            // save
            if (res == NULL)
            {
                res = sum;
                tail = sum;
            }
            else
            {
                tail->next = sum;
                tail = tail->next;
            }
        }

        if (carry > 0)
        {
            tail->next = new ListNode(carry);
        }

        return res;
    }
};