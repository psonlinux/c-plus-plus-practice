//
//  GrayCode.cpp
//  Leetcode
//
//  Created by Prashant Singh on 29/05/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

class Solution {
public:
    vector<int> grayCode(int n) {
        if(n == 0){
            return {0};
        }
        
        if(n == 1){
            return {0, 1};
        }
        
        vector<int> prevGC = grayCode(n - 1);
        vector<int> res = prevGC;
        
        // reverse the list and add append it
        int prevSize = res.size();
        for(int i = prevSize - 1; i >= 0; i--){
            res.push_back(res[i]);
        }
        
        // prepend 1 to second half
        for(int i = prevSize; i < res.size(); i++){
            res[i] = pow(2, n - 1) + res[i];
        }
        
        return res;
    }
};
