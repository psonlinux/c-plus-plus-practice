//
//  IterativeInorder.cpp
//  Leetcode
//
//  Created by Prashant Singh on 11/06/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<int> inorderTraversal(TreeNode* root) {
        vector<int> inorder;
        if(root == NULL){
            return inorder;
        }
        
        stack<TreeNode *> stack;
        stack.push(root);
        TreeNode * temp = NULL;
        // visit remaining node in inorder
        while(stack.empty() == false){
            temp = stack.top();
            stack.pop();
            if(temp->left == NULL && temp->right == NULL){
                inorder.push_back(temp->val);
            }
            else{
                if(temp->right){                        // visit right subtree
                    stack.push(temp->right);
                }
                
                stack.push(new TreeNode(temp->val));    // visit root
                
                if(temp->left){                         // visit left subtree
                    stack.push(temp->left);
                }
            }
        }
        return inorder;
    }
};
