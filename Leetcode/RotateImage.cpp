/*
T : O(n * n)
S : O(n * n)
*/
class Solution {
public:
    void rotate(vector<vector<int>>& matrix) {
        int r = matrix.size();          //rows
        int concentricMatrix = r / 2;   //no. of concentric matrices
        
        //transpose
        for(int i = 0; i < r; i++){
            for(int j = 0; j < i; j++){
                swap(matrix[i][j], matrix[j][i]);
            }
        }
        
        //reverse each row
        for(int i = 0; i < r; i++){
            reverse(matrix[i].begin(), matrix[i].end());
        }
    }
};
