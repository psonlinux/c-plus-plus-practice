/*
T : O(m * n)
S : O(m * n)
*/
class Solution {
public:
    int uniquePaths(int m, int n) {
        vector<vector<int>> dp(m, vector<int>(n));
        
        for(int i = 0; i < m; i++){
            for(int j = 0; j < n; j++){
                if(i == 0 || j == 0)
                    dp[i][j] = 1;
                else
                    dp[i][j] = dp[i - 1][j] + dp[i][j - 1];     //dp[i][j] = no. of unique path to reach that cell
            }                                                   //at each cell we can reach from left side or up side
        }
        return  dp[m - 1][n - 1];
    }
};