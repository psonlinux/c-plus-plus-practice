/*
 T : O(n)
 S : O(n)
 */
class Solution {
public:
    int trap(vector<int>& height) {
        // preprocess : find max height towards right of index i
        int n = height.size();
        if(n == 0){
            return 0;
        }
        
        vector<int> hr = height;
        for(int i = n - 2; i > 0; i--){
            if(hr[i + 1] > hr[i]){
                hr[i] = hr[i + 1];
            }
        }
        
        int totalWater = 0, water = 0;
        int hl = height[0];
        for(int i = 1; i < n - 1; i++){
            if(height[i] >= hl){
                hl = height[i];
            }
            else{
                water = min(hl, hr[i]) - height[i]; // water just above index i
                totalWater += water;
            }
        }
        return totalWater;
    }
};
