/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/*
T : O(n)
S : O(n)
*/
class Solution {
    /* Add child in left to right order*/
    void process(int k, vector<vector<TreeNode *>> & lvl, vector<vector<int>> & zzl){
        vector<int> lr;
        lvl[1 - k].clear();     // removes all item of next level
        for(int i = 0; i < lvl[k].size(); i++){
            lr.push_back(lvl[k][i]->val);
            if(lvl[k][i]->left){
                lvl[1 - k].push_back(lvl[k][i]->left);
            }
            if(lvl[k][i]->right){
                lvl[1 - k].push_back(lvl[k][i]->right);
            }
        }
        zzl.push_back(lr);
    }
    
    /* Reverse odd levels*/
    void revOddLvl(vector<vector<int>> & zzl){
        for(int i = 0; i < zzl.size(); i++){
            if(i % 2 == 1){
                reverse(zzl[i].begin(), zzl[i].end());
            }
        }
    }
public:
    vector<vector<int>> zigzagLevelOrder(TreeNode* root) {
        if(root == NULL){
            return {};
        }
        vector<vector<int>> zzl;
        
        // keep current level and next level
        vector<vector<TreeNode * >> lvl(2);
        lvl[0] = {root};
        int count = 0;
        while(lvl[count % 2].size() > 0){
            process(count % 2, lvl, zzl);
            count += 1;
        }
        
        //reverse odd level
        revOddLvl(zzl);
        return zzl;
    }
};