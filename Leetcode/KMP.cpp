//
//  KMP.cpp
//  Leetcode
//
//  Created by Prashant Singh on 04/07/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

/*
 http://www.inf.fh-flensburg.de/lang/algorithmen/pattern/kmpen.htm
 */
#include "iostream"
#include "vector"
using namespace std;

/*
 http://www.inf.fh-flensburg.de/lang/algorithmen/pattern/kmpen.htm
 */
class Solution {
    vector<int> getLcp(string & P){
        int n = P.size();
        vector<int> b(n);   // b[i] = index at which longest proper prefix ends, which is also the longest suffix of P[0...i]
        
        b[0] = -1;  // base case
        int j = 0;
        // find widest border for each P[i], i = 1...n - 1
        for(int i = 1; i < n; i++){
            j = b[i - 1];
            while(j >= 0 && P[i] != P[j + 1])j = b[j];
            
            if(P[j + 1] == P[i]){
                b[i] = j + 1;
            }
            else{
                b[i] = j;
            }
        }
        return b;
    }
public:
    int strStr(string T, string P) {
        int tSize = T.size(), pSize = P.size();
        if(pSize == 0){
            return 0;
        }
        
        // lcp array
        vector<int> b = getLcp(P);
        int j = 0;
        for(int i = 0; i < tSize; i++){
            while(j > 0 && P[j] != T[i]){   // char not matched
                j = b[j - 1] + 1;
            }
            
            if(P[j] == T[i]){               // char matched
                j += 1;
            }
            
            if(j == pSize){
                return i + 1 - pSize;       // pattern found
            }
        }
        return -1;                          // pattern not found
    }
};

int main(){
    Solution sol;
    string T = "mississippi", P = "mississippi";
    cout << sol.strStr(T, P) << endl;
    return 0;
}

/*
"aabaaabaaac"
"aabaaac"
*/
