/*
https://leetcode.com/problems/binary-subarrays-with-sum/
Time : O(n)
Space : O(n)
*/
class Solution{
    public:
    int numSubarraysWithSum(vector<int>& A, int S) {
        unordered_map<int, int> sumFreq;
        sumFreq[0] = 1;
        int sa = 0;
        int sum = 0;
        for(int i = 0; i < A.size(); i++){
            sum += A[i];
            if(sumFreq.find(sum - S) != sumFreq.end())
                sa += sumFreq[sum - S];
            sumFreq[sum] += 1;
        }
        return sa;
    }
};