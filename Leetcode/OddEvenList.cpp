/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* oddEvenList(ListNode* head) {
        if(head == NULL || head->next == NULL){
            return head;
        }
        
        //odd list
        ListNode * oh = new ListNode(-1), * ot = oh;
        
        //even list
        ListNode * eh = new ListNode(-1), * et = eh;
        
        int count = 1;
        while(head){
            if(count % 2 != 0){ // odd node
                ot->next = head;
                ot = ot->next;
            }else{
                et->next = head;
                et = et->next;
            }
            count += 1;
            head = head->next;
        }
        
        //joint two list
        ot->next = eh->next;
        et->next = NULL;
        return oh->next;
    }
};