//
//  LIS_nlogn.cpp
//  Leetcode
//
//  Created by Prashant Singh on 26/09/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

#include<iostream>
#include<vector>
#include<string>
#include<unordered_map>
#include<queue>
#include<unordered_set>
using namespace std;
int lis(vector<int> &A){
    int n = A.size();
    if(n == 0){
        return 0;
    }
    
    vector<int> seq;
    for(int i = 0; i < n; i++){
        int lo = 0, hi = seq.size(), m = 0;
        while(lo < hi){
            m = lo + (hi - lo) / 2;
            if(A[i] > seq[m]){
                lo = m + 1;
            }
            else{
                hi = m;
            }
        }
        // append
        if(lo < seq.size()){
            seq[lo] = A[i];
        }
        else{
            seq.push_back(A[i]);
        }
    }
    
    
    return seq.size();
}
int main() {
    int T = 0, t = 0;
    scanf("%d", &T);
    while(t < T){
        int n = 0;
        scanf("%d", &n);
        vector<int> A(n);
        for(int i = 0; i < n; i++){
            cin >> A[i];
        }
        int res = lis(A);
        cout << res << endl;
        t += 1;
    }
    return 0;
}
