
// https://leetcode.com/problems/network-delay-time/
class Solution {
public:
    //Dijkstra
    /*
    Time : O(N^2)
    Space : O(N^2)
    */
    int networkDelayTime(vector<vector<int>>& times, int N, int K){
        vector<vector<int>> w(N, vector<int>(N, INT_MAX));
        //create adjacency matrix
        for(int i = 0; i < times.size(); i++){
            w[times[i][0] - 1][times[i][1] - 1] = times[i][2];
        }
        for(int i = 0; i < N; i++)w[i][i] = 0;
        
        vector<int> sd(N, INT_MAX);         //sd[i] = shortest distance from source node to i'th node
        vector<int> priority(N, INT_MAX);   //unexplored node with minimum distance from source node will get priority
        priority[K - 1] = 0;    //source node is at distance zero from source
        
        int V = N;
        while(V--){
            int u = min_element(priority.begin(), priority.end()) - priority.begin();
            sd[u] = priority[u];
            for(int v = 0; v < N; v++){
                if(w[u][v] != INT_MAX && sd[u] != INT_MAX && sd[u] + w[u][v] < sd[v]){
                    sd[v] = sd[u] + w[u][v];
                    priority[v] = sd[u] + w[u][v];
                }
            }
            priority[u] = INT_MAX;
        }
    
        int time = *max_element(sd.begin(), sd.end());
        if(time == INT_MAX)return -1;
        return time;
    }
    
    /*
Time : O(V^3)
Space : O(V^2)
*/
    /*
    Bellman Ford
    int networkDelayTime(vector<vector<int>>& times, int N, int K) {
        vector<vector<int>> dist(N, vector<int>(N, INT_MAX));
        //create adjacency matrix and initialize
        for(int i = 0; i < times.size(); i++){
            dist[times[i][0] - 1][times[i][1] - 1] = times[i][2];
        }
        for(int i = 0; i < N; i++)dist[i][i] = 0;
        
        vector<int> ans(N, INT_MAX);    //ans[i] is shortest dist from node K - 1 to node i
        ans[K - 1] = 0;                 
        
        //relax all edges N - 1 times. Bellman Ford
        for(int v = 0; v < N - 1; v++){
            for(int i = 0; i < N; i++){
                for(int j = 0; j < N; j++){
                    if(dist[i][j] != INT_MAX && ans[i] != INT_MAX && ans[i] + dist[i][j] < ans[j])
                        ans[j] = ans[i] + dist[i][j];
                }
            }    
        }
        
        //min time to send signal
        int maxTime = *max_element(ans.begin(), ans.end());
        if(maxTime == INT_MAX)return -1;
        return maxTime;
    }
    */
    
};