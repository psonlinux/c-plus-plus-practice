//
//  RegExp.cpp
//  Leetcode
//
//  Created by Prashant Singh on 30/04/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

#include <iostream>
#include <string>
using namespace std;

class Solution {
    bool isMatch(string & s, int i, string & p, int j) {
        if(i >= s.size() && j >= p.size()){ // s = "", p = ""
            return true;
        }
        
        if(i < s.size() && j >= p.size()){    // s = "asd" p = ""
            return false;
        }
        
        if(j < p.size() - 1 && p[j + 1] == '*'){   // second char is '*'
            if(isMatch(s, i, p, j + 2))
                return true;
            if(i < s.size() && s[i] == p[j] && isMatch(s, i + 1, p, j))
                return true;
            if(i < s.size() && p[j] == '.' && isMatch(s, i + 1, p, j)){
                return true;
            }
        }
        else{
            if(i >= s.size()){  // s is empty and p don't have *
                return false;
            }
            if(p[j] == '.' && i < s.size()){
                if(isMatch(s, i + 1, p, j + 1))
                    return true;
            }
            if(i < s.size() && s[i] == p[j]){
                if(isMatch(s, i + 1,  p, j + 1))
                    return true;
            }
        }
        return false;
    }
    
public:
    bool isMatch(string s, string p){
        return isMatch(s, 0, p, 0);
    }
};

int main(){
    string s = "aaaaaaab", p = "a*a*a*a*a*a*c";
    Solution sol;
    if(sol.isMatch(s, p)){
        cout << "Matched\n";
    }
    else{
        cout << "Not Matched\n";
    }
    return 0;
}
