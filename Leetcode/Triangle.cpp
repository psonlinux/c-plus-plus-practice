/*
https://leetcode.com/problems/triangle/
Time : O(# item in each row)
Space : O(# item in each row)
*/
class Solution {
public:
    int minimumTotal(vector<vector<int>>& tr) {
        if(tr.size() == 0)return 0;
        for(int i = 1; i < tr.size(); i++){
            int n = tr[i].size();
            for(int j = 0; j < n; j++){
                if(j == 0)tr[i][j] += tr[i - 1][j]; //first item
                else if(j == n - 1)tr[i][j] += tr[i - 1][j - 1]; //last item
                else{
                    tr[i][j] += min(tr[i - 1][j - 1], tr[i - 1][j]);
                }
            }
        }
        return  *min_element(tr[tr.size() - 1].begin(), tr[tr.size() - 1].end());
    }
};