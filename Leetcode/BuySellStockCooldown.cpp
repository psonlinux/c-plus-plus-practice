//
//  BuySellStockCooldown.cpp
//  Leetcode
//
//  Created by Prashant Singh on 09/06/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

/*
 Either i'th stock can be sold or not .
 Recurrence :
 maxProfit(0, i) = max{  maxProfit(0, i - 1),     // i'th is not sold
 max{prices[i] - prices[k] + maxProfit(0, k - 2)}, k < i && k >= 0
 }
 */
class Solution {
public:
    int maxProfit(vector<int>& prices) {
        int n = prices.size();
        if(n <= 1)
            return 0;
        
        // max transaction possible
        int T = n / 3 + 1, profitWithSell = 0;
        
        vector<int> curProfit(n + 1);
        profitWithSell = -prices[0];
        for(int i = 0; i < n + 1; i++){
            if(i <= 1){
                curProfit[i] = 0;
            }
            else if(i == 2){
                curProfit[i] = max(0, prices[i - 1] - prices[i - 2]);
            }
            else{
                profitWithSell = max(profitWithSell, -prices[i - 2] + curProfit[i - 3]);
                curProfit[i] = max(curProfit[i - 1], profitWithSell + prices[i - 1]);
            }
        }
        return curProfit[n];
    }
};
