//
//  RangeSumQuery.cpp
//  Leetcode
//
//  Created by Prashant Singh on 25/06/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//
#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

// https://web.stanford.edu/class/cs97si/03-data-structures.pdf

class NumArray {
    vector<int> A;          // last n items contains the values
    int size;
    int N;                  // count of original items
    
    //Get level of a node
    int level(int p){
        int lvl = 0;
        while(p > 0){
            lvl += 1;
            p = (p - 1) / 2;
        }
        return lvl;
    }
    
public:
    NumArray(vector<int>& nums) {
        int n   = nums.size();
        if(n == 0){
            return ;
        }
        this->N = n;
        int pw = ceil(log2(n));     // no. of items should be power of 2
        int modCount = pow(2, pw);
        this->size = 2 *  modCount - 1;
        
        vector<int> A(size);
        this->A = A;
        for(int i = 0; i < nums.size(); i++){   // copy original items
            update(i, nums[i]);
        }
    }
    
    void update(int i, int val) {
        int n       = (size + 1)/ 2;
        int diff    = val - A[n - 1 + i];
        int par     = n - 1 + i;    // current node is parent
        while(true){
            A[par] += diff;
            if(par == 0){
                break;
            }
            par = (par - 1) / 2;
        }
    }
    
    int sumRange(int i, int j) {
        // corresponding index in A
        int p = (size + 1)/ 2 - 1 + i;
        int q = (size + 1)/ 2 - 1 + j;
        
        // sum till j
        int par = q, sumP = 0, sumQ = 0;
        while(par > 0){
            if(par % 2 == 1){
                sumQ += A[par];
                // terminate if cur node and left node are at diff level
                if(level(par) != level(par - 1)){
                    break;
                }
                par = (par - 2) / 2;
            }
            else{
                par = (par - 1) / 2;
            }
        }
        
        if(par == 0){
            sumQ = A[par];
        }
        
        if(i == 0){
            return sumQ - sumP;
        }
        
        // sum till i - 1   sum[0...i - 1]
        par = p - 1;
        while(par > 0){
            if(par % 2 == 1){
                sumP += A[par];
                // terminate if cur node and left node are at diff level
                if(level(par) != level(par - 1)){
                    break;
                }
                par = (par - 2) / 2;
            }
            else{
                par = (par - 1) / 2;
            }
        }
        
        if(par == 0){
            sumP = A[par];
        }
        return sumQ - sumP;
    }
};

/**
 * Your NumArray object will be instantiated and called as such:
 * NumArray* obj = new NumArray(nums);
 * obj->update(i,val);
 * int param_2 = obj->sumRange(i,j);
 */

int main(){
    vector<int> v = {1,3,5};
    NumArray * obj = new NumArray(v);
    int s1 = obj->sumRange(0, 2);
    obj->update(1, 10);
    s1 = obj->sumRange(0, 2);
    cout << s1 << endl;
    return 0;
}
