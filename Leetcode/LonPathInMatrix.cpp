/*
T : O(items of matrix)
S : O(items of matrix)
*/
class Solution {
    /*
    Gives length longest increasing path from from m[i,j] location .
    */
    int dfs(int i, int j, vector<vector<int>>& mx, vector<vector<bool>>& isVisited, vector<vector<int>>& lonPath, int& len) {
        if (lonPath[i][j] != -1) {
            return lonPath[i][j];
        }
        // consider left node
        int p1 = 0;
        if (j - 1 >= 0 && mx[i][j - 1] > mx[i][j] && !isVisited[i][j - 1]) {
            p1 = dfs(i, j - 1, mx, isVisited, lonPath, len);
        }

        // consider right node
        int p2 = 0;
        if (j + 1 < mx[0].size() && mx[i][j + 1] > mx[i][j] && !isVisited[i][j + 1]) {
            p2 = dfs(i, j + 1, mx, isVisited, lonPath, len);
        }

        //consider top node
        int p3 = 0;
        if (i - 1 >= 0 && mx[i - 1][j] > mx[i][j] && !isVisited[i - 1][j]) {
            p3 = dfs(i - 1, j, mx, isVisited, lonPath, len);
        }

        //consider bottom node
        int p4 = 0;
        if (i + 1 < mx.size() && mx[i + 1][j] > mx[i][j] && !isVisited[i + 1][j]) {
            p4 = dfs(i + 1, j, mx, isVisited, lonPath, len);
        }

        // un visit this node
        isVisited[i][j] = false;
        lonPath[i][j] = 1 + max(max(p1, p2), max(p3, p4));
        return lonPath[i][j];
    }

   public:
    int longestIncreasingPath(vector<vector<int>>& mx) {
        if (mx.size() == 0) {
            return 0;
        }
        int len = 1, m = mx.size(), n = mx[0].size();
        vector<vector<bool>> isVisited(m, vector<bool>(n));
        vector<vector<int>> lonPath(m, vector<int>(n, -1));
        for (int i = 0; i < mx.size(); i++) {
            for (int j = 0; j < mx[0].size(); j++) {
                int x = dfs(i, j, mx, isVisited, lonPath, len);
                lonPath[i][j] = x;
                if (lonPath[i][j] > len) {
                    len = lonPath[i][j];
                }
            }
        }
        return len;  // longest path length
    }
};

/*
[[0,1,2,3,4,5,6,7,8,9],
[19,18,17,16,15,14,13,12,11,10],
[20,21,22,23,24,25,26,27,28,29],
[39,38,37,36,35,34,33,32,31,30],
[40,41,42,43,44,45,46,47,48,49],
[59,58,57,56,55,54,53,52,51,50],
[60,61,62,63,64,65,66,67,68,69],
[79,78,77,76,75,74,73,72,71,70],
[80,81,82,83,84,85,86,87,88,89],
[99,98,97,96,95,94,93,92,91,90],
[100,101,102,103,104,105,106,107,108,109],
[119,118,117,116,115,114,113,112,111,110],
[120,121,122,123,124,125,126,127,128,129],
[139,138,137,136,135,134,133,132,131,130],
[0,0,0,0,0,0,0,0,0,0]]
*/