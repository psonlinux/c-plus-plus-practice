/*
https://leetcode.com/problems/n-ary-tree-preorder-traversal/
Time : O(# nodes)
Space : O(# nodes)
*/
/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};
*/
class Solution {
public:
    vector<int> preorder(Node* root) {
        vector<int> res;
        stack<Node *> stk;
        if(!root)
            return res;
        
        stk.push(root);
        while(!stk.empty()){
            //remove top item of stack
            Node *t = stk.top();
            stk.pop();
            
            //add its children from right to left
            for(int i = t->children.size() - 1; i >= 0; i--)
                stk.push(t->children[i]);
            
            //add parent to result
            res.push_back(t->val);
        }
        return res;
    }
};