 
/* https://leetcode.com/problems/partition-equal-subset-sum/
T : O(n * sum/2)
S : O(n * sum/2)
*/
class Solution {
    /*
     Return true if any subset sum to "half".
     Else false .
     */
    bool isSumHalf(int i, vector<int>& A, int sumSoFar, int half, vector<vector<int>> & memo){
        if(sumSoFar == half){
            return true;
        }
        
        if(i == A.size() || sumSoFar > half){
            return false;
        }
        
        if(memo[i][sumSoFar] != -1){
            return memo[i][sumSoFar];
        }
        
        if(isSumHalf(i + 1, A, sumSoFar + A[i], half, memo) == true){
            memo[i][sumSoFar] = true;
            return true;
        }
        
        if(isSumHalf(i + 1, A, sumSoFar, half, memo) == true){
            memo[i][sumSoFar] = true;
            return true;
        }
        
        memo[i][sumSoFar] = false;
        return false;
    }
public:
    bool canPartition(vector<int>& A) {
        int n            = A.size();
        int sumSoFar     = 0;
        int total        = accumulate(A.begin(), A.end(), 0);
        int half         = total / 2;
        if(2 * half != total){
            return false;
        }
        
        vector<vector<int>> memo(n + 1, vector<int>(half + 1, -1));
        return isSumHalf(0, A, sumSoFar, half, memo);
    }
};
