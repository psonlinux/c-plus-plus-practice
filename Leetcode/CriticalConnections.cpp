//
//  CriticalConnections.cpp
//  Leetcode
//
//  Created by Prashant Singh on 21/09/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

#include <iostream>
#include <unordered_set>
#include <vector>
#include <stack>
using namespace std;

class Digraph{
    vector<unordered_set<int>> G;
    int V;
    vector<bool> isVisited;
    
    void dfs(int s){
        isVisited[s] = true;
        for(int v: G[s]){
            // remove u from v 's list
            unordered_set<int> & adjV = G[v];
            adjV.erase(s);
            
            if(isVisited[v] == false){
                dfs(v);
            }
        }
    }
public:
    Digraph(int V){
        this->V = V;
        G.resize(V);
        isVisited.resize(V);
    }
    
    void addEdge(int u, int v){
        G[u].insert(v);
    }
    
    unordered_set<int> & adj(int u){
        return G[u];
    }
    
    int getVertexCount(){
        return V;
    }
    
    /* reverse the edges*/
    Digraph reverse(){
        Digraph gr(V);
        
        for(int u = 0; u < V; u++){
            for(int v: G[u]){
                gr.addEdge(v, u);
            }
        }
        return gr;
    }
    
    /* Remove all back edge between to adjacent nodes .*/
    void removeBackEdges(){
        for(int i = 0; i < V; i++){
            if(isVisited[i] == false){
                dfs(i);
            }
        }
    }
};

class DepthFirstOrder{
    stack<int> revPost;
    vector<bool> isVisited;
    
    void dfs(Digraph & G, int s){
        isVisited[s] = true;
        
        for(int v: G.adj(s)){
            if(isVisited[v] == false){
                dfs(G, v);
            }
        }
        revPost.push(s);
    }
    
    public:
    DepthFirstOrder(Digraph G){
        int V = G.getVertexCount();
        isVisited.resize(V);
        for(int i = 0; i < V; i++){
            if(isVisited[i] == false){
                dfs(G, i);
            }
        }
    }
    
    stack<int> revPostOrder(){
        return revPost;
    }
};

class KosarajuSCC{
    int count;
    vector<int> id;
    vector<bool> isVisited;
    vector<vector<int>> criticalCon;     // cut edge
    
    void dfs(Digraph & G, int u){
        isVisited[u]    = true;
        id[u]           = count;
        for(int v: G.adj(u)){
            if(isVisited[v] == false){
                dfs(G, v);
            }
            
            if(isVisited[v] == true && id[v] != count){
                // this edge points from this SCC to another SCC, means its a critical connection
                criticalCon.push_back({u, v});
            }
        }
    }
    
    public:
    KosarajuSCC(Digraph G){
        int V = G.getVertexCount();
        isVisited.resize(V);
        id.resize(V);
        
        DepthFirstOrder dfo(G.reverse());
        stack<int> order = dfo.revPostOrder();
        int u = 0;
        count = 0;
        while(order.empty() == false){
            u = order.top(); order.pop();
            if(isVisited[u] == false){
                dfs(G, u);
                count += 1;
            }
        }
    }
    
    /*
    Return the count of Strongly connected component .
    */
    int getCount(){
        return count;
    }
    
    /*
    Return all critical connections
    */
    vector<vector<int>> getCriticalCon(){
        return criticalCon;
    }
};

class Solution {
public:
    vector<vector<int>> criticalConnections(int n, vector<vector<int>>& C) {
        // create a digraph
        Digraph G(n);
        
        int u, v;
        for(int i = 0; i < C.size(); i++){
            u = C[i][0];
            v = C[i][1];
            G.addEdge(u, v);
            G.addEdge(v, u);
        }
        
        //remove back edge of adjacent node
        G.removeBackEdges();
        
        // find strongly connected component
        KosarajuSCC ksr(G);
        
        return ksr.getCriticalCon();
    }
};

int main(){
    int n = 4;
    vector<vector<int>> v = {{0,1},{1,2},{2,0},{1,3}};
    Solution sol;
    vector<vector<int>> cc = sol.criticalConnections(n, v);
    return 0;
}
