/*
https://leetcode.com/problems/average-of-levels-in-binary-tree/
Time : O(n)
Space : O(n)
*/
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<double> averageOfLevels(TreeNode* r) {
        vector<double> averages;
        if(r == NULL)
            return averages;
        
        queue<TreeNode *> q;
        q.push(r);
        while(!q.empty()){
            double avg = 0;
            int size = q.size();;
            for(int i=0; i<size; i++){
                //add child
                TreeNode *e = q.front();
                if(e->left)q.push(e->left);
                if(e->right)q.push(e->right);
                
                avg += e->val;      //avg
                q.pop();            //pop
            }
            averages.push_back(avg/size);  //add avg to vector
        }
        return averages;
    }
};