/*
minCoins(amt, coin[0...n-1]) = min(1 + minCoins(amt - coin[0]), 1 + minCoins(amt - coin[1]) ... , 1 + minCoins(amt - coin[n-1])) 
*/

/*
T : O(amount * no. of coins)
S : O(amount)
*/
#define INF 99999
class Solution {
    int minCoins(int amt, vector<int> & coins, vector<int> & cache){
        if(amt == 0)    return 0;
        if(amt < 0)     return INF;
        if(cache[amt] != -1)    return cache[amt];
        
        int minCn = INF, x;
        for(int i = 0; i < coins.size(); i++){
            x = 1 + minCoins(amt - coins[i], coins, cache);
            if(x < minCn)
                minCn = x;
        }
        
        cache[amt] = minCn;
        return cache[amt];
    }
public:
    int coinChange(vector<int>& coins, int amount) {
        vector<int> cache(amount + 1, -1);
        int mnCn = minCoins(amount, coins, cache);
        if(mnCn == INF) return -1;
        else            return mnCn;
    }
};

