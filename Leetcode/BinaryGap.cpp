/*
https://leetcode.com/problems/binary-gap/
Time : O(no. of bits)
*/
class Solution {
public:
    int binaryGap(int N) {
        int mx = 0;
        bool foundOne = false; //found first one from right
        int count = 0;
        while(N){
            if(N&1){
                if(foundOne){
                   mx =  max(mx, count);
                }
                foundOne = true;
                count = 1;
            }
            else
                count++;
            N = N>>1;
        }
        return mx;
    }
};