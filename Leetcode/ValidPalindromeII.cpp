/* https://leetcode.com/problems/valid-palindrome-ii/
T : O(n)
S : O(n)
*/
class Solution {
    bool isPalin(string s){
        int i = 0, j = s.size() - 1;
        while(i < j){
            if(s[i] != s[j])
                return false;
            i += 1;
            j -= 1;
        }
        return true;
    }
public:
    bool validPalindrome(string s) {
        int i = 0, j = s.size() - 1;
        while(i < j){
            if(s[i] != s[j]){
                //remove i and check for palindrome
                string s1 = s;
                s1.erase(i, 1);
                if(isPalin(s1))
                    return true;

                s1 = s;
                s1.erase(j, 1);
                if(isPalin(s1))
                    return true;
                
                return false;
            }    
            i += 1;
            j -= 1;
        }
        return true;
    }
};