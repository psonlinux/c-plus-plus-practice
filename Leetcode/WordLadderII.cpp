//
//  WordLadderII.cpp
//  Leetcode
//
//  Created by Prashant Singh on 09/07/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

#include "iostream"
#include "vector"
#include "queue"
#include "unordered_map"
#include "string"
using namespace std;

/*
 https://leetcode.com/problems/word-ladder-ii/submissions/
 */
class Solution {
    /*
     Simply prints the path from beginWord to endWord
     */
    vector<vector<string>> path(vector<vector<int>> & parent, int leaf, vector<string> & Wl){
        vector<vector<string>> subPath;
        if(parent[leaf].size() == 0){
            return {{Wl[leaf]}};
        }
        
        for(int i = 0; i < parent[leaf].size(); i++){
            vector<vector<string>> locPath = path(parent, parent[leaf][i], Wl);
            for(int j = 0; j < locPath.size(); j++){
                locPath[j].push_back(Wl[leaf]);
                subPath.push_back(locPath[j]);
            }
        }
        return subPath;
    }
    
public:
    vector<vector<string>> findLadders(string beginWord, string endWord, vector<string>& WL) {
        // create a map of string to their location
        unordered_map<string, int> sToLoc;
        int n = WL.size();
        for(int i = 0; i < n; i++){
            sToLoc[WL[i]] = i;
        }
        
        // add begin word to WL if not present
        if(sToLoc.find(beginWord) == sToLoc.end()){
            WL.push_back(beginWord);
            sToLoc[beginWord] = n;
            n += 1;
        }
        
        // distance from beginWord
        vector<int> dist(n);
        
        // parent of words. A wordcan have multiple parent
        vector<vector<int>> parent(n);
        // parent[sToLoc[beginWord]] = NULL; // parent of beginWord is -1
        
        // All paths
        vector<vector<string>> paths;
        
        int distance = INT_MAX;
        
        //start exploring the words which are nearest to source and move forward
        vector<bool> isVisited(n);
        queue<pair<int, int>> q;    // pair<word, distance from source>
        q.push(make_pair(sToLoc[beginWord], 0));
        isVisited[sToLoc[beginWord]] = true;
        while(q.empty() == false){
            pair<int, int> father = q.front();
            q.pop();
            
            // find nearest unexplored words
            string word = WL[father.first];
            for(int i = 0; i < word.size(); i++){
                char saveChar = word[i];
                for(int j = 0; j < 26; j++){
                    word[i] = 'a' + j;
                    auto child = sToLoc.find(word);
                    if(child != sToLoc.end() && word == endWord){  // found a word
                        if(father.second <= distance){
                            distance = father.second;
                            parent[child->second].push_back(father.first);
                            vector<vector<string>> allPath = path(parent, father.first, WL);
                            
                            // add all path to result
                            for(int k = 0; k < allPath.size(); k++){
                                allPath[k].push_back(endWord);
                                paths.push_back(allPath[k]);
                            }
                        }
                    }
                    else if(child != sToLoc.end()){
                        if(father.second + 1 == dist[child->second] || isVisited[child->second] == false){
                            parent[child->second].push_back(father.first);    // set its parent
                        }
                        
                        if(isVisited[child->second] == false){
                            q.push(make_pair(child->second, father.second + 1));
                            dist[child->second] = father.second + 1;
                            isVisited[child->second] = true;
                        }
                    }
                }
                word[i] = saveChar; // restore i'th char
            }
        }
        return paths;
    }
};

int main(){
    Solution sol;
    string bw = "red", ew = "tax";
    vector<string> wl = {"ted","tex","rex","tad"};
    vector<vector<string>> paths = sol.findLadders(bw, ew, wl);
    return 0;
}

/*
 "red"
 "tax"
 ["ted","tex","red","tax","tad","den","rex","pee"]
 */
