class Solution {
    /*
    Repeat a string 
    input : "abc", 2
    output : "abcabc"
    */
    string repeat(string & s, int r){
        string rs;
        for(int i = 0; i < r; i++)
            rs += s;
        return rs;
    }
    
    string getDecoded(string en, int start, int end){
        // base case : string is already decoded 
        bool isEncoded = true;
        for(int i = start; i <= end; i++){
            if(!isalpha(en[i])){
                isEncoded = false;
                break;
            }
        }
        if(isEncoded)return en.substr(start, end - start + 1);
        
        //scan chars
        string letters = "";
        for(int i = start; i <= end; i++){
            if(isalpha(en[i]))
                letters += en[i];
            if(isdigit(en[i]))
                break;
        }
        
        //scan digits
        string num;
        int d = start;
        while(d <= end){
            if(isdigit(en[d]))
                num += en[d];
            if(en[d] == '[')
                break;
            d += 1;
        }
        int r = stoi(num);
        
        //decode string inside brackets [ ]
        int ob = d, cb;     //index of open bracket [
        int c = 0;
        
        //find index of closing bracket ']'
        for(cb = ob; cb <= end; cb++){
            if(en[cb] == '[')   c += 1;
            if(en[cb] == ']')   c -= 1;
            if(c == 0)          break;  // found closing bracket
        }
        
        //get decoded string inside [ ]
        string ds = getDecoded(en, ob + 1, cb - 1);
        return letters + repeat(ds, r) + getDecoded(en, cb + 1, end);
    }
public:
    string decodeString(string s) {       
        return getDecoded(s, 0, s.size() - 1);
    }
};