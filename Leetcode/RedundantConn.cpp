//
//  RedundantConn.cpp
//  Leetcode
//
//  Created by Prashant Singh on 14/05/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//
#include <iostream>
#include <vector>
#include <unordered_map>
#include <cfloat>
using namespace std;

// Use Union-Find data structure
// connect vertices one by one until we get the edge whose both ends are already connected .

class Solution {
    int V;       // no. of vertices
    vector<int> parent;      // parent[i] =  set representative, which has i'th node
    vector<int> size;        // size[i] = size of set, which contains i'th vertex
    
    //find ancestor
    int find(int x){
        while(x != parent[x]){
            x = parent[x];
        }
        return x;
    }
    
    //unite two set
    void uni(int x, int y){
        int xRoot = find(x), yRoot = find(y);
        if(xRoot == yRoot){ // x and y are already in same set
            return;
        }
        
        if(size[xRoot] < size[yRoot]){
            parent[xRoot]   = yRoot;
            size[yRoot]     += size[xRoot];
        }
        else{
            parent[yRoot]   =  xRoot;
            size[xRoot]     += size[yRoot];
        }
    }
public:
    vector<int> findRedundantConnection(vector<vector<int>>& edges) {
        if(edges.empty() == true){
            return {};
        }
        
        V = edges.size() + 1;   // plus 1 because vertices start from 1
        vector<int> p(V), s(V);
        parent  = p;
        size    = s;
        
        for(int i = 0; i < V; i++){
            parent[i] = i;
        }
        
        //merge vertices
        int x, y;
        vector<int> re; //redundant edge
        for(int i = 0; i < V - 1; i++){
            x = edges[i][0];
            y = edges[i][1];
            
            //if in different set then merge
            int p1 = find(x), p2 = find(y);
            if(p1 != p2){
                uni(p1, p2);
            }
            else{
                re = edges[i];
            }
        }
        return re;  // redundant edge
    }
};

int main(){
    vector<vector<int>> edges = {{1,2}, {1,3}, {2,3}};
    Solution sol;
    vector<int> v = sol.findRedundantConnection(edges);
    printf("%i %i\n", v[0], v[1]);
    return 0;
}
