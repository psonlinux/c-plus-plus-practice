/*
https://leetcode.com/problems/predict-the-winner/
Time : O(n^2)
Space : O(n^2)
*/
class Solution {
public:
    int f(int i, int j, int sm, vector<int> &A, vector<vector<int>> &dp){
        if(i == j){
            dp[i][i] = A[i];
            return dp[i][j];
        }
        
        if(dp[i][j] != -1)return dp[i][j];
        
        dp[i][j] = max(sm - f(i + 1, j, sm - A[i], A, dp) , sm - f(i, j - 1, sm - A[j], A, dp));
        return dp[i][j];
    }
    
    bool PredictTheWinner(vector<int>& nums) {
        int n = nums.size();
        vector<vector<int>> dp(n, vector<int>(n, -1));
        int sm = accumulate(nums.begin(), nums.end(), 0);   //sum of array
        int p1 = f(0, n - 1, sm, nums, dp);                 //max score of first player
        int p2 = sm - p1;                                   //max score of second player
        if(p1 >= p2)return true;
        return false;
    }
};