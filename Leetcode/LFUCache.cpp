//https://leetcode.com/problems/lfu-cache/
/*
 T : O(1)
 S : O(n)
 */
#include "iostream"
#include "unordered_map"
using namespace std;

struct FreqNode{
    int val;
    struct FreqNode * next, * prev;
    struct KeyNode * head, * tail;  //keysTail points to most recent item
    FreqNode(int _val){
        val = _val;
        next = NULL;
        prev = NULL;
        head = NULL;
        tail = NULL;
    }
    
    FreqNode(){
        FreqNode(0);
    }
};

struct KeyNode{
    int key, val;
    struct FreqNode * bucket;
    struct KeyNode * next, * prev;
    KeyNode(int _key, int _val){
        key = _key;
        val = _val;
        bucket  = NULL;
        next    = NULL;
        prev    = NULL;
    }
    
    KeyNode(){
        KeyNode(-1, -1);
    }
};

class LFUCache {
    int size = 0, N = 0;
    unordered_map<int, KeyNode *> um;
    FreqNode * fh = NULL;
    
    //Get next bucket
    FreqNode * getNextBucket(FreqNode * curBucket){
        if(curBucket == NULL){
            return NULL;
        }
        
        //next valid bucket already exist
        if(curBucket->next && curBucket->next->val == curBucket->val + 1){
            return curBucket->next;
        }
        
        //create new bucket
        FreqNode * newNode = new FreqNode(curBucket->val + 1);
        newNode->head = new KeyNode(-1, -1);
        newNode->tail = newNode->head;
        
        newNode->next = curBucket->next;
        newNode->prev = curBucket;
        
        if(newNode->prev){
            newNode->prev->next = newNode;
        }
        
        if(newNode->next){
            newNode->next->prev = newNode;
        }
        return newNode;
    }
    
    //Delete cur bucket if empty
    void deleteBucket(FreqNode * curBucket){
        if(curBucket->tail != curBucket->head){  // current bucket contains some item
            return;
        }
        
        FreqNode *pb, * nb;
        pb = curBucket->prev;
        nb = curBucket->next;
        
        if(pb){
            pb->next = nb;
        }
        
        if(nb){
            nb->prev = pb;
        }
    }
public:
    LFUCache(int capacity) {
        N = capacity;
        fh = new FreqNode(0);
        fh->head = new KeyNode(-1, -1);
        fh->tail = fh->head;
    }
    
    int get(int key) {
        auto it = um.find(key);
        if(it == um.end()){         // key not present
            return -1;
        }
        else{                       // key present
            //delete from current bucket
            KeyNode *kn = it->second;
            KeyNode *pn = kn->prev;
            KeyNode *nn = kn->next;
            
            kn->next = NULL;
            kn->prev = NULL;
            
            //link nodes
            pn->next = nn;
            if(nn){
                nn->prev = pn;
            }
            
            //update tail
            FreqNode * curBucket = kn->bucket;
            if(kn == curBucket->tail){
                curBucket->tail = pn;
            }
            
            //insert in next bucket
            FreqNode * nextBucket = getNextBucket(curBucket);
            
            nextBucket->tail->next = kn;
            kn->prev = nextBucket->tail;
            nextBucket->tail = nextBucket->tail->next;
            
            //update it's bucket
            kn->bucket = nextBucket;
            
            //delete cur bucket if empty
            deleteBucket(curBucket);
            return kn->val;
        }
    }
    
    void put(int key, int value) {
        if(N == 0){     //cache capacity is zero !
            return;
        }
        
        auto it = um.find(key);
        if(it != um.end()){       //key present, then just update key's value and freq
            KeyNode *kn = it->second;
            FreqNode *curBucket = kn->bucket;
            kn->val = value;
            
            //delete from current list
            KeyNode * pn = kn->prev;
            KeyNode * nn = kn->next;
            kn->next = NULL;
            kn->prev = NULL;
            
            if(pn){
                pn->next = nn;
            }
            
            if(nn){
                nn->prev = pn;
            }
            
            //update tail
            if(curBucket->tail == kn){
                curBucket->tail = pn;
            }
            
            //add to next bucket
            FreqNode *nextBucket = getNextBucket(curBucket);
            nextBucket->tail->next = kn;
            kn->prev = nextBucket->tail;
            nextBucket->tail = nextBucket->tail->next;
            
            //update its bucket
            kn->bucket = nextBucket;
            
            //delete cur bucket
            deleteBucket(curBucket);
        }
        else{               //key not present
            //create new node
            KeyNode * kn = new KeyNode(key, value);
            if(size >= N){
                 // cache full
                //remove least frequently used node
                FreqNode * evictionBucket = fh->next;
                
                KeyNode * en = evictionBucket->head->next;
                KeyNode *pn, * nn;
                pn = en->prev;
                nn = en->next;
                if(pn){
                    pn->next = nn;
                }
                
                if(nn){
                    nn->prev = pn;
                }
                
                //update tail
                if(evictionBucket->tail == en){
                    evictionBucket->tail = pn;
                }
                
                //remove from map
                um.erase(en->key);
                
                deleteBucket(evictionBucket);
                size -= 1;
            }
            
            fh->next = getNextBucket(fh);
            FreqNode * curBucket = fh->next;
            
            //update key's bucket
            kn->bucket = curBucket;
            
            //add to bucket
            curBucket->tail->next = kn;
            kn->prev        = curBucket->tail;
            curBucket->tail = curBucket->tail->next;
            
            um[key] = kn;   //add to map
            size += 1;
        }
    }
};

/**
 * Your LFUCache object will be instantiated and called as such:
 * LFUCache* obj = new LFUCache(capacity);
 * int param_1 = obj->get(key);
 * obj->put(key,value);
 */int main(){
    int capacity = 2;
    LFUCache cache(capacity);
    cache.put(1,1);
    cache.put(2,2);
    cout << cache.get(1) << " ";
    cache.put(3, 3);
    cout << cache.get(2) << " ";
    return 0;
}
