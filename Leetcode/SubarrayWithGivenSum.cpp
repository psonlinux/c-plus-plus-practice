/*
Time : O(n) amortized
Space : O(n)
*/
#include<iostream>
#include<unordered_map>
using namespace std;

void printSubarray(int a[], int l, int r){
	for(int i = l; i <= r; i++)
		cout << a[i] << " ";
	cout << endl;
}

bool printSubarrayWithSum(int a[], int n, int k){
	unordered_map<int, int> prevSum; 	//stores prev sum and its index
	prevSum[0] = 0;
	int sum = 0;
	for(int i = 0; i < n; i++){
		sum += a[i];
		if(prevSum.find(sum - k) != prevSum.end()){
			printSubarray(a, prevSum[sum - k], i);
			return true;
		}
		prevSum[sum] = i;
	}
	cout << "No subarray with sum = k . :(" << endl;
	return false;
}

int main(){
	int a[] = {2, 0 , -3, 10, -2, 1};
	int k = -1;
	printSubarrayWithSum(a, 6, k);
	return 0;
}