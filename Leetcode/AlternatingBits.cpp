/*
https://leetcode.com/problems/binary-number-with-alternating-bits
Time : O(1)
SPace : O(1)
*/
class Solution {
public:
    bool hasAlternatingBits(int n) {
        return ( ((n & n>>1) == 0) && ( n>>2 == (n>>2 & n) ) );
    }
};