//
//  LongestSubstringWithK.cpp
//  Leetcode
//
//  Created by Prashant Singh on 07/08/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

#include "iostream"
#include "string"
#include "unordered_map"
using namespace std;

class Solution {
    int f(string & sg, int s, int e, int k){
        if(e - s < k){
            return 0;
        }
        
        vector<int> fr(26);
        for(int i = s; i < e; i++){
            fr[sg[i] - 'a'] += 1;
        }
        
        int par = -1;
        for(int i = s; i < e; i++){
            if(fr[sg[i] - 'a'] < k){
                par = i;
                break;
            }
        }
        
        if(par == -1){
            return e - s;
        }
        
        return max(f(sg, s, par, k), f(sg, par + 1, e, k));
    }
public:
    int longestSubstring(string s, int k) {
        int n = s.size();
        return f(s, 0, n, k);
    }
};
int main(){
    string s = "ddabaaaaabbcc";
    int k = 3;
    Solution sol ;
    cout << sol.longestSubstring(s, k);
    return 0;
}
