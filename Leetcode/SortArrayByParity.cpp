/*
https://leetcode.com/problems/sort-array-by-parity-ii/
Time : O(n)
Space : O(1)
*/
class Solution {
public:
    vector<int> sortArrayByParityII(vector<int>& A) {
        int e = 0, o = 1;
        int n = A.size();
        while(e < n && o < n){
            while(e < n && A[e] % 2 == 0)e += 2;
            while(o < n && A[o] % 2 == 1)o += 2;
            if(e < n && o < n)swap(A[e], A[o]);
        }
        return A;
    }
};