/*
https://leetcode.com/problems/find-k-closest-elements/
Time : O(n)
Space : O(n)
*/
class Solution { 
public:
    vector<int> findClosestElements(vector<int>& v, int k, int x) {
        int p = 0, q = k - 1;
        for(int i = k; i < v.size(); i++){
            if(abs(v[i] - x) < abs(v[p] - x)){
                p = i - k + 1;
                q = i;
            }      
        }
        vector<int> sol(v.begin() + p, v.begin() + q + 1);
        return sol;
    }
};