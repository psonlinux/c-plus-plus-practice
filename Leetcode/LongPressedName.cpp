/*
https://leetcode.com/problems/long-pressed-name/
Time : O(n)
Space : O(name.size() + typed.size())
Count of same group of characters in typed string must be >= than in name string .
*/
class Solution {
public:
    bool isLongPressedName(string name, string typed) {
        int m = name.size();
        int n = typed.size();
        if(m == 0 && n == 0)return true;
        if(m == 0 && n != 0)return false;
        if(m != 0 && n == 0)return false;
        
        int i = 0, j = 0;
        while(true){
            //over name
            int c1 = 1;
            while(i + 1 < m && name[i] == name[i + 1]){
                i += 1;
                c1 += 1;
            }
            
            //over typed
            int c2 = 1;
            while(j + 1 < n && typed[j] == typed[j + 1]){
                j += 1;
                c2 += 1;
            }
            
            if(c1 > c2 || name[i] != typed[j])
                return false;
            
            i += 1;
            if(i >= m)c1 = 0;
            
            j += 1;
            if(j >= n)c2 = 0;
            
            if(i >= m && j >= n)return true;
        }
    }
};