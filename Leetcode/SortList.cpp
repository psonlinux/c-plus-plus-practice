
/* https://leetcode.com/problems/sort-list/
T : O(nlogn)
Extra space : O(1)
*/

#include <iostream>
using namespace std;

// Definition for singly-linked list.
struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
    ListNode * h = NULL, * t = NULL;
    
    /*Compute no. of nodes in link list*/
    int size(ListNode *n){
        int s = 0;
        while(n){
            s += 1;
            n = n->next;
        }
        return s;
    }
    
    /* Merge two sorted link list
    */
    ListNode * merge(ListNode * p, int pSize, ListNode * mid, int midSize, int size){
        while(pSize > 0 || midSize > 0){
            if(midSize == 0 || (pSize > 0 && p->val <= mid->val)){
                if(h == NULL){
                    h = p;
                    t = p;
                }
                else{
                    t->next = p;
                    t       = t->next;
                }
                p       = p->next;
                pSize   -= 1;
            }
            else{
                if(h == NULL){
                    h = mid;
                    t = mid;
                }
                else{
                    t->next = mid;
                    t       = t->next;
                }
                mid     = mid->next;
                midSize -= 1;
            }
        }
        return h;
    }
    
public:
    ListNode* sortList(ListNode* head) {
        if(head == NULL || head->next == NULL)
            return head;
        
        int n = size(head);
            
        int size    = 2;       // sort every two consecutive node
        ListNode *p = head, *q = head, *mid = head;
        
        while(size / 2 < n){
            
            if(h != NULL){
                p = h;
                q = h;
                mid = h;
                
                //reset h and t
                h = NULL;
                t = NULL;
            }
            
            while(true){
                int sa = size / 2;
                int pSize = 0;
                while(q && pSize < sa){     //move q and mid to middle
                    q   = q->next;
                    mid = mid->next;
                    pSize += 1;
                }

                //move q to right end
                int midSize = 0;
                while(q && midSize < sa){
                    q = q->next;
                    midSize += 1;
                }
                
                merge(p, pSize, mid, midSize, sa);

                // update p, mid, q
                p   = q;
                mid = q;    

                if(q == NULL)   // q reached end of list
                    break;
            }
            t->next = NULL;     //terminate last node
            size = 2 * size;   
        }
        return h;
    }
};

int main()
{
    Solution sol;
    ListNode *head = new ListNode(4);
    head->next = new ListNode(3);
    head->next->next = new ListNode(2);
    ListNode *a, *b;
    a = new ListNode(1);
    b = new ListNode(0);
    //a->next = b;
    head->next->next->next = a;
    head = sol.sortList(head);
    while(head){
        printf("%i ", head->val);
        head = head->next;
    }
    return 0;
}