/* https://leetcode.com/problems/target-sum/
T : O(nums.size() * S)
S : O(nums.size() * S)
*/

class Solution {
    /* Each num can be + or - . 
    Increment count if i ... n nums are used to produce remSum sum .
    */
    int combinations(vector<int> & nums, int i, long remSum, map<pair<int, int>, int> & m){
        // cout << i << " " << remSum << endl;
        if(m.find(make_pair(i, remSum)) != m.end()){
            return m[{i, remSum}];
        }
        
        if(i == nums.size()){    //used all numbers to get S
            return (remSum == 0);
         } 

        // case 1 : i'th should be positive
        m[{i + 1, remSum - nums[i]}] = combinations(nums, i + 1, remSum - nums[i], m);
        
        // case 2 : i'th num should be negative
        m[{i + 1, remSum + nums[i]}] = combinations(nums, i + 1, remSum + nums[i], m);
        
        m[{i, remSum}] = m[{i + 1, remSum - nums[i]}] + m[{i + 1, remSum + nums[i]}];
        return m[{i,remSum}];
    }
public:
    int findTargetSumWays(vector<int>& nums, int S) {
        int n = nums.size();
        int x = 0;              //count the number of combinations possible
        
        //total sum
        int total = accumulate(nums.begin(), nums.end(), 0);
        
        if(total < S)
            return 0;
        
        // vector<vector<int>> memoize(n + 1, vector<int>(S + total + 1, -1));
        map<pair<int, int>, int> m;
        return combinations(nums, 0, S, m);
    }
};