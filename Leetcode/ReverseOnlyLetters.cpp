/*
https://leetcode.com/contest/weekly-contest-105/problems/reverse-only-letters/
*/
#include <iostream>
#include <algorithm>
#include <string>
using namespace std;

class Solution {
public:
    string reverseOnlyLetters(string s) {
        int n = s.size();
        int i = 0, j = n - 1;
        
        while(i < j){
            //skip non alphabet from left 
            while(i < j && !isalpha(s[i])){
                i++;
            }
            
            //skip non alphabet from right
            while(j > i && !isalpha(s[j])){
                j--;
            }
            
            //swap alphabets
            if(i < j){
                swap(s[i], s[j]);
                i++;
                j--;
            }
        }
        return s;
    }
};

int main(){
    Solution sol;
    cout << sol.reverseOnlyLetters("-ab-cd") << endl;
    return 0;
}