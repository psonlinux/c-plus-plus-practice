//
//  ReorganizeString.cpp
//  Leetcode
//
//  Created by Prashant Singh on 27/09/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//
#include <vector>
#include "iostream"
#include "string"
#include "unordered_map"
#include "queue"
using namespace std;

struct CharFreq{
    char c;
    int count;
    CharFreq(char c, int count){
        this->c     = c;
        this->count = count;
    }
};

class Comp{
    public:
    bool operator()(const CharFreq &a, const CharFreq &b){
        return a.count < b.count;
    }
};

class Solution {
public:
    string reorganizeString(string s) {
        int n = s.size();
        unordered_map<char, int> fr;
        for(int i = 0; i < n; i++){
            fr[s[i]] += 1;
        }
        
        priority_queue<CharFreq, vector<CharFreq>, Comp> pq;
        for(auto it = fr.begin(); it != fr.end(); it++){
            CharFreq cf(it->first, it->second);
            pq.push(cf);
        }
        
        //limit
        int limit = 0;
        if(n % 2 == 0){
            limit = n / 2;
        }
        else{
            limit = (n + 1) / 2;
        }
        
        string ret(n, ' ');
        int floc = 0;
        while(pq.empty() == false){
            CharFreq cf = pq.top(); pq.pop();
            if(cf.count > limit){
                return "";      // not possible
            }
            
            //place char alternate
            for(int i = 0; i < cf.count; i++){
                ret[floc] = cf.c;
                floc += 2;
                if(floc >= n){
                    floc = 1;
                }
            }
        }
        return ret;
    }
};

int main(){
    string s = "vvvlo";
    Solution sol;
    string rs = sol.reorganizeString(s);
    cout << rs;
    return 0;
}
