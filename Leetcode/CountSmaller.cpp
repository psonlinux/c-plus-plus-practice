//
//  CountSmaller.cpp
//  Leetcode
//
//  Created by Prashant Singh on 23/06/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//
#include <vector>
#include "iostream"
using namespace std;

class Solution {
    vector<int> tp, res;    // tp : used for merging
    // res[i] = count of int smaller than A[i]
    void merge(vector<int> &A, vector<int> &ia, int lo, int mid, int hi){
        int i = lo, j = mid, k = lo;
        while(i < mid && j < hi){
            if(A[ia[i]] < A[ia[j]]){
                tp[k] = ia[i];
                i += 1;
            }
            else{
                tp[k] = ia[j];
                j += 1;
            }
            k += 1;
        }
        
        //copy left half to T
        while(i < mid){
            tp[k] = ia[i];
            i += 1;
            k += 1;
        }
        
        //copy right half to T
        while(j < hi){
            tp[k] = ia[j];
            j += 1;
            k += 1;
        }
        
        //copy back T to B
        for(int k = lo; k < hi; k++){
            ia[k] = tp[k];
        }
    }
    
    void countSmallerAcross(vector<int> &A, vector<int> &ia, int lo, int mid, int hi){
        int lb = mid;
        for(int p = lo; p < mid; p++){
            while(lb < hi && A[ia[p]] > A[ia[lb]]) lb += 1;
            res[ia[p]] += (lb - mid);
        }
        
        merge(A, ia, lo, mid, hi);
    }
    
    void countSmaller(vector<int> & A, vector<int> & indexArr, int lo, int hi){
        if(hi - lo < 1){
            return ;
        }
        
        if(hi - lo == 1){
            res[indexArr[lo]] += 0;
            return;
        }
        
        if(hi - lo == 2){
            if(A[indexArr[lo]] > A[indexArr[hi - 1]]){
                res[indexArr[lo]] += 1;
                swap(indexArr[lo], indexArr[hi - 1]);
            }
            return;
        }
        
        int mid = lo + (hi - lo) / 2;
        
        //count inversion in left half
        countSmaller(A, indexArr, lo, mid);
        
        //count inversion in right half
        countSmaller(A, indexArr, mid, hi);
        
        //count inversion across the left and right halves
        countSmallerAcross(A, indexArr, lo, mid, hi);
    }
public:
    vector<int> countSmaller(vector<int>& A) {
        int n = A.size();
        if(n == 0){
            return {};
        }
        
        tp.resize(n);
        res.resize(n);
        vector<int> indexArr(n);    // indexArr[i] = index of A[i]
        for(int i = 0; i < n; i++){
            indexArr[i] = i;
        }
        countSmaller(A, indexArr, 0, n);
        return res;
    }
};

int main(){
    vector<int> A = {2, 0, 1};
    Solution sol;
    vector<int> res;
    res = sol.countSmaller(A);
    cout << res;
    return 0;
}
