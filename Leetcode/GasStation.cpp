//
//  GasStation.cpp
//  Leetcode
//
//  Created by Prashant Singh on 02/06/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

class Solution {
public:
    int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
        int totalGas = accumulate(gas.begin(), gas.end(), 0);
        int totalCost = accumulate(cost.begin(), cost.end(), 0);
        if(totalGas < totalCost){   // can complete the circle
            return -1;
        }
        
        int start = 0, gasInTank = 0, n = gas.size();
        // some station i is our answer
        for(int i = 1; i < n; i++){
            gasInTank += gas[i - 1];
            gasInTank -= cost[i - 1];
            if(gasInTank < 0){
                start = i;
                gasInTank = 0;
            }
        }
        return start;
    }
};
