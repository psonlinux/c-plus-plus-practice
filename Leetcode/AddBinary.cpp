/* https://leetcode.com/problems/add-binary/
Time : O(n)
Space : O(size of both string)
*/
#include <iostream>
#include <algorithm>
using namespace std;
class Solution {
public:
    string addBinary(string a, string b) {
        if(b.size() > a.size())swap(a, b);    //a will always holds string of greater size
        int m = a.size(), n = b.size();
        string r    = "";
        int j = n - 1, sum = 0;
        for(int i = m - 1; i >= 0; i--){
            sum = sum / 2 + a[i] - '0';
            if(j >= 0){
                sum += b[j] - '0';
                j -= 1;
            }
            r += '0' + sum % 2;
        }
        if(sum / 2 == 1)r += '1';
        reverse(r.begin(), r.end());
        return r;
    }
};


int main(){
    cout << "hello";
    return 0;
}