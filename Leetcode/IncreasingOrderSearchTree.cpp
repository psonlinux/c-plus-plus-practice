/**
https://leetcode.com/problems/increasing-order-search-tree/
Time : O(n)
Space : O(n)
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
    TreeNode *p = NULL;
    TreeNode *q = NULL;
    void inorder(TreeNode *n){
        if(n){
            inorder(n->left);
            if(p == NULL){
                p = new TreeNode(n->val);
                q = p;
            }
            else{
                q->right = new TreeNode(n->val);
                q = q->right;
            }
            inorder(n->right);
        }
    }
public:
    TreeNode* increasingBST(TreeNode* r) {
        inorder(r);
        return p;
    }
};