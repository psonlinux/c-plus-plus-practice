/* https://leetcode.com/problems/search-a-2d-matrix-ii/
T : O(m + n)
S : O(m * n)
*/
class Solution {
public:
    bool searchMatrix(vector<vector<int>>& mt, int t) {
        if(mt.size() == 0)  return false;
        int r = mt.size() - 1;
        int c = mt[0].size() - 1;
        
        int i = 0, j = mt[0].size() - 1;    //start from top right item
        while(i <= r && j >= 0){
            if(t == mt[i][j])       return true;
            else if(t < mt[i][j])   j -= 1;
            else                    i += 1;
        }
        return false;
    }
};

