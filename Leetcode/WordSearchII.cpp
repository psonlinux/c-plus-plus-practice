/*
 T : O(n^3)
 S : O(n^2)
 */
/* Using trie */
#include <iostream>
#include <vector>
#include <set>
#include <string>
#include <unordered_map>
using namespace std;

class Solution {
    struct Node{    // Node of TRIE
        bool isWord;
        unordered_map<char, Node *> child;
    };
    struct Node root;
    
    /* Return 1, if inserted successfully
     otherwise 0
     */
    int insert(string word){
        int n = word.size();
        struct Node * cur = & root;
        for(int i = 0; i < n; i++){
            if(cur->child[word[i]] == NULL){    // insert the char
                cur->child[word[i]] = new Node();
            }
            cur = cur->child[word[i]];
        }
        cur->isWord = true;
        return true;
    }
    
    /* Takes O(#items in board) time .
     Takes O(#items in board) space .
     */
    void dfs(vector<vector<char>> & B, int i, int j, string & word, vector<vector<bool>> & isVisited, struct Node * cur, set<string> & res){
        if(i < 0 || i >= B.size() || j < 0 || j >= B[0].size() || isVisited[i][j]){
            return ;
        }
        
        if(cur->child[B[i][j]] == NULL){ // char not in dictionary
            return ;
        }
        
        isVisited[i][j] = true;
        word.push_back(B[i][j]);
        cur = cur->child[B[i][j]];
        if(cur->isWord){            // got a word
            res.insert(word);
        }
        
        dfs(B, i - 1, j, word, isVisited, cur, res);
        dfs(B, i + 1, j, word, isVisited, cur, res);
        dfs(B, i , j - 1, word, isVisited, cur, res);
        dfs(B, i , j + 1, word, isVisited, cur, res);
        
        // undo visit operation
        isVisited[i][j] = false;
        word.pop_back();
    }
    
public:
    vector<string> findWords(vector<vector<char>>& B, vector<string>& words) {
        // load words in dictionary
        for(int i = 0; i < words.size(); i++){
            insert(words[i]);
        }
        
        set<string> res;
        // perform dfs from every char
        int r = B.size(), c = B[0].size();
        vector<vector<bool>> isVisited(r, vector<bool>(c));
        string word;
        for(int i = 0; i < r; i++){
            for(int j = 0; j < c; j++){
                word.clear();
                dfs(B, i, j, word, isVisited, &root, res);
            }
        }
        
        vector<string> out(res.begin(), res.end());
        return out;
    }
};
