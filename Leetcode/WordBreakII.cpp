//
//  WordBreakII.cpp
//  Leetcode
//
//  Created by Prashant Singh on 29/04/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//


class Solution {
    vector<string> * wordBreak(string & s, int start, unordered_set<string> & wordSet, vector<vector<string> *> & memo){
        if(memo[start] != NULL){
            return memo[start];
        }
        
        vector<string> * res = new vector<string>();
        string w = s.substr(start, s.size() - start);
        if(wordSet.find(w) != wordSet.end()){       // is s[start...s.size() - 1] in dict
            res->push_back(w);
        }
        
        string left = "";
        for(int i = start; i < s.size() - 1; i++){
            left += s[i];
            if(wordSet.find(left) == wordSet.end()){
                continue;
            }
            
            // get all valid word break from right part
            vector<string> * rightRes = wordBreak(s, i + 1, wordSet, memo);
            for(const string rr: *rightRes){
                res->push_back(left + " " + rr);
            }
            memo[start] = res;
        }
        return res;
    }
public:
    vector<string> wordBreak(string s, vector<string>& wordDict) {
        unordered_set<string> wordSet(wordDict.begin(), wordDict.end());
        vector<vector<string> *> memo(s.size() + 1);
        memo[s.size()] = new vector<string>();
        return *wordBreak(s, 0, wordSet, memo);
    }
};
