/* https://leetcode.com/problems/kth-largest-element-in-an-array/
T : O(nlogk)
S : O(n + logk)
*/

class MinHeap{
    vector<int> pq;
    int N = 0;

public:
    MinHeap(int maxN){
        vector<int> t(maxN + 1, 0);    //pq[0] is unused . 1..maxN+1 index are used to store item
        pq = t;
    }

    int size(){
        return N;
    }

    bool isEmpty(){
        return N == 0;
    }

    int getMin(){
        if(isEmpty())
            printf("Underflow.\n");
        return pq[1];
    }

    void insert(int v){
        N += 1;
        pq[N] = v;
        swim(N);
    }

    int delMin(){
        int min = pq[1];
        swap(pq[1], pq[N]);
        N -= 1;
        sink(1);
        return min;
    }

    /*
    The item will climb up from bottom to top.
    */
    void swim(int k){
        while(k>1 && (pq[k] < pq[k/2])){
            swap(pq[k], pq[k/2]);
            k = k/2;
        }
    }

    /*
    Run heapify if parent violates the min heap property.
    */
    void sink(int k){
        while(2*k <= N){
            int j = 2*k;
            if(j < N && (pq[j+1] <  pq[j]))j += 1;
            if(pq[k] < pq[j])break;
            swap(pq[k], pq[j]);
            k = j;
        }
    }
};

class Solution {
public:
    int findKthLargest(vector<int>& nums, int k) {
        MinHeap mh(k + 1); 
        for(int y: nums){
            //simply insert in heap if count so far is less than k
            mh.insert(y);
            if(mh.size() > k)
                mh.delMin();    
        }
        return mh.getMin();
    }
};