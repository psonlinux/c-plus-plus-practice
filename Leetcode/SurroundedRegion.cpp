/*
T : O(# of item in matrix)
S : O(1)
*/
class Solution {
    void dfs(int i, int j, vector<vector<char>>& B) {
        if (i < 0 || i >= B.size() || j < 0 || j >= B[0].size())
            return;
        if (B[i][j] == 'O') {
            B[i][j] = '1';
            dfs(i, j - 1, B);
            dfs(i, j + 1, B);
            dfs(i - 1, j, B);
            dfs(i + 1, j, B);
        }
    }

    // travel in i 'th row
    void travelRow(int i, vector<vector<char>>& B) {
        for (int j = 0; j < B[0].size(); j++) {
            if (B[i][j] == 'O') {
                dfs(i, j, B);
            }
        }
    }

    // travel in j'th column
    void travelCol(int j, vector<vector<char>>& B) {
        for (int i = 0; i < B.size(); i++) {
            if (B[i][j] == 'O') {
                dfs(i, j, B);
            }
        }
    }

   public:
    void solve(vector<vector<char>>& B) {
        if (B.size() == 0) {
            return;
        }
        int m = B.size(), n = B[0].size();
        travelRow(0, B);
        travelRow(m - 1, B);
        travelCol(0, B);
        travelCol(n - 1, B);

        // flip surrounded 'O' to 'X'       // O(m * n)
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (B[i][j] == 'O') {
                    B[i][j] = 'X';
                }

                // flip all  '1' to 'O'       // O(m * n)
                if (B[i][j] == '1') {
                    B[i][j] = 'O';
                }
            }
        }
    }
};