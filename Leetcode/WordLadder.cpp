//
//  WordLadder.cpp
//  Leetcode
//
//  Created by Prashant Singh on 09/07/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//
#include "iostream"
#include "vector"
#include "queue"
#include "unordered_map"
using namespace std;

/*
 https://leetcode.com/problems/word-ladder/
 T : O(no. of words + no. of edges)
 S : O(no. of words)
 */
class Solution {
public:
    int ladderLength(string beginWord, string endWord, vector<string>& WL) {
        // create a map of string to their location
        unordered_map<string, int> sToLoc;
        int n = WL.size();
        for(int i = 0; i < n; i++){
            sToLoc[WL[i]] = i;
        }
        
        // add begin word to WL if not present
        if(sToLoc.find(beginWord) == sToLoc.end()){
            WL.push_back(beginWord);
            sToLoc[beginWord] = n;
            n += 1;
        }
        
        //start exploring the words which are nearest to source and move forward
        vector<bool> isVisited(n);
        queue<pair<int, int>> q;    // pair<word, distance from source>
        q.push(make_pair(sToLoc[beginWord], 0));
        isVisited[sToLoc[beginWord]] = true;
        while(q.empty() == false){
            pair<int, int> node = q.front();
            q.pop();
            
            // find nearest unexplored words
            string word = WL[node.first];
            for(int i = 0; i < word.size(); i++){
                char saveChar = word[i];
                for(int j = 0; j < 26; j++){
                    word[i] = 'a' + j;
                    auto it = sToLoc.find(word);
                    if(it != sToLoc.end() && word == endWord){
                        return node.second + 1 + 1;
                    }
                    
                    if(it != sToLoc.end() && isVisited[it->second] == false){
                        q.push(make_pair(it->second, node.second + 1));
                        isVisited[it->second] = true;
                    }
                }
                word[i] = saveChar; // restore i'th char
            }
        }
        return 0;
    }
};

int main(){
    Solution sol;
    string bw = "hit", ew = "cog";
    vector<string> wl = {"hot","dot","dog","lot","log"};
    cout << sol.ladderLength(bw, ew, wl) << endl;
    return 0;
}
