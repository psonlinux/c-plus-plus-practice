/* https://leetcode.com/problems/word-pattern/
T : O(pattern size)
S : O(pattern size + str size)
*/
class Solution {
public:
    bool wordPattern(string pattern, string str) {
        unordered_map<char , string> m1;
        unordered_map<string, char> m2;
        stringstream ss(str);
        string token;
        for(int i = 0; i < pattern.size(); i++){
            getline(ss, token, ' ');
            if(m1.find(pattern[i]) == m1.end() && m2.find(token) == m2.end()){
                m1[pattern[i]]  = token;
                m2[token]       = pattern[i];
            }
            else {
                if(m1[pattern[i]] != token || m2[token] != pattern[i])
                    return false;
            }
        }
        
        if(getline(ss, token, ' '))
            return false;
        return true;
    }
};