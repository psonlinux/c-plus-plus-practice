/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
 /*
 https://leetcode.com/contest/weekly-contest-110/problems/range-sum-of-bst/
Time : O(n)
Space : O(n)
 */
class Solution {
public:
    int rangeSumBST(TreeNode* r, int L, int R) {
        if(r == NULL)return 0;
        if(r->val < L)return rangeSumBST(r->right, L, R);
        if(r->val > R)return rangeSumBST(r->left, L, R);
        int ls = rangeSumBST(r->left, L, R);
        int rs = rangeSumBST(r->right, L, R);
        return r->val + ls + rs;
    }
};