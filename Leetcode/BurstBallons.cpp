/*
T : O(n^2)
S : O(n^2)
*/
class Solution {
    int maxCoins(int i, int j, vector<int>& v, vector<vector<int>>& cache){
        if(cache[i][j] != -1)
            return cache[i][j];
        if(j - i + 1 < 3)
            return 0;
        
        int mx = 0;
        for(int k = i + 1; k < j; k++){
            int x = v[i] * v[k] * v[j] + maxCoins(i, k, v, cache) + maxCoins(k, j, v, cache);
            mx = max(mx, x);
        }
        
        cache[i][j] = mx;
        return mx;
    }
public:
    int maxCoins(vector<int>& v) {
        vector<int> cs(v.size() + 2);
        int n = cs.size();
        cs[0]       = 1;
        cs[n - 1]   = 1;
        for(int i = 0; i < v.size(); i++){
            cs[i + 1] = v[i];
        }
        
        vector<vector<int>> cache(n, vector<int>(n, -1));
        return maxCoins(0, n - 1, cs, cache);
    }
};