/*
https://leetcode.com/problems/binary-subarrays-with-sum/
Time : O(n)
Space : O(1)
*/
class Solution {
    //handle case when S == 0
    int zeroSum(vector<int> & A){
        int t = 0;
        int c = 0;
        for(int i=0; i<A.size(); i++){
            if(A[i] == 0){
                c += 1;
                t += c;
            }
            else
                c = 0;
        }
        return t;
    }
public:
    int numSubarraysWithSum(vector<int>& A, int S) {
        if(S == 0)
            return zeroSum(A);
        
        int sa = 0;
        int sum = 0;
        int lz = 0;     //# left side zero
        int rz = 0;     //# right side zero
        
        int lone = -1;  //index of left one 
        int rone = -1;  //index of right one
        
        bool foundSum = false;
        for(int i = 0; i < A.size(); i++){
            sum += A[i];
            
            if(A[i] == 0){     //count left zero
                if(lone == -1)
                    lz += 1;
                if(rone != -1)    //count right 
                    rz += 1;
            }else{
                if(lone == -1)
                    lone = i;
                
                if(sum == S){        //index of rightmost one of sub array
                    rone = i;
                    foundSum = true;
                }
                
                //count subarray
                if(sum > S){
                    sa += (lz + 1) * (rz + 1);
                    
                    //setup for next subarray
                    rz = 0;
                    
                    //find lone
                    lone += 1;
                    lz = 0;
                    while(A[lone] == 0){
                        lone += 1;
                        lz += 1;
                    }
                       
                    rone = i;
                    sum = S;
                }   
            }
        }
        
        if(foundSum)
            sa += (lz + 1) * (rz + 1);
        return sa;
    }
};