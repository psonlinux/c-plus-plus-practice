//
//  MedianOfSortedArr.cpp
//  Leetcode
//
//  Created by Prashant Singh on 29/04/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

/*
 T : O(total/2 * log total/2)
 S : O(total / 2)
 total = m + n
 */
class Solution {
public:
    double findMedianSortedArrays(vector<int>& M, vector<int>& N) {
        // Key idea : Median is that item which is greater than n / 2
        // items
        // Get a Max heap to keep item smaller than Median
        int m = M.size(), n = N.size();
        int i = 0, j = 0;
        priority_queue<int> maxHeap;
        int half = (m + n) / 2;
        for(int k = 0; k < half; k++){
            if(i < m && j < n){
                if(M[i] < N[j]){
                    maxHeap.push(M[i]);
                    i += 1;
                }
                else{
                    maxHeap.push(N[j]);
                    j += 1;
                }
            }
            else if(i == m){
                maxHeap.push(N[j]);
                j += 1;
            }
            else if(j == n){
                maxHeap.push(M[i]);
                i += 1;
            }
        }
        
        int total = m + n;
        double med = 0;
        
        if(i == m){
            med = N[j];
        }
        else if(j == n){
            med =  M[i];
        }
        else{
            if(M[i] < N[j]){
                med = M[i];
            }
            else{
                med =  N[j];
            }
        }
        
        // odd case
        if(total % 2 == 1){
            return med;
        }
        else{   // even case
            return (med + maxHeap.top()) / 2 ;
        }
    }
};
