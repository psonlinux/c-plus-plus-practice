/*
https://leetcode.com/problems/word-break/
*/
/*
T : O(n)
S : O(n)
n = string size
*/
class Solution {
    /*
    Check if s can be made from dictionary word .
    */
    bool canBeMade(string & s, int start, unordered_set<string> & dict, vector<bool> & cache){
        if(start >= s.size()) 
            return true;            //empty string "" can always be made from dictionary
        if(cache[start] == false)      // return saved value
            return cache[start];
        for(int i = start; i < s.size(); i++){
            string word = s.substr(start, i - start + 1);
            if(dict.find(word) != dict.end()){
                if(canBeMade(s, i + 1, dict, cache)){
                    return true;
                }
            }
        }
        cache[start] = false;
        return cache[start];
    }
public:
    bool wordBreak(string s, vector<string>& wordDict) {
        // put words in map
        unordered_set<string> dict(wordDict.begin(), wordDict.end());
        
        // memoize
        vector<bool> cache(s.size() + 1, true);    // cache[i] = true, if string from [i ... end] can be made from dictionary word
        cache[0] = canBeMade(s, 0, dict, cache);
        return cache[0];
    }
};