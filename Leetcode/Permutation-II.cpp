/*
https://leetcode.com/problems/permutations-ii/
Time : O(n * n!)
Space : O(n * n!)
*/
class Solution {
public:
    vector<vector<int>>  permute(vector<int> v){
        vector<vector<int>> pms;
        if(v.size() == 0)return pms;
        if(v.size() == 1){
            pms.push_back(v);
            return pms;
        }
        
        for(int i = 0; i < v.size(); i++){
            if(i != 0 && v[i] == v[i-1])continue;
            int c = v[i];
            
            vector<int> rem = v;
            rem.erase(rem.begin() + i);
            //find all permutation starting with v[i]
            vector<vector<int>> p = permute(rem);
            for(int j = 0; j < p.size(); j++){
                p[j].push_back(c);
                pms.push_back(p[j]);
            }
        }
        return pms;
    }
    
    vector<vector<int>> permuteUnique(vector<int>& nums) {
        sort(nums.begin(), nums.end());
        return permute(nums);
    }
};