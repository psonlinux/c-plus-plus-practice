/*
T : O(m * n)
S : O(min(m, n)/2)
*/
class Solution {
    void rowMat(vector<int>& ob, vector<vector<int>>& mat, int x) {
        for (int j = x; j < mat[0].size() - x; j++) {
            ob.push_back(mat[x][j]);
        }
    }

    void colMat(vector<int>& ob, vector<vector<int>>& mat, int x) {
        for (int i = x; i < mat.size() - x; i++) {
            ob.push_back(mat[i][x]);
        }
    }

    /* Recursively save outer boundary .*/
    void outerBoundary(vector<int>& ob, vector<vector<int>>& mat, int x) {
        int rows = mat.size(), cols = mat[0].size();
        if (cols - 2 * x <= 0 || rows - 2 * x <= 0) {
            return;
        }

        if (cols - 2 * x == 1 && rows - 2 * x > 0) {  // for even columns
            colMat(ob, mat, x);
            return;
        }
        if (rows - 2 * x == 1 && cols - 2 * x > 0) {  // for odd column
            rowMat(ob, mat, x);                       // push center item
            return;
        }

        // save n - 1 item of first row
        for (int j = x; j < cols - x - 1; j++) {
            ob.push_back(mat[x][j]);
        }

        // save n - 1 item of last col
        for (int i = x; i < rows - x - 1; i++) {
            ob.push_back(mat[i][cols - 1 - x]);
        }

        //save n - 1 item of last row
        for (int j = cols - 1 - x; j > x; j--) {
            ob.push_back(mat[rows - 1 - x][j]);
        }

        //save n - 1 item of first col
        for (int i = rows - 1 - x; i > x; i--) {
            ob.push_back(mat[i][x]);
        }

        outerBoundary(ob, mat, x + 1);
    }

   public:
    vector<int> spiralOrder(vector<vector<int>>& matrix) {
        if (matrix.size() == 0) {
            return {};
        }

        int rows = matrix.size(), cols = matrix[0].size();
        vector<int> ob;
        // Otherwise Recursively print outer boundary
        outerBoundary(ob, matrix, 0);
        return ob;
    }
};