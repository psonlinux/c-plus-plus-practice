/* https://leetcode.com/problems/x-of-a-kind-in-a-deck-of-cards/
T : O(n)
S : O(n)
*/
class Solution {
    int gcd(int a, int b){
        if(b == 0)
            return a;
        return gcd(b, a % b);
    }
public:
    bool hasGroupsSizeX(vector<int>& deck) {
        unordered_map<int, int> m;
        for(int i = 0; i < deck.size(); i++){
            m[deck[i]] += 1;
        }
        
        unordered_map<int, int>::iterator it = m.begin();
        int g = it->second;
        for(it = m.begin(); it != m.end(); it++){
            g =  gcd(g, it->second);
        }
        
        if(g == 1)return false;
        return true;
    }
};