/*
T : O(n logn)
S : O(n)
*/
class MedianFinder {
    // create two heap
    priority_queue<int> max;
    priority_queue<int, vector<int>, greater<int>> min;

   public:
    /** initialize your data structure here. */
    MedianFinder() {
    }

    void addNum(int num) {
        if (max.empty() == true) {
            max.push(num);
        } else {
            if (num <= max.top()) {  // num goes to left of current median
                max.push(num);
            } else {
                min.push(num);
            }
        }

        // for odd item max.top() is the median
        // size of max heap is always >= min heap
        if ((max.size() < min.size())) {
            max.push(min.top());
            min.pop();
        }

        // max heap size should be equal to min heap or exceed by atmost 1
        if (max.size() - min.size() >= 2) {
            min.push(max.top());
            max.pop();
        }
    }

    double findMedian() {
        if (max.empty() == true) {  //no item present
            cout << "No item present in list.\n";
        }

        // even case
        double m = 0;
        if (max.size() == min.size()) {
            m = max.top() + (double)(min.top() - max.top()) / 2;
        } else {
            m = max.top();
        }
        return m;
    }
};

/**
 * Your MedianFinder object will be instantiated and called as such:
 * MedianFinder obj = new MedianFinder();
 * obj.addNum(num);
 * double param_2 = obj.findMedian();
 */