//
//  SubSetsII.cpp
//  Leetcode
//
//  Created by Prashant Singh on 28/07/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//
// https://leetcode.com/problems/subsets-ii/
class Solution {
    /* 3 4 8 9 9 8 6
     sub : subsets of all items whose frequency is one ex {3, 4, 6}
     item : item with count more than 1 . ex : 8
     count : count of item
     */
    void dupSubset(int item, int count, vector<vector<int>> & sub){
        int size = sub.size();
        vector<int> ls;
        for(int i = 0; i < count; i++){
            for(int j = i * size; j < i * size + size; j++){
                ls = sub[j];
                ls.push_back(item);
                sub.push_back(ls);
            }
        }
    }
    
    void powerSet(int start, vector<int> & noDup, vector<int> & combSoFar, vector<vector<int>> & ps){
        if(start == noDup.size()){
            ps.push_back(combSoFar);
            return ;
        }
        
        //include
        combSoFar.push_back(noDup[start]);
        powerSet(start + 1, noDup, combSoFar, ps);
        
        //exclude
        combSoFar.pop_back();
        powerSet(start + 1, noDup, combSoFar, ps);
    }
public:
    vector<vector<int>> subsetsWithDup(vector<int>& A) {
        //get all unique items
        unordered_map<int, int> um;
        int n = A.size();
        for(int i = 0; i < n; i++){
            um[A[i]] += 1;
        }
        
        //separate dup, noDup items
        vector<int> noDup;
        for(auto it = um.begin(); it != um.end(); it++){
            if(it->second == 1){
                noDup.push_back(it->first);
            }
        }
        
        //get power set of noDup items
        vector<vector<int>> ps;
        vector<int> combSoFar;
        powerSet(0, noDup, combSoFar, ps);
        
        //get power with duplicates
        for(auto it = um.begin(); it != um.end(); it++){
            if(it->second > 1){
                dupSubset(it->first, it->second, ps);
            }
        }
        return ps;
    }
};
