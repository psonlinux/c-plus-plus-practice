/** https://leetcode.com/problems/remove-nth-node-from-end-of-list/
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* removeNthFromEnd(ListNode* head, int n) {
        ListNode * p = head, * q = head;
        n = n;
        while(q->next && n > 0){
            q = q->next;
            n -= 1;
        }
        
        if(n == 1){      //remove first item
            head = head->next;
            return head;
        }
            
        while(p->next && q->next){
            p = p->next;
            q = q->next;
        }
        
        if(p->next)
            p->next = p->next->next;
        return head;
    }
};