/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Codec {
    void ser(TreeNode *r, stringstream &ss){
        if(r == NULL){
            ss << " n ";
        }
        else{
            ss << r->val << " ";
            ser(r->left, ss);
            ser(r->right, ss);
        }
    }
    
    TreeNode * des(stringstream & ss){
        string str;
        ss >> str;
        if(str == "n"){
            return NULL;
        }
        else{
            TreeNode * r = new TreeNode(stoi(str));
            r->left = des(ss);
            r->right = des(ss);
            return r;
        }
    }
public:

    // Encodes a tree to a single string.
    string serialize(TreeNode* root) {
        stringstream ss;
        ser(root, ss);
        return ss.str();
    }

    // Decodes your encoded data to tree.
    TreeNode* deserialize(string data) {
        stringstream ss(data);
        return des(ss);
    }
};

// Your Codec object will be instantiated and called as such:
// Codec codec;
// codec.deserialize(codec.serialize(root));