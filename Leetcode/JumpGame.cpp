/* https://leetcode.com/problems/jump-game/
T : O(n)
S : O(n)
*/
class Solution {
public:
    bool canJump(vector<int>& nums) {
        if(nums.size() == 0)
            return false;
        if(nums.size() == 1)
            return true;
        int d = nums.size() - 1;    // destination index
        /* Iterate from back.
        Make i as new destination , if jumping from i can reach to index d
        */
        for(int i = nums.size() - 2; i >= 0; i--){
            if(i + nums[i] >= d)        
                d = i;
        }
        
        if(d == 0)  return true;
        else        return false;
    }
};