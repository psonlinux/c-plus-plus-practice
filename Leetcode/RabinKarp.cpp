/*
T : O(n)
S : O(1)
*/
class Solution {
    long hash(string & s, int start, int end){
        long h = 0;
        for(int i = start; i < end; i++){
            h += (s[i] - 'a');
        }
        return h;
    }
public:
    int strStr(string haystack, string needle) {
        if(needle == ""){
            return 0;
        }
        
        long needleHash = hash(needle, 0, needle.size()), m = needle.size();
        
        long subHash = hash(haystack, 0, m);   // substring hash
        if(needleHash == subHash){
            if(needle == haystack.substr(0, m)){
                return 0;
            }        
        }
        
        for(int i = m; i < haystack.size(); i++){
            // remove most significant digit
            subHash = subHash - (haystack[i - m] - 'a');
            
            //add
            subHash = subHash + haystack[i] - 'a';
            
            //check
            if(subHash == needleHash){
                if(needle == haystack.substr(i - m + 1, m)){
                    return i - m + 1;
                }
            }
        }
        return -1;
    }
};