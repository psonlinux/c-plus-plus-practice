//
//  KConMaxSum.cpp
//  Leetcode
//
//  Created by Prashant Singh on 18/09/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

class Solution {
    int MOD = 1e9 + 7;
public:
    int kConcatenationMaxSum(vector<int>& A, int k) {
        int n = A.size();
        if(n == 0){
            return 0;
        }
        
        //case 1: maxSoFar is not BIG
        long total      = accumulate(A.begin(), A.end(), 0);
        int maxSoFar    = 0, maxEndHere = 0, sum = 0;
        for(int i = 0; i < n; i++){
            sum = (maxEndHere % MOD + A[i] % MOD) % MOD;
            if(sum > A[i]){
                maxEndHere = sum;
            }
            else{
                maxEndHere = A[i];
            }
            maxSoFar = max(maxSoFar, maxEndHere);
        }
        
        if(k <= 1){
            return maxSoFar;
        }
        
        //case 2:
        // sum with end removed . Big maxSoFar .
        // sum of all k copies of A
        int sumK = 0;
        for(int i = 0; i < k; i++){
            sumK = (sumK + total) % MOD;
        }
        int trimmedSum = sumK - (total - maxSoFar);
        
        //case 3: minSoFar is BIG
        int minSoFar = 0, minEndHere = 0;
        for(int i = 0; i < n; i++){
            sum = minEndHere + A[i];
            if(sum < A[i]){
                minEndHere = sum;
            }
            else{
                minEndHere = A[i];
            }
            minSoFar = min(minSoFar, minEndHere);
        }
        int maxAcross = total - minSoFar;
        
        return max(max(maxSoFar, trimmedSum), maxAcross);
    }
};
