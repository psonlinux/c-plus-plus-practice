
#include<iostream>
#include<algorithm>
#include<string>
using namespace std;
class Solution {
public:
    string addStrings(string num1, string num2) {
        if(num2.size() < num1.size())swap(num1, num2);
        int c = 0, sum = 0;
        string s = "";
        for(int i = num2.size() - 1; i >= 0; i--){
            sum =  (num2[i] - '0') + c;     //sum
            
            int j = i - (num2.size() - num1.size());
            if(j >= 0)sum += (num1[j] - '0');
            
            c = sum / 10;           //carry
            s +=  sum % 10 + '0';  
        }
        if (c == 1)s = s + '1';     
        reverse(s.begin(), s.end());        
        return s;
    }
};

int main()
{
    /* code */
    cout << "HELLO\n";
    Solution sol; 
    sol.addStrings("18","190");
    return 0;
}
