//
//  Battleship.cpp
//  Leetcode
//
//  Created by Prashant Singh on 09/07/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

class Solution {
public:
    int countBattleships(vector<vector<char>>& B) {
        int x = 0;
        int R = B.size(), C = B[0].size();
        for(int i = 0; i < R; i++){
            for(int j = 0; j < C; j++){
                if(i == 0 && j == 0 && B[i][j] == 'X'){
                    x += 1;
                }
                else if(i > 0 && j > 0 && B[i][j] == 'X'){
                    if(B[i][j - 1] == '.' && B[i - 1][j] == '.'){
                        x += 1;
                    }
                }
                else if(i == 0){
                    if(B[i][j] == 'X' && B[i][j - 1] == '.'){
                        x += 1;
                    }
                }
                else if(j == 0){
                    if(B[i][j] == 'X' && B[i - 1][j] == '.'){
                        x += 1;
                    }
                }
            }
        }
        return x;
    }
};
