/*
T : O(n)
S : O(n)
*/
class Solution {
public:
    int longestConsecutive(vector<int>& nums) {
        int n = nums.size();
        unordered_set<int> isPt; // check if nums[i] is present
        for(int i = 0; i < n; i++){
            isPt.insert(nums[i]);
        }
        
        int mL = 0;
        //check range of each item
        for(int i = 0; i < n; i++){
            if(isPt.find(nums[i]) == isPt.end())
                continue;
            
            int c = 1;
            // check range length of nums[i]
            // left
            int x = nums[i] - 1;
            while(isPt.find(x) != isPt.end()){
                c += 1;
                isPt.erase(x);      // erase once used in range, to remove recounting
                x -= 1;
            }
            
            // right
            x = nums[i] + 1;
            while(isPt.find(x) != isPt.end()){
                c += 1;
                isPt.erase(x);
                x += 1;
            }
            
            // is this is max
            if(c > mL)mL = c;
        }
        
        return mL;
    }
};