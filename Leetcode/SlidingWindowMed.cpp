/*
In multiset , a item can be deleted in LOGN time, which can not be done in priority_queue !! .
It keeps its data sorted internally .
By default the data is in ascending order .
Can access MIN amd MAX item in CONSTANT Time .
T : O(n log k)
S : O(k)
*/
class Solution {
    void balance(multiset<int>& wl, multiset<int>& wr) {
        if (wl.size() < wr.size()) {
            wl.insert(*wr.begin());
            wr.erase(wr.begin());
        }

        if (wl.size() - wr.size() > 1) {
            multiset<int>::iterator it = wl.end();
            --it;
            wr.insert(*it);
            wl.erase(it);
        }
    }

   public:
    vector<double> medianSlidingWindow(vector<int>& nums, int k) {
        int n = nums.size();
        vector<double> res;
        if (n == 0 || k == 0) {
            return res;
        }

        /*
        Left contain items <= median.
        Right contain items > median
        */
        multiset<int> wl, wr;
        for (int i = 0; i < n; i++) {
            // remove item not in current window
            if (i >= k) {
                if (nums[i - k] <= *(wl.rbegin())) {
                    wl.erase(wl.find(nums[i - k]));
                } else {
                    wr.erase(wr.find(nums[i - k]));
                }

                // rebalance both halfs
                balance(wl, wr);
            }

            // insert new items
            if (wl.empty() == true) {
                wl.insert(nums[i]);
            } else {
                // insert in left of median
                if (nums[i] <= *(wl.rbegin())) {
                    wl.insert(nums[i]);
                } else {
                    wr.insert(nums[i]);
                }
            }

            // rebalance both halfs
            balance(wl, wr);

            // save median
            double med = 0;
            if (i >= k - 1) {
                if (k % 2 == 1) {
                    med = *wl.rbegin();
                } else {
                    med = *wl.rbegin() + ((double)*wr.begin() - *wl.rbegin()) / 2.0;
                }
                res.push_back(med);
            }
        }
        return res;
    }
};