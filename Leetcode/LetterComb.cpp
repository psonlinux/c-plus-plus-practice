/* https://leetcode.com/problems/letter-combinations-of-a-phone-number/
T : O(no. of combinations)
S : O(no. of combinations)
*/
class Solution {
    void getCombination(vector<string> & cb, string & dl){
        int n = cb.size();
        for(int i = 0; i < n; i++){     // iterate over combinations
            for(int j = 1; j < dl.size(); j++){
                cb.push_back(cb[i] + dl[j]);
            }
            cb[i] += dl[0];
        }
    }
public:
    vector<string> letterCombinations(string digits) {
        int n = digits.size();
        vector<string> empty;
        if(n == 0)
            return empty;
        vector<string> map = {"", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
        vector<string> cb(1);
        for(int i = 0; i < digits.size(); i++){
            string s = map[digits[i] - '0'];
            getCombination(cb, s);
        }
        return cb;
    }
};