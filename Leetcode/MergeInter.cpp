/**
 * Definition for an interval.
 * struct Interval {
 *     int start;
 *     int end;
 *     Interval() : start(0), end(0) {}
 *     Interval(int s, int e) : start(s), end(e) {}
 * };
 */
/* https://leetcode.com/problems/merge-intervals/
T : O(nlogn)
S : O(n)
*/
struct comp{
    bool operator()(const vector<int> & a, const vector<int> & b){
        return a[0] < b[0];
    }
}comp;

class Solution {
public:
    vector<vector<int>> merge(vector<vector<int>>& I) {
        int n = I.size();
        vector<vector<int>> ret;
        if(n == 0){
            return ret;
        }
        
        sort(I.begin(), I.end(), comp);
        
        vector<int> x = I[0];
        for(int i = 1; i < n; i++){
            if(I[i][0] > x[1]){
                ret.push_back(x);
                x = I[i];
            }
            else{
                x[1] = max(x[1], I[i][1]);;
            }
        }
        ret.push_back(x);
        return ret;
    }
};
