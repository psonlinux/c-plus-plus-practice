/*
https://leetcode.com/problems/monotonic-array/
Time : O(n)
Space : O(1)
*/
class Solution {
public:
    bool isMonotonic(vector<int>& A) {
        bool inc = true, dec = true;
        for(int i = 1; i < A.size(); i++){
            inc = inc && (A[i] >= A[i - 1]);
            dec = dec && (A[i] <= A[i - 1]);
        }
        return inc || dec;
    }
};