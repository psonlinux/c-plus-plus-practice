/* https://leetcode.com/problems/number-of-islands/
T : O(size of matrix)
S : O(size of matrix)
*/
class Solution {
    /* Make grid[i][j] zero
    */
    void rem(int i, int j, vector<vector<char>>& grid){
        grid[i][j] = '0';
        if(i - 1 >= 0 && grid[i - 1][j] == '1'){          // if up is 1 remove it
            rem(i - 1, j, grid);
        }
        
        if(i + 1 < grid.size() && grid[i + 1][j] == '1'){ // if down is one remove it
            rem(i + 1, j, grid);
        }
        
        if(j - 1 >= 0 && grid[i][j - 1] == '1'){          // if left is 1, rem it
            rem(i, j - 1, grid);
        }
        
        if(j + 1 < grid[0].size() && grid[i][j + 1] == '1'){ //if right is 1, rem it
            rem(i, j + 1, grid);
        }
    }
public:
    int numIslands(vector<vector<char>>& grid) {
        int r = grid.size();
        if(r == 0)
            return 0;
        int c = grid[0].size();
        
        int islands = 0;
        for(int i = 0; i < r; i++){
            for(int j = 0; j < c; j++){
                if(grid[i][j] == '1'){
                    islands += 1;
                    rem(i, j, grid);
                }
            }
        }
        return islands;
    }
};