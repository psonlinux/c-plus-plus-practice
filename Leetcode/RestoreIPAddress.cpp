/*
The key idea for solving this question is to generalize the number of dots.
What if we have to place only one dot to make valid ip ?
What if we have to place only two dot to make valid ip ?
...
*/
class Solution {
    /*
    Counts digit in given integer .
    */
    int getDigitCount(int n){
        if(n == 0){
            return 1;
        }
        
        int c = 0;
        while(n){
            c += 1;
            n = n / 10;
        }
        return c;
    }
    
    /*
    Return true if decimal value of string is in between 0 and 255
    */
    bool isValidNum(string & s, int start, int end){
        int res = 0;
        for(int i = start; i < end; i++){
            res = res * 10 + s[i] - '0';
            if(res > 255){      // range exceed
                return false;
            }
        }
        
        // return false digit count and string size are different
        if(getDigitCount(res) != (end - start) ){
            return false;
        }
        
        return true;
    }
    
    /*
    Saves the valid IP .
    */
    void isValid(string & s, int start, int remDotsToPlace, string ipSoFar, vector<string> & vip){  
        if(start == s.size()){
            return;    
        }
        
        if(remDotsToPlace == 0){  // no dots to place
            // if valid ip then add it to vector
            string sub = s.substr(start);
            if(isValidNum(sub, 0, sub.size()) == true){
                ipSoFar += sub;
                vip.push_back(ipSoFar);
            }
            else{
                return;
            }
        }
        
        // consider first 3 digits only
        for(int i = start; i < s.size() && i < start + 3; i++){
            //check first part
            if(isValidNum(s, start, i + 1) == false){
                return;
            }

            ipSoFar += s[i];
            isValid(s, i + 1, remDotsToPlace - 1, ipSoFar + '.', vip);  
        }
    }
public:
    vector<string> restoreIpAddresses(string s) {
        vector<string> vip;     // valid ip address
        string ipSoFar = "";
        isValid(s, 0, 3, ipSoFar, vip);
        return vip;
    }
};