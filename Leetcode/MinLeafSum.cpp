/*
https://practice.geeksforgeeks.org/problems/sum-of-leaf-nodes-at-min-level/1
Time : O(n)
Space : O(1)
*/

void getSum(Node *r, int l, int &sum, int minLev){
    if(r){
        if(l == minLev && (!r->left && !r->right)){
            sum += r->data;
        }
        else{
            getSum(r->left, l + 1, sum, minLev);
            getSum(r->right, l + 1, sum, minLev);
        }
    }
}
void getMinLevel(Node * r, int l, int & minLev){
    if (r){
        if(!r->left && !r->right){
            if(l < minLev)minLev = l;
        }
        else{
            getMinLevel(r->left, l + 1, minLev);
            getMinLevel(r->right, l + 1, minLev);
        }
    }
}

// Your task is to complete this function
// function shoudl return the required sum
int minLeafSum(Node* root)
{
	int minLev = INT_MAX;
	getMinLevel(root, 0, minLev);
	
	int sum = 0;
	getSum(root, 0, sum, minLev);
	return sum;
}