//
//  KnightWalk.cpp
//  Leetcode
//
//  Created by Prashant Singh on 03/12/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//
// https://practice.geeksforgeeks.org/problems/knight-walk/0
#include<iostream>
#include<vector>
#include<string>
#include<unordered_map>
#include<queue>
#include<unordered_set>
#include<algorithm>
using namespace std;
struct Info{
    int x, y, d;
    Info(int _x, int _y, int _d){
        x = _x;
        y = _y;
        d = _d;
    }
    
    Info(){};
};

bool isValidNext(int x, int y, vector<vector<bool>> &vis){
    if(x < 1 || x >= vis.size() || y < 1 || y >= vis[0].size() || vis[x][y] == true){
        return false;
    }
    return true;
}

int minStep(int M, int N, int s1, int s2, int d1, int d2){
    
    vector<vector<bool>> vis(M + 1, vector<bool>(N + 1));
    Info start(s1, s2, 0);
    queue<Info> q;
    
    q.push(start);
    vis[s1][s2] = true;
    while(q.empty() == false){
        Info node = q.front();
        q.pop();
        if(node.x == d1 && node.y == d2){
            return node.d;
        }
        
        for(int i = -2; i <= 2; i++){
            for(int j = -2; j <= 2; j++){
                if(abs(i) + abs(j) == 3){
                    if(isValidNext(node.x + i, node.y + j, vis) == true){
                        q.push(Info(node.x + i, node.y + j, node.d + 1));
                        vis[node.x + i][node.y + j] = true;
                    }
                }
            }
        }
    }
    return -1;
}

int main() {
    int T = 0, t = 0;
    cin >> T;
    while(t < T){
        int m = 0, n = 0;
        cin >> m >> n;
        
        int s1, s2, d1, d2;
        cin >> s1 >> s2 >> d1 >> d2;
        int ret = minStep(m, n, s1, s2, d1, d2);
        cout << ret << endl;
        t += 1;
    }
    return 0;
}
