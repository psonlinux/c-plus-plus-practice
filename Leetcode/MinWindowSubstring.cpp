/*
T : O(2 * s.size()) , every character is touched two times
S : O(t.size())
*/
class Solution {
   public:
    string minWindow(string s, string t) {
        // let index of minimum window
        int p = -1, q = s.size();

        // put string t in map for fast lookup
        unordered_map<char, int> tMap;
        for (int i = 0; i < t.size(); i++) {
            tMap[t[i]] += 1;
        }

        int remToCover = t.size();  // item remain to cover
        // currentCover keeps the count of all char occuring in 't' between two
        // pointer i and j .
        unordered_map<char, int> currentCover;
        int j = 0;

        // The minimum window must start at some i
        for (int i = 0; i < s.size(); i++) {
            while (j < s.size() && remToCover > 0) {
                if (tMap[s[j]] > 0) {  // current char is in t
                    if (currentCover[s[j]] < tMap[s[j]]) {
                        remToCover -= 1;
                    }
                    currentCover[s[j]] += 1;
                }
                j += 1;
            }

            // covered
            if (remToCover == 0 && (j - i < q - p)) {
                p = i;
                q = j;
            }

            // remove s[i] from current cover
            if (tMap[s[i]] > 0) {
                currentCover[s[i]] -= 1;
                if (currentCover[s[i]] < tMap[s[i]]) {
                    remToCover += 1;
                }
            }
        }

        if (q - p <= s.size())
            return s.substr(p, q - p);
        return "";
    }
};