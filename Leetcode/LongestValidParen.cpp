//
//  LongestValidParen.cpp
//  Leetcode
//
//  Created by Prashant Singh on 10/06/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

/*
 T : O(n)
 S : O(n)
 Idea : try to break in smaller sub problem .
 case 1: string ends at "()"
 longestParenEnding(0, i) = longestParenEnding(0, i - 2) + 2
 
 case 2: string ends with "))" for example "()(())"
 longestParenEnding(0, i) =
 
 case 3: string ends with "("
 longestParenEnding(0, i) = 0
 */
class Solution {
    int lonParen(int i, string & s, vector<int> & memo){
        if(i <= 0){
            return 0;
        }
        
        if(memo[i] != -1){
            return memo[i];
        }
        
        if(s[i] == '('){
            memo[i] = 0;
        }
        else if(s[i] == ')' && s[i - 1] == '('){
            memo[i] = lonParen(i - 2, s, memo) + 2;
        }
        else if(s[i] == ')' && s[i - 1] == ')'){
            int len = lonParen(i - 1, s, memo);
            if(i - 1 - len >= 0 && s[i - 1 - len] == '('){
                memo[i] = len + 2 + lonParen(i - len - 2, s, memo);
            }
            else{
                memo[i] = 0;
            }
        }
        return memo[i];
    }
public:
    int longestValidParentheses(string s) {
        
        int n = s.size(), maxLen = 0;
        vector<int> memo(n, -1);
        for(int i = 0; i < n; i++){
            maxLen = max(maxLen, lonParen(i, s, memo));
        }
        return maxLen;
    }
};
