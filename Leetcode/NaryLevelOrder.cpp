/*
https://leetcode.com/problems/n-ary-tree-level-order-traversal/
Time : O(n)
Space : O(n)
*/
/*
// Definition for a Node.
class Node {
public:
    int val = NULL;
    vector<Node*> children;

    Node() {}

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};
*/
class Solution {
public:
    vector<vector<int>> levelOrder(Node* r) {
        vector<vector<int>> sol;
        if(!r)return sol;
        
        queue<Node *> q;
        q.push(r);
        while(!q.empty()){
            int size = q.size();
            vector<int> nodes;
            for(int i = 0; i < size; i++){
                Node *n = q.front();
                nodes.push_back(n->val);
                for(int j = 0; j < n->children.size(); j++){
                    if(n->children[j])q.push(n->children[j]);    
                }
                q.pop();
            }
            sol.push_back(nodes);
        }
        return sol;
    }
};