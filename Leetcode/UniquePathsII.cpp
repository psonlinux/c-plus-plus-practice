//
//  UniquePathsII.cpp
//  Leetcode
//
//  Created by Prashant Singh on 28/05/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

class Solution {
public:
    int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid) {
        int m = obstacleGrid.size(), n = obstacleGrid[0].size();
        vector<vector<long>> G(m, vector<long>(n, 0));
        
        // Intialization
        // We can reach G[0][0] in one way
        if(obstacleGrid[0][0] == 0){
            G[0][0] = 1;
        }
        
        for(int i = 0; i < m; i++){
            for(int j = 0; j < n; j++){
                if(obstacleGrid[i][j] == 0){
                    if(i - 1 >= 0){
                        G[i][j] = G[i - 1][j];
                    }
                    
                    if(j - 1 >= 0){
                        G[i][j] += G[i][j - 1];
                    }
                }
            }
        }
        return G[m - 1][n - 1];
    }
};
