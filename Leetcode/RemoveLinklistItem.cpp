/** https://leetcode.com/problems/remove-linked-list-elements/
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
/*
T : O(n)
S : O(n)
*/
class Solution {
public:
    ListNode* removeElements(ListNode* head, int val) {
        if(head == NULL)
            return head;
        ListNode *p = head, *c = head->next;
        while(c){
            if(c->val == val)
                p->next = c->next;
            else
                p = p->next;    
            c = c->next;
        }
        
        if(head->val == val)head = head->next;
        return head;
    }
};