class Trie {
    struct Node{
        unordered_map<char, Node *> map;
        bool isWord = false;;
    };
    
    Node * root; 
public:
    /** Initialize your data structure here. */
    Trie() {
        root = new Node;
    }
    
    /** Inserts a word into the trie. */
    /* T : O(word size)
    */
    void insert(string word) {
        Node * cur = root;
        int i ;
        for(i = 0; i < word.size(); i++){
            if(cur->map[word[i]] == NULL)
                cur->map[word[i]] = new Node;
            cur = cur->map[word[i]];
        }
        cur->isWord = true;
    }
    
    /* T : O(string size)
    */
    /** Returns if the word is in the trie. */
    bool search(string word) {
        Node * cur = root;
        for(int i = 0; i < word.size(); i++){
            if(cur->map[word[i]] == NULL)
                return false;
            cur = cur->map[word[i]];
        }
        
        if(cur->isWord)
            return true;
        return false;
    }
    
    /* T : O(string size)
    */
    /** Returns if there is any word in the trie that starts with the given prefix. */
    bool startsWith(string prefix) {
        Node * cur = root;
        for(int i = 0; i < prefix.size(); i++){
            if(cur->map[prefix[i]] == NULL)
                return false;
            cur = cur->map[prefix[i]];
        }
        return true;
    }
};

/**
 * Your Trie object will be instantiated and called as such:
 * Trie obj = new Trie();
 * obj.insert(word);
 * bool param_2 = obj.search(word);
 * bool param_3 = obj.startsWith(prefix);
 */