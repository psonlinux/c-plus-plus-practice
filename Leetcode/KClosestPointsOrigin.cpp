//
//  KClosestPointsOrigin.cpp
//  Leetcode
//
//  Created by Prashant Singh on 25/09/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

class Solution {
    
    bool isLess(const vector<int> &a, const vector<int> &b){
        float d1 = sqrt(a[0] * a[0] + a[1] * a[1]);
        float d2 = sqrt(b[0] * b[0] + b[1] * b[1]);
        return  d1 <= d2;
    }
    
    int partition(vector<vector<int>>& P, int lo, int hi){
        int i = lo, j = hi;
        while(true){
            while(i <= j && isLess(P[i], P[lo]) == true){
                i += 1;
            }
            
            while(i <= j && isLess(P[j], P[lo]) == false){
                j -= 1;
            }
            
            if(i >= j){
                break;
            }
            
            swap(P[i], P[j]);
            i += 1;
            j -= 1;
        }
        swap(P[lo], P[j]);
        return j;
    }
public:
    vector<vector<int>> kClosest(vector<vector<int>>& points, int K) {
        int n = points.size();
        int lo = 0, hi = n - 1, p = -1;
        
        while(lo < hi){
            p = partition(points, lo, hi);
            if(p == K - 1){
                break;
            }
            else if(p < K - 1){
                lo = p + 1;
            }
            else{
                hi = p - 1;
            }
        }
        
        vector<vector<int>> ret;
        for(int i = 0; i < K; i++){
            ret.push_back(points[i]);
        }
        return ret;
    }
};

int main(){
    vector<vector<int>> v = {{68,97},{34,-84},{60,100},{2,31},{-27,-38},{-73,-74},{-55,-39},{62,91},{62,92},{-57,-67}};
    int K = 5;
    Solution sol;
    vector<vector<int>> res = sol.kClosest(v, K);
    return 0;
}
