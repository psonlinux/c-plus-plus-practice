/*
T : O(m * n)
S : O(1)
*/
class Solution {
   public:
    int findLength(vector<int>& A, vector<int>& B) {
        int res = 0;
        int m = A.size(), n = B.size(), t = 0;

        // shift A left one at a time
        for (int i = 0; i < m; i++) {
            t = 0;
            for (int j = i, k = 0; j < m && k < n; j++, k++) {
                if (A[j] == B[k]) {
                    t += 1;
                    res = max(res, t);
                } else {
                    t = 0;
                }
            }
        }

        // shift B left one at a time
        for (int i = 0; i < n; i++) {
            t = 0;
            for (int j = i, k = 0; j < n && k < m; j++, k++) {
                if (B[j] == A[k]) {
                    t += 1;
                    res = max(res, t);
                } else {
                    t = 0;
                }
            }
        }
        return res;
    }
};