//
//  CountRange.cpp
//  Leetcode
//
//  Created by Prashant Singh on 27/06/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

/*
 T : O(nlogn)
 S : O(n)
 cum[i] = cum[i - 1] + nums[i - 1]
 lower <= cum(i, j) <= upper , i < j
 lower <= cum[j] - cum[i] <= upper , i < j
 */
class Solution {
private:
    vector<long> t;
    
    int countRangesAcross(vector<long> & cum, int lo, int mid, int hi, int lower, int upper){
        int count = 0;
        int lb = mid,ub = mid;
        /*
         For each left cum[i] find the appropriate cum[j] in right subarray.
         Selecting j from right subarray will always satisfy that j > i .
         */
        for(int i = lo; i < mid; i++){
            while(lb < hi && (cum[lb] - cum[i]) < lower) lb += 1;
            while(ub < hi && (cum[ub] - cum[i]) <= upper) ub += 1;
            count   += (ub - lb);
        }
        
        merge(cum, lo, mid, hi);
        return count;
    }
    
    // Merge to sorted array
    void merge(vector<long> & cum, int lo, int mid, int hi){
        int i = lo, j = mid, k = lo;
        while(i < mid && j < hi){
            if(cum[i] < cum[j]){
                t[k] = cum[i];
                k += 1;
                i += 1;
            }
            else{
                t[k] = cum[j];
                k += 1;
                j += 1;
            }
        }
        
        //copy left subarray
        while(i < mid){
            t[k] = cum[i];
            i += 1;
            k += 1;
        }
        
        //copy right subarray
        while(j < hi){
            t[k] = cum[j];
            j += 1;
            k += 1;
        }
        
        // copy temp array back to cum
        for(k = lo; k < hi; k++){
            cum[k] = t[k];
        }
    }
    
    int countRanges(vector<long> & cum, int lo, int hi, int lower, int upper){
        if(hi - lo < 2){
            return 0;
        }
        
        int mid = lo + (hi - lo) / 2;
        int count = 0;
        
        // count ranges in left half
        count += countRanges(cum, lo, mid, lower, upper);
        
        // count ranges in right half
        count += countRanges(cum, mid, hi, lower, upper);
        
        // count ranges across
        count += countRangesAcross(cum, lo, mid, hi, lower, upper);
        return count;
    }
    
public:
    int countRangeSum(vector<int>& nums, int lower, int upper) {
        int n = nums.size();
        if(n == 0){
            return 0;
        }
        
        t.resize(n + 1);
        // cumulative sum
        vector<long> cum(n + 1);
        for(int i = 1; i < n + 1; i++){
            cum[i] = cum[i - 1] + nums[i - 1];
        }
        
        int cr = countRanges(cum, 0, n + 1, lower, upper);
        return cr;
    }
};
