//
//  LIS.cpp
//  Leetcode
//
//  Created by Prashant Singh on 01/08/19.
//  Copyright © 2019 Prashant Singh. All rights reserved.
//

#include <iostream>
#include <vector>
using namespace std;
/* 10 9 2 5 3 7*/
class Solution {
public:
    int lengthOfLIS(vector<int>& A) {
        int n = A.size();
        if(n < 2){
            return n;
        }
        
        vector<int> sr(1);
        sr[0] = A[0];
        int lo = 0, hi, m = -1;
        for(int i = 1; i < n; i++){
            lo = 0;
            hi = sr.size() - 1;
            while(lo <= hi){
                m = lo + (hi - lo) / 2;
                if(A[i] == sr[m]){
                    break;
                }
                else if(A[i] > sr[m]){
                    lo = m + 1;
                }
                else{
                    hi = m - 1;
                }
            }
            
            if(A[i] > sr[m]){
                m = m + 1;
            }
            
            //add as last item
            if(m == sr.size()){
                sr.push_back(A[i]);
            }
            else{
                sr[m] = A[i];
            }
        }
        return sr.size();
    }
};


int main(){
    vector<int> A = {4,10,4};
    //vector<int> A = {10, 9, 2, 5, 3, 7};
    Solution sol;
    cout << sol.lengthOfLIS(A);
    return 0;
}
