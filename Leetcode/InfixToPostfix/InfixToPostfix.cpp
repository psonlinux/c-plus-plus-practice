#include<iostream>
#include<vector>
#include<string>
#include<unordered_map>
#include<queue>
#include<unordered_set>
#include<algorithm>
#include<stack>
#include<ctype.h>
#include<fstream>
#include<stdio.h>
using namespace std;

int precedence(char op){
    switch(op){
        case '^':return 5;
        case '*':return 4;
        case '/':return 3;
        case '+':return 2;
        case '-':return 1;
        default: return 0;
    }
}

bool isOp(char c){
    return c == '+' || c == '-' || c == '*' || c == '/' || c == '-' || c == '#' || c == '^';
}

string f(string &s){
    s = s + "#";
    int n = s.size();
    stack<string> opd, opr;
    string cs = "";
    for(int i = 0; i < n; i++){
        if(isalpha(s[i]) == true){
            opd.push(cs + s[i]);
        }
        else if(s[i] == '('){
            opr.push(cs + s[i]);
        }
        else if(isOp(s[i])){
            //found a low prec operator
            while(opr.empty() == false && opr.top() != "(" && precedence(s[i]) < precedence(opr.top()[0])){
                string ex = "";
                ex = opd.top() + ex; opd.pop();
                ex = opd.top() + ex; opd.pop();
                ex = ex + opr.top(); opr.pop();
                opd.push(ex);
            }
            
            opr.push(cs + s[i]);
        }
        else if(s[i] == ')'){
            while(opr.empty() == false && opr.top() != "("){
                string exp = "";
                exp = opd.top() + exp; opd.pop();
                exp = opd.top() + exp; opd.pop();
                exp = exp + opr.top(); opr.pop();
                opd.push(exp);
            }
            opr.pop();  // remove "("
        }
    }
    return opd.top();
}

int main() {
    ifstream cin("./InfixToPostfix/in.txt", ios::in);
    int T = 0, t = 0;
    cin >> T;
    while(t < T){
        string s;
        cin >> s;
        cout << f(s) << endl;
        t += 1;
    }
    return 0;
}
