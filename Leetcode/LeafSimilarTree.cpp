/*
https://leetcode.com/problems/leaf-similar-trees/
*/
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<int> t1, t2;
    
    void postorder(TreeNode *n, vector<int> &t){
        if(!n)
            return ;
        postorder(n->left, t);
        postorder(n->right, t);
        if(!n->left && !n->right)
            t.push_back(n->val);
    }
    
    bool leafSimilar(TreeNode* root1, TreeNode* root2) {
        postorder(root1, t1);
        postorder(root2, t2);
        
        //equal ?
        int m = t1.size(), n = t2.size();
        if(m != n)
            return false;
        for(int i = 0; i < m; i++){
            if(t1[i] != t2[i])
                return false;
        }
        return true;
    }
};