
/*
https://leetcode.com/problems/n-ary-tree-postorder-traversal/
Time : O(n)
Space : O(n)
*/
/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};
*/
class Solution {
public:
    vector<int> postorder(Node* root) {
        vector<int> res;
        stack<Node *> stk;
        
        if(!root)
            return res;
        
        //add parent to stack
        stk.push(root);
        
        while(!stk.empty()){
            Node *t = stk.top();
            stk.pop();
            
            //add children on stack
            for(int i = 0; i < t->children.size(); i++)
                stk.push(t->children[i]);
            
            //add parent to result
            res.push_back(t->val);
        }
        reverse(res.begin(), res.end());
        return res;
    }
};