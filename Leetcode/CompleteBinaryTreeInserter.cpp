/*
https://leetcode.com/problems/complete-binary-tree-inserter/submissions/
*/

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/*
Time : O(n)		Space : O(n)
*/
class CBTInserter {
    vector<TreeNode *> a;
    
public:
    //Time complexity : O(n)  //Space complexity : O(n)
    CBTInserter(TreeNode* root) {
        // store node pointers in vector
        a.push_back(root);
        for(int i = 0; i < a.size(); i++){
            if(a[i]->left)  a.push_back(a[i]->left);
            else            break;
            if(a[i]->right) a.push_back(a[i]->right);
            else            break;
        }
    }
    
    //Time complexity : O(1)
    int insert(int v) {
        TreeNode *node = new TreeNode(v);	//create new node 
        a.push_back(node);					//add it
        int child = a.size();				//position of new node
        
        int parent = child/2 - 1;			//add child to parent node
        if(a[parent]->left == NULL) a[parent]->left = node;
        else                        a[parent]->right = node;
        
        return a[parent]->val;
    }
    
    //Time complexity : O(1)
    TreeNode* get_root() {
        return a[0];
    }
};

/**
 * Your CBTInserter object will be instantiated and called as such:
 * CBTInserter obj = new CBTInserter(root);
 * int param_1 = obj.insert(v);
 * TreeNode* param_2 = obj.get_root();
 */