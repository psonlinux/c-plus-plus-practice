class Solution {
    char * v;
    public:
     Solution(){
       v = (char *)malloc(10);  
    }
    private:
    // check single row
    bool isRowValid(int i, vector<vector<char>>& B){
       int cols = B[0].size();
        memset(v, 0, cols + 1);
        for(int j = 0; j < cols; j++){
            if(B[i][j] == '.')continue;
            if(v[B[i][j] - '0'] == true)
                return false;
            v[B[i][j] - '0'] = true;
        }
        return true;
    }
    
    bool isColumnValid(int j, vector<vector<char>>& B){
        int rows = B.size();
        memset(v, 0, rows + 1);
        for(int i = 0; i < rows; i++){
            if(B[i][j] == '.')continue;
            if(v[B[i][j] - '0'] == true)
                return false;
            v[B[i][j] - '0'] = true;
        }
        return true;
    }
    
    bool isSubBoxesValid(int r, int c, vector<vector<char>>& B){
        int rows = B.size();
        memset(v, 0, rows + 1);
        for(int i = r; i < r + 3; i++){
            for(int j = c; j < c + 3; j++){
                if(B[i][j] == '.')continue;
                if(v[B[i][j] - '0'] == true)
                    return false;
                v[B[i][j] - '0'] = true;
            }
        }
        return true;
    }  
public:
    bool isValidSudoku(vector<vector<char>>& board) {
        for(int i = 0; i < board.size(); i++){
            if(!isRowValid(i, board) || !isColumnValid(i, board) || !isSubBoxesValid((i / 3) * 3, (i % 3) * 3, board))
                return false;
        }
        return true;
    }
};