/*
https://leetcode.com/problems/intersection-of-two-arrays-ii/
Time : O(size of n1 and n2)
Space : O(size of n1 and n2)
*/
class Solution {
public:
    vector<int> intersect(vector<int>& n1, vector<int>& n2) {
        int m = n1.size();
        int n = n2.size();
        //put larger array in map
        unordered_map<int, int> um;
        for(int i = 0; i < n; i++){
            um[n2[i]] += 1;
        }
        
        //now find intersection
        vector<int> intersection;
        for(int i = 0;i < m; i++){
            if(um[n1[i]] > 0){
                intersection.push_back(n1[i]);
                um[n1[i]] -= 1;
            }
        }
        return intersection;
    }
};