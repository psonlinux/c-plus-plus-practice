/*
T : O(n log n)
S : O(n)
*/
class Solution {
   public:
    void wiggleSort(vector<int>& nums) {
        sort(nums.begin(), nums.end());

        int n = nums.size();
        int mid = n / 2 - 1, last = n - 1;
        if (n % 2 == 1) {
            mid = n / 2;
        }

        int j = mid, k = last;
        vector<int> v(n);
        for (int i = 0; i < n;) {
            if (j >= 0) {
                v[i] = nums[j];
                j -= 1;
                i += 1;
            }

            if (k > mid) {
                v[i] = nums[k];
                k -= 1;
                i += 1;
            }
        }
        nums = v;
    }
};