/*
T : O(n)
S : O(n)
*/
class Solution {
public:
    int maxProduct(vector<int>& nums) {
        int gMax = nums[0];             //global max
       int mx = nums[0], mn = nums[0];
        for(int i = 1; i < nums.size(); i++){
            int x = mx * nums[i];
            int y = mn * nums[i];
            mx = max(max(x, y), nums[i]);
            mn = min(min(x, y), nums[i]);
            
            if(mx > gMax)   gMax = mx;
        }
       return gMax;
    }
};