/*  1666 --- 6166
    132  --- 213
    4202320 -- 4203022
*/
/*
T : O(n)
S : O(1)
*/
class Solution {
public:
    void nextPermutation(vector<int>& v) {
        int i = 0, n = v.size();
        for(i = n - 2; i >= 0; i--){
            if(v[i] < v[i + 1])
                break;
        }
        
        if(i == -1){        // its largest
            reverse(v.begin(), v.end());
            return ;
        }
        
        // find first item from right side, greater than v[i]
        for(int j = n - 1; j >= 0; j--){
            if(v[j] > v[i]){
                swap(v[j], v[i]);
                break;
            }
        }
        
        // reverse v[k + 1, n-1]
        reverse(v.begin() + i + 1, v.end());
    }
};